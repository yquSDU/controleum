package dk.sdu.mmmi.controleum.impl.moea.solution;

import com.decouplink.Utilities;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Issue;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.Value;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.GroupScorer;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import dk.sdu.mmmi.controleum.api.moea.Commitable;

import static com.decouplink.Utilities.context;
import java.util.BitSet;
import java.util.Set;
import org.openide.util.Exceptions;

/**
 * @author mrj, jcs & ancla
 */
public class Option implements ISolution {

    private final static Random R = new Random();
    private final static Logger logger = Logger.getLogger(Option.class.getName());
    private final Object problem;
    private final Map<Issue, Object> issueValues;
    private final Map<Input, Object> inputValues;
    private final Map<Output, Object> outputValues;
    private final Date time;
    private List<Double> fitnessValues;
    private Map<Concern, Double> fitnessValuesMap;
    private List<Concern> concerns;
    private UUID id = UUID.randomUUID();
    boolean valid = true;
    private Map<Concern, MinMax> minMaxFitnessValues;

    /**
     * Copy an existing solution.
     */
    private Option(Object problem, Date time, Option oldSolution) {
        this.problem = problem;
        this.inputValues = new HashMap<>();
        this.outputValues = new HashMap<>();
        this.issueValues = new HashMap<>();
        this.time = time;
        this.concerns = oldSolution.getConcerns();

        updateInputValues(problem);

        updateOutputValues(problem);

        //For each issue in the current context
        for (Issue issue : Utilities.context(problem).all(Issue.class)) {
            //Copy the controleumValueList from the issue in the old solution
            Object controleumValueList = issue.copy(time, oldSolution.getIssueValues().get(issue));
            if (controleumValueList == null) {
                // If the issue contains no controleumValueList to copy, then generate a random controleumValueList
                controleumValueList = issue.getRandomValue(time);
            }
            //Map the controleumValueList to the issue in the current solution
            //Index the id of the issue 

            indexIssue(issue, controleumValueList);
        }
        notifyListeners();
        calculateFitness();

    }

    /**
     * Create a random solution.
     */
    private Option(Object problem, Date time, List<Concern> concerns) {
        this.problem = problem;
        this.issueValues = new HashMap<>();
        this.inputValues = new HashMap<>();
        this.outputValues = new HashMap<>();
        this.time = time;
        this.concerns = concerns;
        Collection<? extends Issue> issues = Utilities.context(problem).all(Issue.class);

        // Set outputs to null, as no value has been assigned to these until termination.
        updateOutputValues(problem);

        // Import input values into this candidate solution.
        updateInputValues(problem);

        // Import random ovalues into this candidate solution.
        if (issues.isEmpty()) {
            //TODO Implement exception handling. Use ControleumException.
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Illegal state: No Issues in context. Terminating.");
            System.out.println("No Issues in context. Terminating´.");
            System.exit(1);
        }
        for (Issue is : issues) {
            Object v = is.getRandomValue(time);
            indexIssue(is, v);
        }
        notifyListeners();
        calculateFitness();

    }

    /**
     * Mutate an existing solution.
     */
    private Option(Option mutateThis) {
        this.problem = mutateThis.getProblem();
        this.issueValues = new HashMap<>(mutateThis.getIssueValues());
        this.inputValues = new HashMap<>(mutateThis.inputValues);
        this.outputValues = new HashMap<>(mutateThis.outputValues);
        this.time = mutateThis.getTime();
        this.concerns = mutateThis.getConcerns();

        // Mutate the value of one random AbstractIssue.
        ArrayList<Issue> a = new ArrayList(getIssueValues().keySet());
        if (a.size() == 0) {
            System.out.println("Illigal staet in Controleum. No Entries in Issue-map. Terminating.");
            System.exit(1);
        }
        Issue randomIssue = a.get(getRandomizer().nextInt(a.size()));
        Object rValue = issueValues.get(randomIssue);
        Object nValue = randomIssue.getMutatedValue(rValue);
        indexIssue(randomIssue, nValue);

        notifyListeners();
        calculateFitness();

    }

    /**
     * Crossover solutions must come from the same context to guarantee same
     * number of Issues.
     */
    private Option(Option s1, Option s2) {
        this.problem = s1.getContext();
        this.inputValues = new HashMap<>(s1.getInputValues());
        this.outputValues = new HashMap<>(s1.getOutputValues()); //new HashMap<Output, Object>(s1.outputs);   //qqq comment
        this.issueValues = new HashMap<>(s1.getIssueValues());
        this.concerns = s1.getConcerns();
        this.time = s1.getTime();

        if (s1.getOutputValues().size() != s2.getOutputValues().size()) {
            throw new IllegalArgumentException("Expected same number of outputs.");
        }

        if (s1.getContext() != s2.getContext()) {
            throw new IllegalArgumentException("Expected same context.");
        }

        if (!s1.getTime().equals(s2.getTime())) {
            throw new IllegalArgumentException("Expected same time.");
        }
        if (issueValues.size() == 0) {
            System.out.println("Illigal staet in Controleum. No Entries in Issue-map. Terminating.");
            System.exit(1);
        }

        //int size = issueValues.size();      //qqq add for debug
        //int cut = R.nextInt(issueValues.size());    //TODO //qqq comment. ??? issueValues.size() is always ==1. So 0 ≤ R.nextInt(1) <1, so cut can be always only 0.
        ArrayList<Issue> keys = new ArrayList<>(s1.getIssueValues().keySet());

        //TODO //qqq add to fix the crossover bug.
        Issue issue = keys.get(0);
        Object get1 = s1.getIssueValues().get(issue);
        Object get2 = s2.getIssueValues().get(issue);
        issueValues.put(issue, issue.crossover(get1, get2));

        //TODO //qqq comment. 
        //qqq bug: here it is the switch status in the lightplan (Issue) that should participate in the crossover.
        //qqq But issueValues is the list of solutions, and there is always only one solution (s1) in the list.
        //qqq which causes that the result of the crossover is always s1 itself.
/*    int size = issueValues.size();      //qqq add for debug
      int cut = R.nextInt(issueValues.size());    //TODO //qqq comment. ??? issueValues.size() is always ==1. So 0 ≤ R.nextInt(1) <1, so cut can be always only 0.
      
        for (int i = 0; i < issueValues.size(); i++) {  //qqq??? issueValues.size() always == 1, so i can only be 0.

            Issue issue = keys.get(i);
            if (i < cut) {      //qqq??? i always equals to 0, always go into this if.
                Object v = s1.getIssueValues().get(issue);
                indexIssue(issue, v);       //qqq this sentance means: issueValues.put(issue, v)
            } else {
                Object v = s2.getIssueValues().get(issue);
                indexIssue(issue, v);
            }
        }
        logger.log(Level.INFO, String.format("-----\n"
                + "%s"
                + "%s"
                + "R: %s"
                + "cut: %s\n"
                + "keys = new ArrayList<>(s1.getIssueValues().keySet()) == %s\n",
                s1.qToString_onlyLP(), s2.qToString_onlyLP(), R, cut, keys));*/
        notifyListeners();
        calculateFitness();

    }

    
    //TODO //qqq add for compare v1 PF
    private Option(Object problem, Date time, List<Concern> concerns, String str) {
        this.problem = problem;
        this.inputValues = new HashMap<>();
        this.outputValues = new HashMap<>();
        this.issueValues = new HashMap<>();
        this.time = time;
        this.concerns = concerns;
        Collection<? extends Issue> issues = Utilities.context(problem).all(Issue.class);
        
        // Set outputs to null, as no value has been assigned to these until termination.
        updateOutputValues(problem);

        // Import input values into this candidate solution.
        updateInputValues(problem);

        // Import random ovalues into this candidate solution.
        if (issues.isEmpty()) {
            //TODO Implement exception handling. Use ControleumException.
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Illegal state: No Issues in context. Terminating.");
            System.out.println("No Issues in context. Terminating´.");
            System.exit(1);
        }

        BitSet bitset = new BitSet();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '1') {
                bitset.set(i);
            }
        }
        for (Issue is : issues) {
            Object lp = is.qSetBitSet(time, bitset);
            indexIssue(is, lp);
        }

        calculateFitness();
    }

    /**
     *
     * @return
     */
    public UUID getUUID() {
        return this.id;
    }

    private void indexIssue(Issue issue, Object v) {
        //TODO Test with this included:
        //issue.setValue(v);
        getIssueValues().put(issue, v);
    }

    private Random getRandomizer() {
        return R;
    }

    //
    // Public
    //
    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public double getEvaluationResult(int priority) {

        // Build total fitness.
        double sum = 0;

        // Calculate average.
        for (Concern c : Utilities.context(getProblem()).all(Concern.class)) {

            if (c.isEnabled() && priority == c.getPriority()) {
                sum += c.evaluate(this);
            }
        }

        return sum;
    }

    /**
     *
     * @param priority
     * @param concernsMinMax
     * @return
     */
    public double getEvaluationResult(int priority, Map<Concern, MinMax> concernsMinMax) {

        // Build total fitness.
        double sum = 0;
        Map<Concern, Double> normalizedFitnessValuesMap = new HashMap<>();
        // Calculate normalized fitness values for concerns in this priority.
        for (Concern c : Utilities.context(getProblem()).all(Concern.class)) {
            if (c.isEnabled() && priority == c.getPriority()) {
                MinMax concernMinMax = concernsMinMax.get(c);
                normalizedFitnessValuesMap.put(c, NormalizationUtil.normalizeMinMax(c.evaluate(this), concernMinMax.getMin(), concernMinMax.getMax()));
            }
        }
        Map<Concern, Double> normalizedFitnessValues = normalizedFitnessValuesMap;
        double average = NormalizationUtil.CalculateAverageCost(normalizedFitnessValues);

        return NormalizationUtil.calculateAbsoluteDeviation(normalizedFitnessValues, average);
    }

    /**
     *
     * @param priority
     * @param concernsMinMax
     * @return
     */
    @Override
    public double getSummarizedEvaluationResult(int priority, Map<Concern, MinMax> concernsMinMax) {

        // Build total fitness.
        double sum = 0;
        // Calculate normalized fitness values for concerns in this priority.
        //for (Concern c : Utilities.context(getProblem()).all(Concern.class)) {    //TODO //qqq 
        for (Concern c : concernsMinMax.keySet()) {         //TODO //qqq  Utilities.context(getProblem()).all(Concern.class) ->  concernsMinMax.keySet()
            if (c.isEnabled() && priority == c.getPriority()) {
                MinMax concernMinMax = concernsMinMax.get(c);
                double qNorm = NormalizationUtil.normalizeMinMax(c.evaluate(this), concernMinMax.getMin(), concernMinMax.getMax()); //qqq add
                sum += qNorm;
            }
        }

        return sum;
    }

    @Override
    public double getEvaluationValue(Class<? extends Concern> clazz) {
        return Utilities.context(getProblem()).one(clazz).evaluate(this);
    }

    /*
    //TODO UUID is assmued, but we are less strict when defining ID's.
    @Override
    public ControleumValueList getValue(UUID id) {
        for (Issue issue : getIssueValues().keySet()) {
            if (issue.getIssueId().equals(id)) {
                return issueValues.get(issue);
            }
        }
        return null;
    }
     */
    @Override
    public Object getContext() {
        return getProblem();

    }

    @Override
    public void commit(Date now) {
        //TODO Refactor this. SimulationOutput is last thing in api.aggregation - eliminate.
        for (Issue i : Utilities.context(getProblem()).all(Issue.class)) {
            Object value = issueValues.get(i);
            i.doCommitValue(now, value);
        }

        updateOutputValues(getProblem());

        for (Commitable o : Utilities.context(getProblem()).all(Commitable.class)) {
            o.commit(this);
        }

    }

    @Override
    public Solution mutate() {
        return new Option(this);
    }

    @Override
    public Solution crossover(Solution s) {
        return new Option(this, (Option) s);
    }

    @Override
    public Solution copy(Object context, Date time) {
        return new Option(context, time, this);
    }

    /**
     *
     * @param context
     * @param time
     * @param concerns
     * @return
     */
    public static Option random(Object context, Date time, List<Concern> concerns) {
        return new Option(context, time, concerns);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        //sb.append("Issue values: ");    //qqq comment
        List<String> outputStrs = new ArrayList<>();
        for (Map.Entry<Issue, Object> e : getIssueValues().entrySet()) {
            outputStrs.add(/*e.getKey().getName() + " =\t"*/"\t" + e.getValue() + "\n");    //qqq comment name
        }
        Collections.sort(outputStrs);
        for (String s : outputStrs) {
            sb.append(s);
        }
        sb.append("  Fitness description:\n");
        for (Map.Entry<Concern, Double> fitnessPair : fitnessValuesMap.entrySet()) {
            sb.append(fitnessPair.getKey().getName() + ": " + fitnessPair.getValue() + "\n");
        }
        return sb.toString();
    }

    /**
     * @author yqu
     * @return string for analyze file
     */
    @Override
    public String qToString_onlyLP() {
        StringBuilder sb = new StringBuilder();
        List<String> outputStrs = new ArrayList<>();
        for (Map.Entry<Issue, Object> e : getIssueValues().entrySet()) {
            outputStrs.add(" " + e.getValue() + "\n");
        }
        Collections.sort(outputStrs);
        for (String s : outputStrs) {
            sb.append(s);
        }
        return sb.toString();
    }

    @Override
    public List<Double> getEvaluationValues() {

        return fitnessValues;

    }

    /**
     *
     */
    public void calculateFitness() {
        fitnessValuesMap = new HashMap<>();
        for (Concern c : getConcerns()) {
            if (c.isEnabled()) {
                try {
                    Double fitnessValue = c.evaluate(this);
                    fitnessValuesMap.put(c, fitnessValue);
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }

            } else {
                // ensure the num. of fitness values == total num. of objectives
                fitnessValuesMap.put(c, 0.0);
            }
        }

        int concernSize = this.concerns.size();
        if (fitnessValuesMap.size() != concernSize) {
            for (Concern c : getConcerns()) {
                if (!fitnessValuesMap.keySet().contains(c)) {
                    if (c.isEnabled()) {
                        try {
                            Double fitnessValue = c.evaluate(this);
                            fitnessValuesMap.put(c, fitnessValue);
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                        }
                    }
                } else {
                    // ensure the num. of fitness values == total num. of objectives
                    fitnessValuesMap.put(c, 0.0);
                }
            }
        }

        List<Concern> orderedKeys = new ArrayList(fitnessValuesMap.keySet());
        Collections.sort(orderedKeys, new SortConcerns());
        fitnessValues = new ArrayList();
        for (Concern orderedKey : orderedKeys) {
            fitnessValues.add(fitnessValuesMap.get(orderedKey));
        }
    }

    @Override
    public void publishContract(Date now) {
        for (Commitable problem : Utilities.context(getProblem()).all(Commitable.class)) {
            problem.commit(this);
        }

        for (Issue issue : Utilities.context(getProblem()).all(Issue.class)) {
            Object v = getIssueValues().get(issue);
            //Set value for issue
            issue.setValue(v);
            issue.doCommitValue(now, v);
        }

        for (Concern concern : Utilities.context(getProblem()).all(Concern.class)) {
            if (concern.isEnabled()) {
                concern.commit(this);
            }
        }
    }

    @Override
    public Map<String, Issue> getIssues() {
        throw new UnsupportedOperationException("getIssues() method are not implemented");
    }

    @Override
    public double getApproxFairnessEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {
        // Build total fitness.
        double result = 0.0;

        result = GroupScorer.APPROX_FAIRNESS.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getFairnessAnalysisEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {
        // Build total fitness.
        double result = 0.0;

        result = GroupScorer.FAIRNESS_ANALYSIS.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getQuantitativeFairnessEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {
        // Build total fitness.
        double result = 0.0;

        result = GroupScorer.QUANTITATIVE_FAIRNESS.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getEntropyEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {
        // Build total fitness.
        double result = 0.0;

        result = GroupScorer.ENTROPY.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getSummarizedEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {
        // Build total fitness.
        double result = 0.0;

        result = GroupScorer.SOCIAL.score(score);       //qqq score(score): return normalized sum
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getEgalitarianEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {

        double result = 0.0;
        result = GroupScorer.EGALITARIAN.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getElitistEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {

        double result = 0.0;
        result = GroupScorer.ELITIST.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getRankEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {

        double result = 0.0;
        result = GroupScorer.MEDIAN_RANK.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public double getNashEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {

        double result = 0.0;

        result = GroupScorer.NASH_PRODUCT.score(score);
        this.minMaxFitnessValues = minMaxFitnessValues;
        return result;
    }

    @Override
    public List<Double> getLexiMinEvaluationResult(List<Double> score, Map<Concern, MinMax> minMaxFitnessValues) {

        Collections.sort(score, Collections.reverseOrder()); // descending ordering of cost vector
        this.minMaxFitnessValues = minMaxFitnessValues;
        return score;
    }

    @Override
    public Map<Output, Object> getOutputValues() {
        return outputValues;
    }

    @Override
    public Map<Input, Object> getInputValues() {
        return inputValues;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<Concern, Double> getFitnessValuesMap() {
        return fitnessValuesMap;
    }

    /**
     * @return the concerns
     */
    @Override
    public List<Concern> getConcerns() {
        return concerns;
    }

    @Override
    public double getRootEvaluationResult(List<Concern> ca) {
        return 0;
    }

    /**
     *
     * @param o2
     * @return
     */
    @Override
    public boolean equals(Object o2) {
        return (o2 instanceof Option) && (this.compare((Option) o2) == 0) && this.getIssueValues().equals(((Option) o2).getIssueValues());
    }

    private int compare(Option s) {
        List<Double> f1 = this.getEvaluationValues();
        List<Double> f2 = s.getEvaluationValues();

        if (f1.size() != f2.size()) {
            throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
        }
        int v;
        for (int i = 0; i < f1.size(); i++) {
            v = f1.get(i).compareTo(f2.get(i));
            if (v != 0) {
                return v;
            }
        }

        return 0;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.inputValues);
        hash = 97 * hash + Objects.hashCode(this.outputValues);
        hash = 97 * hash + Objects.hashCode(this.issueValues);
        hash = 97 * hash + Objects.hashCode(this.fitnessValuesMap);
        return hash;
    }

    /**
     *
     * @return
     */
    public List<Double> getFitnessValues() {
        return fitnessValues;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     *
     */
    @Override
    public void invalidate() {
        this.valid = false;
    }

    /**
     * @return the problem
     */
    public Object getProblem() {
        return problem;
    }

    /**
     * @return the issueValues
     */
    @Override
    public Map<Issue, Object> getIssueValues() {
        return issueValues;
    }

    //TODO ONLY TEST
    /**
     *
     * @param id
     * @param value
     */
    public void setValue(Issue id, Object value) {
        issueValues.put(id, value);
    }

    private void notifyListeners() {
        Collection<? extends OptionCreatedListener> listeners = Utilities.context(problem).all(OptionCreatedListener.class);
        for (OptionCreatedListener l : listeners) {
            l.notify(this);
        }

    }

    private void notifyEvalComplete() {
        Collection<? extends OptionCreatedListener> listeners = Utilities.context(problem).all(OptionCreatedListener.class);
        for (OptionCreatedListener l : listeners) {
            l.notifyEvalComplete(this);
        }

    }

    @Override
    public <T> T getValue(Class<? extends Value<T>> valueClazz) {
        return getValue(Utilities.context(problem).one(valueClazz));
    }

    public <T> T getValue(Value<T> valueObject) {
        /*TODO Probably the thought was here that you could look up both
        input values and issue values. Not it might also be output values.
        Reimplement in a better way.
         */
        Object v = inputValues.get(valueObject);
        if (v == null) {
            v = issueValues.get(valueObject);
        }

        assert v != null;

        return (T) v;
    }

    private static class SortConcerns implements Comparator {

        public SortConcerns() {
        }

        @Override
        public int compare(Object o1, Object o2) {
            Concern co1 = (Concern) o1;
            Concern co2 = (Concern) o2;
            return co1.getName().compareTo(co2.getName());
        }
    }

    private void updateOutputValues(Object context) {
        // Impot output values into this candidate solution.
        for (Output output : Utilities.context(context).all(Output.class)) {
            outputValues.put(output, output.getValue());
        }
    }

    private void updateInputValues(Object problem1) {
        // Import input values into this candidate solution.
        for (Input i : Utilities.context(problem1).all(Input.class)) {
            inputValues.put(i, i.getValue());
        }
    }

    @Override
    public Object qGetProblem() {
        return problem;
    }

    @Override
    public Solution convertStringToLP(Object problem, Date time, List<Concern> concerns, String str) {
        ArrayList<Issue> keys = new ArrayList<>(this.getIssueValues().keySet());
        Issue issue = keys.get(0);

        return new Option(problem, time, concerns, str);
    }
}
