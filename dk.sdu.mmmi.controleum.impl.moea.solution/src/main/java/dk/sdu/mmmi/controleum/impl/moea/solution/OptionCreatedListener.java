/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solution;

/**
 *
 * @author Anders
 */
public interface OptionCreatedListener {
    
    //TODO Already have a SolutionListener with a different purpose. Rename or reorganaize.

    /**
     *
     * @param solution
     */
    public void notify(Option solution);
    
    /**
     *
     * @param solution
     */
    public void notifyEvalComplete(Option solution);
}
