package dk.sdu.mmmi.controleum.common.config;

/**
 *
 * @author mrj
 */
public class ConcernConfig {

    private final boolean enabled;
    private final int priority;

    public static class Builder {

        // Optional parameters - initialized to default values
        private boolean isEnabled = true;
        private int priority = 1;

        public Builder() {
        }

        public Builder(ConcernConfig cfg) {
            isEnabled = cfg.isEnabled();
            priority = cfg.getPriority();
        }

        public Builder setEnabled(boolean isEnabled) {
            this.isEnabled = isEnabled;
            return this;
        }

        public Builder setPriority(int priority) {
            this.priority = priority;
            return this;
        }

        public ConcernConfig build() {
            return new ConcernConfig(this);
        }
    }

    private ConcernConfig(Builder b) {
        enabled = b.isEnabled;
        priority = b.priority;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getPriority() {
        return priority;
    }
}
