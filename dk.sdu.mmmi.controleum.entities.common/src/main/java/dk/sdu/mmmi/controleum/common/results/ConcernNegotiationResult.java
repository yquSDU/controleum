package dk.sdu.mmmi.controleum.common.results;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jcs
 */
public class ConcernNegotiationResult {

    private final BigDecimal cost;
    private final String concernID;
    private final Map<String, String> valueHelp;

    public ConcernNegotiationResult(String concernID, Double cost, Map<String, String> help) {
        this.concernID = concernID;
        this.cost = BigDecimal.valueOf(cost);
        this.valueHelp = help;
    }

    public ConcernNegotiationResult(String concernID, double cost, Map<Class, String> help) {
        this.concernID = concernID;
        this.cost = BigDecimal.valueOf(cost);
        this.valueHelp = new HashMap<>();
        for (Class clazz : help.keySet()) {
            valueHelp.put(clazz.getName(), help.get(clazz));
        }
    }

    //
    // Getters
    //
    public String getConcernID() {
        return concernID;
    }

    public double getCost() {
        return cost.doubleValue();
    }

    public Map<String, String> getValueHelp() {
        return valueHelp;
    }
}
