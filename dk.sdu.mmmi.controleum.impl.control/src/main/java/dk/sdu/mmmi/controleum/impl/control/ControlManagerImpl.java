package dk.sdu.mmmi.controleum.impl.control;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlListener;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import dk.sdu.mmmi.controleum.common.config.ControlConfig;
import dk.sdu.mmmi.controleum.common.units.Duration;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.netbeans.api.progress.ProgressHandle;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.WINDOW_SIZE;
import static dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil.endOfXDaysAfter;

/**
 * @author jcs
 */
public final class ControlManagerImpl implements ControlManager, ControleumListener, Runnable {

    private final ControleumContext g;
    private final ScheduledExecutorService scheduler;
    private ScheduledFuture<?> scheduledAutoTask;
    private final ContextDataAccess db;
    private ControlConfig cfg;
    private ScheduledFuture<?> controlTask;
    private Random random;

    public ControlManagerImpl(ControleumContext g) {
        this.g = g;
        this.scheduler = context(g).one(ScheduledExecutorService.class);
        this.db = context(g).one(ContextDataAccess.class);
        this.cfg = db.getControlConfiguration();
        this.random = new Random();
    }

    //
    // Public methods
    //
    @Override
    public void startAutoControl() {

        ControlConfig c = new ControlConfig.Builder(this.cfg).autoRunning(true).build();
        save(c);

        // Schedule with initial delay of 1-10 minutes.
        scheduledAutoTask = scheduler.scheduleAtFixedRate(this, (long) (random.nextDouble() * 10 * 1000 * 60), getDelay().toMS(), TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopAutoControl() {

        ControlConfig c = new ControlConfig.Builder(this.cfg).autoRunning(false).build();
        save(c);
        scheduledAutoTask.cancel(false);
    }

    @Override
    public void controlOnce() {
        ControlConfig c = new ControlConfig.Builder(this.cfg).controlling(true).build();
        save(c);
        controlTask = scheduler.schedule(this, 0, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopControlOnce() {
        ControlConfig c = new ControlConfig.Builder(this.cfg).controlling(false).build();
        save(c);
        if (controlTask != null) {
            controlTask.cancel(true);
        }
    }

    private synchronized void save(ControlConfig config) {
        this.cfg = config;
        // Save changed config in db
        db.setControlConfiguration(config);
        notifyListeners();
    }

    @Override
    public void run() {

        ProgressHandle h = ProgressHandle.createHandle("Controlling.");
        Date d = new Date();
        Date endTime = endOfXDaysAfter(d, WINDOW_SIZE-1);       //qqq add

        // Execute negotiation
        SearchStatistics ns = context(g).one(Solver.class).solve(d, endTime, h, true);     //qqq add "endTime"

        // Save results
        ControlHelper.saveNegotiationResult(g, d, ns);

        // Notify listeners
        ControlConfig c = new ControlConfig.Builder(this.cfg).controlling(false).build();
        save(c);
    }

    //
    // Setter and getters
    //
    @Override
    public void setSimulating(boolean isSimulating) {
        save(new ControlConfig.Builder(this.cfg).simulating(isSimulating).build());
    }

    @Override
    public void setDelayMS(long controlIntervalMS) {
        save(new ControlConfig.Builder(this.cfg).delay(controlIntervalMS).build());
    }

    @Override
    public Duration getDelay() {
        return this.cfg.getDelay();
    }

    @Override
    public boolean isSimulating() {
        return this.cfg.isSimulating();
    }

    @Override
    public boolean isControlling() {
        return this.cfg.isControlling();
    }

    @Override
    public boolean isAutoRunning() {
        return this.cfg.isAutoRunning();
    }

    @Override
    public boolean isSimCsvExport() {
        return this.cfg.isSimCsvExport();
    }

    //
    //
    //
    @Override
    public void dispose() {
        if (scheduledAutoTask != null) {
            scheduledAutoTask.cancel(false);
        }
    }

    //
    // Events
    //
    @Override
    public void onControlDomainAdded(ControleumEvent e) {

        // Init control (start automatic control?) when the greenhouse is
        // loaded.
        if (isAutoRunning()) {
            startAutoControl();
        }
    }

    @Override
    public void onControlDomainUpdated(ControleumEvent e) {
    }

    @Override
    public void onControlDomainDeleted(ControleumEvent e) {
        dispose();
    }

    //
    // Private
    //
    private void notifyListeners() {

        for (ControlListener l : context(g).all(ControlListener.class)) {
            l.onControlConfigChanged();
        }
    }
}
