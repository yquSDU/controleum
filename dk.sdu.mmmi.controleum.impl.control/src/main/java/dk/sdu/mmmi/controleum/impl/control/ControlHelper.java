package dk.sdu.mmmi.controleum.impl.control;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.common.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.common.results.NegotiationResult;
import dk.sdu.mmmi.controleum.common.units.Sample;
import java.util.Date;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author jcs
 */
public class ControlHelper {

    public static void saveNegotiationResult(ControleumContext g, Date t, SearchStatistics ns) {

        // Save negotiated result
        NegotiationResult nr = new NegotiationResult(ns.getGenerationsEvaluated(), ns.getTimeSpendMS());

        ContextDataAccess db = context(g).one(ContextDataAccess.class);
        db.insertNegotiationResult(new Sample(t, nr));

        // Save Concern results
        for (Concern c : context(g).all(Concern.class)) {
            if (c.isEnabled()) {

                db.insertConcernNegotiationResult(
                        new Sample(t,
                                new ConcernNegotiationResult(
                                        c.getID(), c.getEvaluationResult(),
                                        c.getValueHelp())));
            }
        }
    }
}
