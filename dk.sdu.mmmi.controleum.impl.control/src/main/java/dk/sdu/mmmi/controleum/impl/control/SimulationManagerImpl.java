package dk.sdu.mmmi.controleum.impl.control;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.data.CSVExporter;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import dk.sdu.mmmi.controleum.api.simulation.SimulationContext;
import dk.sdu.mmmi.controleum.api.simulation.SimulationManager;
import java.io.File;
import static java.lang.System.getProperty;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author jcs
 */
public class SimulationManagerImpl implements SimulationManager, Runnable {

    private ScheduledFuture<?> sheduledTask;
    private Date from;
    private Date to;
    private final ControleumContext g;
    private static boolean bOnceOrContinue = false;     //qqq true: simulate only once. false: contine until finish all time [from, to]
    private Date cursor;

    public SimulationManagerImpl(ControleumContext g) {
        this.g = g;
    }

    @Override
    public void startSimulation(Date from, Date to, boolean onceOrContinue) {
        this.from = from;
        this.to = to;
        bOnceOrContinue = onceOrContinue;
        cursor = from;

        getControlManager().setSimulating(true);

        sheduledTask = getScheduler().schedule(this, 0, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopSimulation() {
        if (sheduledTask != null) {
            sheduledTask.cancel(false);
        }
        getControlManager().setSimulating(false);
    }

    @Override
    public void run() {

        // Init Sim. context
        SimulationContext context = context(g).one(SimulationContext.class);
        context.init(from, to);

        if (false == bOnceOrContinue) {     //qqq false: contine until finish all time [from, to]
            while (context.hasNext()) {
                System.out.println("\nSIMULATION START");

                // Get next state from simulator.
                Date d = context.next();

                System.out.println("Date: " + d);

                ProgressHandle h = ProgressHandle.createHandle("Simulating.");

                // Control.
                SearchStatistics ns = context(g).one(Solver.class).solve(d, this.to, h, true);

                ControlHelper.saveNegotiationResult(g, d, ns);

                System.out.format("SIMULATION STEP %d OF %d COMPLETED.\n", context.position(), context.size());

                if (!getControlManager().isSimulating()) {
                    return;
                }
            }
        }
        else{       //qqq true: simulate only once.
            System.out.println("\nSIMULATION START");

            // Get next state from simulator.
            Date d = context.next();

            System.out.println("Date: " + d);

            ProgressHandle h = ProgressHandle.createHandle("Simulating.");

            // Control.
            SearchStatistics ns = context(g).one(Solver.class).solve(d, this.to, h, true);

            ControlHelper.saveNegotiationResult(g, d, ns);

            System.out.format("SIMULATION STEP %d OF %d COMPLETED.\n", context.position(), context.size());

            if (!getControlManager().isSimulating()) {
                return;
            }
        }

        // Save simulation data as CSV using CSVExporter (Data API).
        exportSimulationDataToCSV();

        context.dispose();

        getControlManager().setSimulating(false);
    }

    @Override
    public boolean isSimulationReady() {
        return !getControlManager().isAutoRunning()
                && !getControlManager().isSimulating()
                && !getControlManager().isControlling();
    }

    @Override
    public boolean isSimulating() {
        return getControlManager().isSimulating();
    }
    //
    // Private
    //

    private void exportSimulationDataToCSV() {
        if (getControlManager().isSimCsvExport()) {
            String dir = getProperty("user.home") + "/GreenhouseSimulationData/";
            new File(dir).mkdir();

            String date = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
            File dataFile = new File(dir + "GreenhouseSimulationData-" + date + ".csv");

            try {
                CSVExporter ex = new CSVExporter(g, from, to);
                //TODO: ProgressHandle h = ProgressHandleFactory.createHandle("Exporting simulation data to a CVS file.");
                ex.export(dataFile, null);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private ControlManagerImpl getControlManager() {
        return context(g).one(ControlManagerImpl.class);
    }

    private ScheduledExecutorService getScheduler() {
        return context(g).one(ScheduledExecutorService.class);
    }
    
}
