/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.ancla;

/**
 *
 * @author ancla
 */
public class MinMax {
    private double min;
    private double max;
    
    public MinMax()
    {
        min = 0.0;
        max = 0.0;
    }
    
    public MinMax(double min, double max)
    {
        this.min = min;
        this.max = max;
    }
    
    public MinMax(MinMax copy)
    {
        this.min = copy.getMin();
        this.max = copy.getMax();
    }


    /**
     * @return the min
     */
    public double getMin() {
        return min;
    }

    /**
     * @return the max
     */
    public double getMax() {
        return max;
    }

    public void setMax(Double curValue) {
        this.max = curValue;
    }

    public void setMin(Double curValue) {
        this.min = curValue;
    }
}
