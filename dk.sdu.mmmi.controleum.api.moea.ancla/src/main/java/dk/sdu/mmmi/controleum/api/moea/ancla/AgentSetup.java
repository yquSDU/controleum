/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.ancla;

import dk.sdu.mmmi.controleum.api.moea.Element;

/**
 *
 * @author ancla
* TODO This is not part of core Controleum. Refactor out into module that addresses multi-context optimization.
 */
public interface AgentSetup extends Element {
    public void doSetup();
    public void doUpdate();
    boolean isSubsystemAgent();
}
