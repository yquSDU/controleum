/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.ancla;

import dk.sdu.mmmi.controleum.api.moea.Concern;


import java.util.List;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public interface ConcernAgent extends Concern, AgentSetup {
   /**
     * @return the concernId
     */
    UUID getConcernId();

    /**
     * @return the threshold
     */
    UUID getIssueId();
    
    List<UUID> getIssueIds();
    
    void setIssueId(UUID issueId);
        
    double maxFitness();
    
}
