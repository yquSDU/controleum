/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

/**
 *
 * @author ancla
 */
public class ClusterSelection {

    final List<ISolution> pop;
    List<Cluster> clusters;
    List<Cluster> clustersForDistance;

    public ClusterSelection(List<ISolution> pop) {
        this.pop = pop;
    }

    public void reduceParetoSet(int reduceTo) {
        //Compute distances between all nodes
        clusters = createClusters();
        PriorityQueue<DistancePair> distanceQueue = computeAllDistances();
        while (clusters.size() > reduceTo) {
            //Merge two closets clusters
            DistancePair mostClosePair = distanceQueue.poll();
            distanceQueue.removeIf(qP -> qP.contains(mostClosePair.getC1()) || qP.contains(mostClosePair.getC2()));
            Cluster nCluster = mergeAndRemoveClusters(mostClosePair, clusters);
            distanceQueue.add(findMinDistanceSingle(nCluster));
        }
    }

    private Cluster mergeAndRemoveClusters(DistancePair mostClosePair, List<Cluster> clusters) {
        Cluster nCluster = mostClosePair.getC1();
        nCluster.merge(mostClosePair.getC2());
        clusters.remove(mostClosePair.getC2());
        return nCluster;
    }

    public List<ISolution> getReducedFront() {
        List<ISolution> newPop = new ArrayList<>();
        clusters.stream().forEach((c) -> {
            newPop.add(c.getPhysicalCentroid());
        });
        return newPop;
    }

    private List<Cluster> createClusters() {
        return pop.stream()
                .map(s -> new Cluster(s))
                .collect(Collectors.toList());

    }

    private PriorityQueue<DistancePair> computeAllDistances() {
        ArrayList<Cluster> clustersCopy = new ArrayList<>(clusters);
        clustersForDistance = new ArrayList<>(clusters);
        return clustersCopy.stream()
                .map(c -> findMinDistance(c))
                .collect(Collectors.toCollection(PriorityQueue::new));
    }

    private DistancePair findMinDistance(Cluster c) {
        double distanceToNearestNeighbour = Double.POSITIVE_INFINITY;
        Cluster nearestNeighbour = c;
        for (Iterator<Cluster> clusterIter = clustersForDistance.iterator(); clusterIter.hasNext();) {
            Cluster compareCluster = clusterIter.next();
            if (c != compareCluster) {
                double distanceToCompareCluster = c.findDistanceToVirtualCentroid(compareCluster.getVirtualCentroid());
                if (distanceToCompareCluster < distanceToNearestNeighbour) {
                    nearestNeighbour = compareCluster;
                    distanceToNearestNeighbour = distanceToCompareCluster;
                }
            } else {
                clusterIter.remove();
            }
        }

        return new DistancePair(distanceToNearestNeighbour, c, nearestNeighbour);
    }

    private DistancePair findMinDistanceSingle(Cluster c) {
        double distanceToNearestNeighbour = Double.POSITIVE_INFINITY;
        Cluster nearestNeighbour = c;
        for (Iterator<Cluster> clusterIter = clusters.iterator(); clusterIter.hasNext();) {
            Cluster compareCluster = clusterIter.next();
            if (c != compareCluster) {
                double distanceToCompareCluster = c.findDistanceToVirtualCentroid(compareCluster.getVirtualCentroid());
                if (distanceToCompareCluster < distanceToNearestNeighbour) {
                    nearestNeighbour = compareCluster;
                    distanceToNearestNeighbour = distanceToCompareCluster;
                }
            }
        }

        return new DistancePair(distanceToNearestNeighbour, c, nearestNeighbour);
    }

}
