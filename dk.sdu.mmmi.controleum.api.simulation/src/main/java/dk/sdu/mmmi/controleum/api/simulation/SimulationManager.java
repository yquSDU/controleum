package dk.sdu.mmmi.controleum.api.simulation;

import java.util.Date;

/**
 *
 * @author jcs
 */
public interface SimulationManager {

    void startSimulation(Date from, Date to, boolean onceOrContinue);

    void stopSimulation();

    boolean isSimulationReady();

    boolean isSimulating();
}
