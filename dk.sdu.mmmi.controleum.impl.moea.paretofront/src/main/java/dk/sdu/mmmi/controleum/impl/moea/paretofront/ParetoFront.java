package dk.sdu.mmmi.controleum.impl.moea.paretofront;

import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.ancla.IParetoFront;
import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulationSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection.ClusteredParetoOptimalSelector;
import dk.sdu.mmmi.controleum.impl.moea.paretoselection.ParetoOptimalSelector;
import java.io.FileWriter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ancla
 */
public class ParetoFront implements IParetoFront {
    private List<ISolution> paretoFront;
    private IPopulationSelector selector;

    public ParetoFront (List<ISolution> pop, SearchConfiguration cfg) {
        this.paretoFront = new ArrayList<>(pop);

        if (cfg.isClusteringEnabled()) {
            this.selector = new ClusteredParetoOptimalSelector(cfg.getPopSize(), cfg.reduceTo());
        } else {
            this.selector = new ParetoOptimalSelector();
        }
    }

    @Override
    public boolean addAll(List<ISolution> newPopulation) {
        return selector.selectParetoFront(paretoFront, newPopulation);
    }

    @Override
    public boolean add(ISolution newSolution) {
        return this.paretoFront.add(newSolution);
    }

    @Override
    public List<ISolution> getCopy() {
        return new ArrayList<>(paretoFront);
    }
    
    @Override
    public void compV1PF (FileWriter fw_v1PF, List<ISolution> v1PF) {
        selector.compare_v1PF(fw_v1PF, v1PF, paretoFront);
    }
}
