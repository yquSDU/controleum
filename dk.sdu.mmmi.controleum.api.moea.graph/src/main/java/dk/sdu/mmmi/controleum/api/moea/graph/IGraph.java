package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;

import java.util.*;
import org.w3c.dom.Document;

public interface IGraph<T> {

    
    
    /**
     * Are there any verticies in the graph
     * @return true if there are no verticies in the graph
     */
     boolean isEmpty();


    /**
     * Get the vertex count.
     * @return the number of verticies in the graph.
     */
     int size();

    /**
     * Get the root vertex
     * @return the root vertex if one is set, null if no vertex has been set as
     * the root.
     */
     IVertex<T> getRootVertex();

     String getName();

     IVertex<T> findVertexByData(T data);
    /**
     * Search the graph for cycles. In order to detect cycles, we use a modified
     * depth first search called a colored DFS. All nodes are initially marked
     * white. When a node is encountered, it is marked grey, and when its
     * descendants are completely visited, it is marked black. If a grey node is
     * ever encountered, then there is a cycle.
     *
     * @return the edges that form cycles in the graph. The array will be empty if
     * there are no cycles.
     */
     ArrayList findCycles();


    /**
     * Get the graph edges
     * @return the graph edges
     */
     List<IEdge<T>> getEdges();

     boolean getEdge(IVertex<T> from, IVertex<T> to);


    /**
     * Clear the mark state of all verticies in the graph by calling clearMark()
     * on all verticies.
     * @see IVertex#clearMark()
     */
     void clearMark();

    /**
     * Clear the mark state of all edges in the graph by calling clearMark() on
     * all edges.
     */
     void clearEdges();

     Document depthFirstSearch(IVertex v, IVisitor visitor) throws VisitorException;

    /**
     * Perform a breadth first search of this graph, starting at v. The vist may
     * be cut short if visitor throws an exception during a vist callback.
     *
     * @param v the search starting point
     * @param visitor the vistor whose vist method is called prior to visting a vertex.
     * @throws VisitorException if vistor.visit throws an exception
     */
     void breadthFirstSearch(IVertex<T> v, IVisitor<T> visitor) throws VisitorException;

    /**
     * Find the spanning tree using a DFS starting from v.
     * @param v the vertex to start the search from
     * @param visitor visitor invoked after each vertex is visited and an edge is added to the tree.
     */
     void dfsSpanningTree(IVertex<T> v, IDFSVisitor<T> visitor);



     String toString();

     List<RelativeImportanceConcern> getConcerns();

     GraphType getGraphType();

    /**
     * Perform a depth first serach using recursion.
     * @param v the Vertex to start the search from
     * @param visitor the vistor to inform prior to
     * @param minMaxFitnessValues
     * @param rSelector
     * @return
     */
    List<ISolution> depthFirstSearch(IVertex v, IVisitor visitor, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector);
}
