package dk.sdu.mmmi.controleum.api.moea.graph;

import java.util.List;

public interface IVertex<T> {
    /**
     * @return the possibly null name of the vertex
     */
    String getName();

    String getSelector();

    void setSelector(String selector);

    /**
     * @return the possibly null data of the vertex
     */
    List<T> getData();

    boolean isRoot();

    /**
     * @param data
     *          The data to set.
     */
    void addData(T data);

    /**
     * @param data
     *          The data to remove.
     */
    void removeData(T data);

    /**
     * Add an edge to the vertex. If edge.from is this vertex, its an outgoing
     * edge. If edge.to is this vertex, its an incoming edge. If neither from or
     * to is this vertex, the edge is not added.
     *
     * @param e -
     *          the edge to add
     * @return true if the edge was added, false otherwise
     */
    boolean addEdge(IEdge<T> e);

    /**
     * Add an outgoing edge ending at to.
     *
     * @param to -
     *          the destination vertex
     * @param cost
     *          the edge cost
     */
    void addOutgoingEdge(IVertex<T> to, int cost);

    /**
     * Add an incoming edge starting at from
     *
     * @param from -
     *          the starting vertex
     * @param cost
     *          the edge cost
     */
    void addIncomingEdge(IVertex<T> from, int cost);

    /**
     * Check the vertex for either an incoming or outgoing edge mathcing e.
     *
     * @param e
     *          the edge to check
     * @return true it has an edge
     */
    boolean hasEdge(IEdge<T> e);

    /**
     * Remove an edge from this vertex
     *
     * @param e -
     *          the edge to remove
     * @return true if the edge was removed, false if the edge was not connected
     *         to this vertex
     */
    boolean remove(IEdge<T> e);

    boolean removes(IEdge<T> e);


    void removeEdges();


    void removeEdgesExcept(IEdge<T> e);

    /**
     *
     * @return the count of incoming edges
     */
    int getIncomingEdgeCount();

    /**
     * Get the ith incoming edge
     *
     * @param i
     *          the index into incoming edges
     * @return ith incoming edge
     */
    IEdge<T> getIncomingEdge(int i);

    /**
     * Get the incoming edges
     *
     * @return incoming edge list
     */
    List getIncomingEdges();

    int getIncomingEdgesCost();

    /**
     *
     * @return the count of incoming edges
     */
    int getOutgoingEdgeCount();

    /**
     * Get the ith outgoing edge
     *
     * @param i
     *          the index into outgoing edges
     * @return ith outgoing edge
     */
    IEdge<T> getOutgoingEdge(int i);

    /**
     * Get the outgoing edges
     *
     * @return outgoing edge list
     */
    List getOutgoingEdges();

    /**
     * Search the outgoing edges looking for an edge whose's edge.to == dest.
     *
     * @param dest
     *          the destination
     * @return the outgoing edge going to dest if one exists, null otherwise.
     */
    IEdge<T> findEdgeTo(IVertex<T> dest);

    IEdge<T> findEdgeFrom(IVertex<T> src);

    IEdge<T> findEdgeFromRoot(IVertex<T> from);

    int findCostFromRoot(IVertex<T> from);

    /**
     * Search the outgoing edges for a match to e.
     *
     * @param e -
     *          the edge to check
     * @return e if its a member of the outgoing edges, null otherwise.
     */
    IEdge<T> findEdge(IEdge<T> e);

    /**
     * What is the cost from this vertext to the dest vertex.
     *
     * @param dest -
     *          the destination vertex.
     * @return Return Integer.MAX_VALUE if we have no edge to dest, 0 if dest is
     *         this vertex, the cost of the outgoing edge otherwise.
     */
    int cost(IVertex<T> dest);

    /**
     * Is there an outgoing edge ending at dest.
     *
     * @param dest -
     *          the vertex to check
     * @return true if there is an outgoing edge ending at vertex, false
     *         otherwise.
     */
    boolean hasEdge(IVertex<T> dest);

    /**
     * Has this vertex been marked during a visit
     *
     * @return true is visit has been called
     */
    boolean visited();

    /**
     * Set the vertex mark flag.
     *
     */
    void mark();

    /**
     * Set the mark state to state.
     *
     * @param state
     *          the state
     */
    void setMarkState(int state);

    /**
     * Get the mark state value.
     *
     * @return the mark state
     */
    int getMarkState();

    /**
     * Visit the vertex and set the mark flag to true.
     *
     */
    void visit();


    /**
     * Clear the visited mark flag.
     *
     */
    void clearMark();

    /**
     * @return a string form of the vertex with in and out edges.
     */
    String toString();
}
