package dk.sdu.mmmi.controleum.api.moea.graph;

/**
 *
 * @author aiu
 */
public enum Selector {
    
    SOCIAL, EGALITARIAN, ELITIST, NASH_PRODUCT, APPROX_FAIRNESS, QUANTITATIVE_FAIRNESS, ENTROPY, FAIRNESS_ANALYSIS, MEDIANRANK, LEXIMIN;

}

