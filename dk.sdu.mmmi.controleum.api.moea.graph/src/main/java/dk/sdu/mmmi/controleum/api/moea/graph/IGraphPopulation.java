/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulation;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.io.FileWriter;
import java.util.List;

/**
 *
 * @author Anders
 */
public interface IGraphPopulation extends IPopulation {

    ISolution getBestGraphSolution(Selector s);

    ISolution getFinalGraphSolution(Selector s);

    public Selector getSelector();

    public Object getGraph();

    public IGraphPopulation evolveAsGraphPop();

    public void setLogFile(FileWriter file);   //qqq add
    
    public void compareV1PF(List<ISolution> pf_v2);   //qqq add
}
