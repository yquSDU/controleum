package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;

import java.util.List;


/**
 * A graph visitor interface.
 * 
 * @author Scott.Stark@jboss.org
 * @version $Revision$
 * @param <T>
 */
public interface Visitor<T> {
  /**
   * Called by the graph traversal methods when a vertex is first visited.
   * @param g - the graph
   * @param v - the vertex being visited.
   */
  public List<ISolution> visit(IGraph<T> g, IVertex<T> v) throws VisitorException;
}



