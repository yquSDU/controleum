/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.graph;

import java.util.List;

/**
 *
 * @author aiu
 */
public interface IGroupScorerInterface {
    
    double score(List<Double> normalizedValues);
    
}

