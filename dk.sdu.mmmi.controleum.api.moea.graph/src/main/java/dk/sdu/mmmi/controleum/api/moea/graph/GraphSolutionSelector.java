package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.io.FileWriter;
import java.util.Date;

import java.util.List;

/**
 *
 * @author ancla
 */
public interface GraphSolutionSelector {
    ISolution getSolution(List<ISolution> pop, Selector selector, Date time);       //TODO //qqq add "Date time"
    
    IGraph getGraph();
    
    void setLogFile(FileWriter file);   //qqq add
}

