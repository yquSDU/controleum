To build a single fat JAR file run:
./create-jar.sh
The output is in the dk.sdu.mmmi.controleum.deployment.aggregator/target directory named Controleum-library.jar


When new modules are added to the parent POM, make sure to include these in the dk.sdu.mmmi.controleum.deployment.aggregator POM under dependencies.
