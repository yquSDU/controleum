package dk.sdu.mmmi.controleum.api.control;

import java.util.EventListener;

/**
 *
 * @author jcs
 */
public interface ControlListener extends EventListener {

    void onControlConfigChanged();
}
