/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.NormalizedVarianceComparator;
import dk.sdu.mmmi.controleum.impl.moea.comperator.NormalizedVectorComparator;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class FairDifferenceVectorSelectorI implements ISolutionSelector {

    @Override
    public ISolution getSolution(List<ISolution> pop) {
                //Currently only supports fairness across entire population. Hence, should only be used when no priorities are present.
        //Recipe: Get a list of sorted solutions with their associated vector length.
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        NormalizedVarianceComparator BY_NORMALIZED_VARIANCE = new NormalizedVarianceComparator(minMaxFitnessValues);
        Collections.sort(pop, BY_NORMALIZED_VARIANCE);

        //Create then a sublist containing all solutions with the lowest vector norm (if more have the same).
        List<ISolution> popOptimalVariance = new ArrayList<>();
        ISolution bestVariance = pop.get(0);
        popOptimalVariance.add(bestVariance);
        for (ISolution s : pop) {
            if (NormalizationUtil.TotalDistanceFromAverage(bestVariance, minMaxFitnessValues) == NormalizationUtil.TotalDistanceFromAverage(s, minMaxFitnessValues)) {
                popOptimalVariance.add(s);
            } else {
                break;
            }
        }
        NormalizedVectorComparator BY_NORMALIZED_VECTOR = new NormalizedVectorComparator(minMaxFitnessValues);
        Collections.sort(popOptimalVariance, BY_NORMALIZED_VECTOR);
        return (ISolution) popOptimalVariance.get(0);
    }
    
}
