package dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ListSolutionSelection;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author aiu
 */
public class SocialSelector implements ListSolutionSelection {

    Map<ISolution, List<Double>> scoreForSolution;
    Map<Concern, MinMax> minMaxFitnessValues;
    public SocialSelector(Map<ISolution, List<Double>> scoreForSolution, Map<Concern, MinMax> minMaxFitnessValues) {

        this.scoreForSolution=scoreForSolution;
        this.minMaxFitnessValues=minMaxFitnessValues;
    }

    @Override
    public List<ISolution> select(List<ISolution> pop) {        //qqq !!!
        List<ISolution> candidatePop = new ArrayList<>();
        Map<ISolution, Double> fitnessForSolution = new HashMap<>();
        double f1;
        // evaluating solutions
        for (ISolution solution : pop) {
            f1 = solution.getSummarizedEvaluationResult(scoreForSolution.get(solution), minMaxFitnessValues);
            fitnessForSolution.put(solution, f1);
        }
       
        // Finding min score
       Collection c=fitnessForSolution.values();
       final double min=(double) Collections.min(c);
     
       //Extracting all solutions with min score and save them as list of candidate solutions
        candidatePop=fitnessForSolution.entrySet()
              .stream()
              .filter(entry -> Objects.equals(entry.getValue(), min))
              .map(Map.Entry::getKey)
              .collect(Collectors.toList());
        
        return candidatePop;

    }

}

 
       /* //Finding min score
        for (ISolution solution : scoreforSolution.keySet()) {
            min = (scoreforSolution.get(solution) < min) ? scoreforSolution.get(solution) : min;
        }


         // Extracting all solutions with min score and save them as list of candidate solutions
        for (ISolution solution : scoreforSolution.keySet()) {

            if (scoreforSolution.get(solution).equals(min)) {
                candidatePop.add(solution);
            }
        }*/