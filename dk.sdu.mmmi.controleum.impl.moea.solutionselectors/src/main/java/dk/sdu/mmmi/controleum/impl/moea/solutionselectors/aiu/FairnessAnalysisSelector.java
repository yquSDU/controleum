package dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ListSolutionSelection;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author aiu
 */
public class FairnessAnalysisSelector implements ListSolutionSelection {

    Map<ISolution, List<Double>> scoreForSolution;
    Map<Concern, MinMax> minMaxFitnessValues;

    public FairnessAnalysisSelector(Map<ISolution, List<Double>> scoreForSolution, Map<Concern, MinMax> minMaxFitnessValues) {

        this.scoreForSolution=scoreForSolution;
        this.minMaxFitnessValues=minMaxFitnessValues;
    }

    @Override
    public List<ISolution> select(List<ISolution> pop) {
        List<ISolution> candidatePop = new ArrayList<>();
        //double min = Double.MAX_VALUE;
        Map<ISolution, Double> fitnessForSolution = new HashMap<>();
        
        // evaluating solutions
        for (ISolution solution : pop) {
            double f1 = solution.getFairnessAnalysisEvaluationResult(scoreForSolution.get(solution),minMaxFitnessValues);
            fitnessForSolution.put(solution, f1);
        }
       
        // Finding min score
       Collection c=fitnessForSolution.values();
       final double min=(double) Collections.min(c);
     
       //Extracting all solutions with min score and save them as list of candidate solutions
        candidatePop=fitnessForSolution.entrySet()
              .stream()
              .filter(entry -> Objects.equals(entry.getValue(), min))
              .map(Map.Entry::getKey)
              .collect(Collectors.toList());
        
        return candidatePop;

    }

}



