/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.NormalizedVarianceComparator;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class FairDifferenceSelectorI implements ISolutionSelector {

    @Override
    public ISolution getSolution(List<ISolution> pop) {
        //Currently only supports fairness across entire population. Hence, should only be used when no priorities are present.
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        NormalizedVarianceComparator BY_SOCIAL_FAIRNESS = new NormalizedVarianceComparator(minMaxFitnessValues);
        Collections.sort(pop, BY_SOCIAL_FAIRNESS);
        return (ISolution) pop.get(0);
    }
    
}
