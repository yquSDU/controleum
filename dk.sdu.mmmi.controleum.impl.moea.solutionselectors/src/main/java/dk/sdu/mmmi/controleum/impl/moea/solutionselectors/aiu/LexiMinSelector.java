package dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ListSolutionSelection;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author aiu
 */
public class LexiMinSelector <T> implements ListSolutionSelection,Comparator<Solution> {

    Map<ISolution, List<Double>> scoreForSolution;
    Map<Concern, MinMax> minMaxFitnessValues;

    public LexiMinSelector(Map<ISolution, List<Double>> scoreForSolution, Map<Concern, MinMax> minMaxFitnessValues) {

        this.scoreForSolution = scoreForSolution;
        this.minMaxFitnessValues= minMaxFitnessValues;
    }

    @Override
    public List<ISolution> select(List<ISolution> pop) {
        List<ISolution> candidatePop = new ArrayList<>();
        Map<ISolution, List<Double>> fitnessForSolution = new HashMap<>();
        
        // evaluating solutions
        for (ISolution solution : pop) {
            List<Double> f1 = solution.getLexiMinEvaluationResult(scoreForSolution.get(solution), minMaxFitnessValues);
            fitnessForSolution.put(solution, f1);
        }
       
        
        Collections.sort(pop,this);
        
        List<Double> min=fitnessForSolution.get(pop.get(0));
       
     
       //Extracting all solutions with min score and save them as list of candidate solutions
        candidatePop=fitnessForSolution.entrySet()
              .stream()
              .filter(entry -> Objects.equals(entry.getValue(), min))
              .map(Map.Entry::getKey)
              .collect(Collectors.toList());
        
        return candidatePop;

    }

    @Override
    public int compare(Solution s1, Solution s2) {
        int v = 0;
        
            List<Double> f1 = ((ISolution) s1).getLexiMinEvaluationResult(scoreForSolution.get(s1), minMaxFitnessValues);
            List<Double> f2 = ((ISolution) s2).getLexiMinEvaluationResult(scoreForSolution.get(s2),minMaxFitnessValues);


            if (f1.size() != f2.size()) {
                throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
            }

            for (int i = 0; i < f1.size(); i++) {
                v = f1.get(i).compareTo(f2.get(i));
                if (v != 0) {
                    return v;
                }
            }

        
        return v;
    }
}


