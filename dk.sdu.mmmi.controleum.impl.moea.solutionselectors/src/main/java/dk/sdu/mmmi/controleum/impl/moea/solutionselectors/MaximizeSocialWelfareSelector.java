/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.PrioritizedNormalizedFitnessComparator;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class MaximizeSocialWelfareSelector implements ISolutionSelector {

    @Override
    public ISolution getSolution(List<ISolution> pop) {
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        PrioritizedNormalizedFitnessComparator BY_SOCIAL_BENEFIT = new PrioritizedNormalizedFitnessComparator(minMaxFitnessValues);
        Collections.sort(pop, BY_SOCIAL_BENEFIT);
        /*  if (pop.get(0).getConcerns().size() == 4) {

            if (pop.size() > 1) {
                System.out.println("Equal? " + BY_SOCIAL_BENEFIT.compare(pop.get(0), pop.get(1)));
            }

        }*/
        return (ISolution) pop.get(0);
    }

}
