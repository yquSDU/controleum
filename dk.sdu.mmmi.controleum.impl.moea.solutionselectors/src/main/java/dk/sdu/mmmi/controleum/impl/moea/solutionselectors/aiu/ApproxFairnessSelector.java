package dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ListSolutionSelection;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import static java.lang.Math.abs;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author aiu
 */
public class ApproxFairnessSelector<T> implements ListSolutionSelection {

    Map<ISolution, List<Double>> scoreForSolution;
    Map<Concern, MinMax> minMaxFitnessValues;

    public ApproxFairnessSelector(Map<ISolution, List<Double>> scoreForSolution, Map<Concern, MinMax> minMaxFitnessValues) {

        this.scoreForSolution = scoreForSolution;
        this.minMaxFitnessValues = minMaxFitnessValues;

    }

    @Override
    public List<ISolution> select(List<ISolution> pop) {
        List<ISolution> candidatePop = new ArrayList<>();
        List<ISolution> subPop = new ArrayList<>();             //TODO //qqq add. for saving candidates with same cost
        Map<ISolution, Double> fitnessForSolution = new HashMap<>();

        // evaluating solutions
        pop.stream().forEach((solution) -> {
            double f1 = solution.getApproxFairnessEvaluationResult(scoreForSolution.get(solution), minMaxFitnessValues);
            fitnessForSolution.put(solution, f1);
        });

        // Finding min score
        Collection c = fitnessForSolution.values();
        final double min = (double) Collections.min(c);

        //Extracting all solutions with min score and save them as list of candidate solutions
        candidatePop = fitnessForSolution.entrySet()
                .parallelStream()
                .filter(entry -> entry.getValue().equals(min))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        //TODO //qqq add. for saving candidates with same cost
//        for (Map.Entry candy : fitnessForSolution.entrySet()) {
//            if (0.001 > abs((Double) candy.getValue() - min)) {
//                subPop.add((ISolution) candy.getKey());
//            }
//        }
//
//        if (subPop != null) {
//            return subPop;            //qqq add
//        } else {
//            return candidatePop;      //qqq original
//        }
        return candidatePop;
    }

}
