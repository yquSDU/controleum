/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author ancla
 */
public class ApproxFairNoCompareSelectorI implements ISolutionSelector {

    @Override
    public ISolution getSolution(List<ISolution> pop) {
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        double[] bestApproximateFairness = new double[10];
        java.util.Arrays.fill(bestApproximateFairness, Double.MAX_VALUE);
        ISolution finalSolution = null;
        Map<ISolution, Map<Concern, Double>> normalizedPopulation = new HashMap<>();
        double n = pop.get(0).getEvaluationValues().size();
        
        Map<Concern, Double> normalizedSumPerConcern = normalizedAndSummarize(pop, minMaxFitnessValues, normalizedPopulation);
        
        Map<Concern, Double> averageSumPerConcern = findAverageCostPerConcern(normalizedSumPerConcern, pop);
        
        for (Entry<ISolution, Map<Concern, Double>> solutionWithNormalizedSolution : normalizedPopulation.entrySet()) {
            //Prio specific
            double[] approximateFairnessArray = new double[10];
            for (int i = 0; i < 10; i++) {
                Map<Concern, Double> concernsForPrio = new HashMap<>();
                for (Concern c : solutionWithNormalizedSolution.getValue().keySet()) {
                    if (c.getPriority() == i) {
                        concernsForPrio.put(c, solutionWithNormalizedSolution.getValue().get(c));
                    }
                }
                double approximateFairness = 0.0;
                for (Entry<Concern, Double> costForConcern : concernsForPrio.entrySet()) {
                   if(concernsForPrio.size()>1)  // check for number of concerns in a priority group
                       approximateFairness += Math.pow(costForConcern.getValue() - averageSumPerConcern.get(costForConcern.getKey()), 2) / n;
                   else
                       approximateFairness=costForConcern.getValue();               
                }
                approximateFairnessArray[i] = approximateFairness;
            }
            
            for (int i = 0; i < 10; i++) {
                if (approximateFairnessArray[i] < bestApproximateFairness[i]) {
                    bestApproximateFairness = approximateFairnessArray;
                    finalSolution = solutionWithNormalizedSolution.getKey();
                    
                    
                }
                else if (approximateFairnessArray[i] > bestApproximateFairness[i])
                {
                    break;
                }
            }

        }

        return finalSolution;
    }

    private Map<Concern, Double> findAverageCostPerConcern(Map<Concern, Double> normalizedSumPerConcern, List<ISolution> pop) {
        Map<Concern, Double> averageSumPerConcern = new HashMap<>();
        for(Entry<Concern,Double> e : normalizedSumPerConcern.entrySet())
        {
            averageSumPerConcern.put(e.getKey(),e.getValue()/pop.size());
        }
        return averageSumPerConcern;
    }

    private Map<Concern, Double> normalizedAndSummarize(List<ISolution> pop, Map<Concern, MinMax> minMaxFitnessValues, Map<ISolution, Map<Concern, Double>> normalizedPopulation) {
        Map<Concern, Double> normalizedSumPerConcern = new HashMap<>();
        for (ISolution solution : pop) {
            Map<Concern, Double> normalizedSolution = NormalizationUtil.NormalizeSolution(solution, minMaxFitnessValues);
            normalizedPopulation.put(solution, normalizedSolution);
            for(Entry<Concern, Double> e : normalizedSolution.entrySet())
            {
                normalizedSumPerConcern.put(e.getKey(), normalizedSumPerConcern.get(e.getKey()) + e.getValue());
            }
        }
        return normalizedSumPerConcern;
    }

}
