/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.generic.issue.generators;


/**
 *
 * @author ancla
 */
public class IntegerValueGenerator implements IValueGenerator {

    private final Integer upperBoundary;
    private final Integer lowerBoundary;
    private final Integer resolution;

    public IntegerValueGenerator(Integer lowerBoundary, Integer upperBoundary, Integer resolution) {
        this.upperBoundary = upperBoundary == null ? Integer.MAX_VALUE : upperBoundary;
        this.lowerBoundary = lowerBoundary == null ? (-1 * Integer.MAX_VALUE) : lowerBoundary;
        this.resolution = resolution == null ? 1 : resolution;
    }

    @Override
    public Integer generateRandomValue() {
        //return getLowerBoundary() + (int) (Math.random() * ((getUpperBoundary() - getLowerBoundary()) + 1));

        if (resolution > 0) {
            return (int) (Math.round(((Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary()) / resolution) * resolution);
        } else {
            return getLowerBoundary() + (int) (Math.random() * ((getUpperBoundary() - getLowerBoundary()) + 1));
        }
    }

    /**
     * @return the upperBoundary
     */
    @Override
    public Integer getUpperBoundary() {
        return upperBoundary;
    }

    /**
     * @return the lowerBoundary
     */
    @Override
    public Integer getLowerBoundary() {
        return lowerBoundary;
    }

    /**
     * @return the resolution
     */
    @Override
    public Integer getResolution() {
        return resolution;
    }
}
