/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class DoubleValueGeneratorFactory extends AbstractValueGeneratorFactory {
    
    private final static UUID id = UUID.fromString("fc0d1959-3473-4f4d-89cd-22cd97ac56e3");
    
    public DoubleValueGeneratorFactory()
    {
        super("Double", id);
    }
    
    @Override
    public DoubleValueGenerator createGenerator(Number lowerBoundary, Number upperBoundary, Number resolution)
    {
            return new DoubleValueGenerator(lowerBoundary.doubleValue(), upperBoundary.doubleValue(), resolution.doubleValue());
    }

    @Override
    public Number parseString(String value) throws NumberFormatException {
        return Double.parseDouble(value);
    }
       
}
