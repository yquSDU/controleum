/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;

/**
 * Value generators are used by Issues to generate random values for the schedules.
 * @author ancla
 */
public interface IValueGenerator {
    public Number generateRandomValue();
    
        /**
     * @return the upperBoundary
     */
    public Number getUpperBoundary();

    /**
     * @return the lowerBoundary
     */
    public Number getLowerBoundary();

    /**
     * @return the resolution
     */
    public Number getResolution();
}
