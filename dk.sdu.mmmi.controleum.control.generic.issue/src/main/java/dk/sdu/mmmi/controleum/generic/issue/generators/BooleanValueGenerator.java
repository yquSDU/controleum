/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.generic.issue.generators;

import java.util.Random;

/**
 *
 * @author ancla
 */
public class BooleanValueGenerator implements IValueGenerator {

    private final Random r = new Random();

    public BooleanValueGenerator() {
    }

    @Override
    public Short generateRandomValue() {
        return (short) (r.nextBoolean() ? 1 : 0);
    }

    @Override
    public Short getUpperBoundary() {
        return 1;
    }

    @Override
    public Short getLowerBoundary() {
        return 0;
    }

    @Override
    public Short getResolution() {
        return null;
    }
}
