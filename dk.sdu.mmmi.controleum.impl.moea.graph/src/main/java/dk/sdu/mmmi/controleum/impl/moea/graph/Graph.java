package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.*;
import static dk.sdu.mmmi.controleum.api.moea.graph.Selector.APPROX_FAIRNESS;
import static dk.sdu.mmmi.controleum.api.moea.graph.Selector.SOCIAL;

import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

/**
 * A directed graph data structure.
 *
 * @param <T>
 * @author Scott.Stark@jboss.org
 * @version $Revision$
 */
@SuppressWarnings("unchecked")
public class Graph<T> implements IGraph<T> {

    private GraphType graphType;
    /**
     * Color used to mark unvisited nodes
     */
    public static final int VISIT_COLOR_WHITE = 1;

    /**
     * Color used to mark nodes as they are first visited in DFS order
     */
    public static final int VISIT_COLOR_GREY = 2;

    /**
     * Color used to mark nodes after descendants are completely visited
     */
    public static final int VISIT_COLOR_BLACK = 3;

    /**
     * Vector<Vertex> of graph verticies
     */
    private List<IVertex<T>> verticies;

    /**
     * Vector<Edge> of edges in the graph
     */
    private List<IEdge<T>> edges;

    /**
     * The vertex identified as the root of the graph
     */
    private IVertex<T> rootVertex;

    private Comparator comp = null;
    private List<RelativeImportanceConcern> concerns = null;
    private String name = null;

    //qqq log
//    private FileWriter fw_analyze = null;
    private String str = "";
    private int count = 1;
    private Date currentTime;

    /**
     * Construct a new graph without any vertices or edges
     *
     * @param name
     * @param concerns
     */
    public Graph(String name, List<RelativeImportanceConcern> concerns) {
        this(name, concerns, new RelativeImportanceComparator());
    }

    public Graph(String name, List<RelativeImportanceConcern> concerns, Comparator comp) {
        this(name, concerns, comp, GraphType.CONCERN);
    }

    public Graph(String name, List<RelativeImportanceConcern> concerns, Comparator comp, GraphType graphType) {
        verticies = new ArrayList<>();
        edges = new ArrayList<>();
        this.name = name;
        this.concerns = concerns;
        this.comp = comp;
        this.graphType = graphType;
    }

    /**
     * Are there any verticies in the graph
     *
     * @return true if there are no verticies in the graph
     */
    @Override
    public boolean isEmpty() {
        return verticies.size() == 0;
    }

    public void buildGraph() {          //qqq build graph as a map: <node rank number> <concern>
        HashMap<Integer, List<RelativeImportanceConcern>> rankedConcerns = new HashMap<>();
        List<RelativeImportanceConcern> sortedConcerns = this.getConcerns();
        Comparator comparator = this.getComparator();
        sortedConcerns.sort(comparator);

        Iterator<RelativeImportanceConcern> caIt = sortedConcerns.iterator();
        Integer i = 0;
        RelativeImportanceConcern previousCa = null;
        while (caIt.hasNext()) {
            RelativeImportanceConcern curCa = caIt.next();
            if (previousCa == null) //First RelativeImportanceConcern
            {
                List<RelativeImportanceConcern> cas = new ArrayList<>();
                cas.add(curCa);
                rankedConcerns.put(i, cas);
            } else if (comparator.compare(curCa, previousCa) == 0) {
                rankedConcerns.get(i).add(curCa);
            } else {
                i++;
                List<RelativeImportanceConcern> cas = new ArrayList<>();
                cas.add(curCa);
                rankedConcerns.put(i, cas);
            }
            previousCa = curCa;
        }

        drawGraph(rankedConcerns);
    }

    public void drawGraph(HashMap<Integer, List<RelativeImportanceConcern>> rankedConcerns) {   //qqq draw graph as vertex(node) with its rank number
        Vertex root = new Vertex("root", rankedConcerns.get(0));
        this.addVertex(root);
        this.setRootVertex(root);
        Vertex v = null;

        for (Integer i : rankedConcerns.keySet()) {
            if (i.intValue() > 0) {
                v = new Vertex("N" + Integer.toString(i), rankedConcerns.get(i));
                this.addVertex(v);
                this.addEdge(root, v, -1);
                root = v;

            }

        }
    }

    /**
     * Search the verticies for one with data.
     *
     * @param data - the vertex data to match the comparator to perform the
     * match
     * @return the first vertex with a matching data, null if no matches are
     * found
     */
    @Override
    public IVertex<T> findVertexByData(T data) {
        IVertex<T> match = null;
        for (IVertex<T> v : verticies) {
            if (v.getData().contains(data)) {
                match = v;
                break;
            }

        }
        return match;
    }

    /**
     * Add a vertex to the graph
     *
     * @param v the Vertex to add
     * @return true if the vertex was added, false if it was already in the
     * graph.
     */
    private boolean addVertex(IVertex<T> v) {
        boolean added = false;
        if (verticies.contains(v) == false) {
            added = verticies.add(v);
        }
        return added;
    }

    /**
     * Get the vertex count.
     *
     * @return the number of verticies in the graph.
     */
    @Override
    public int size() {
        return verticies.size();
    }

    /**
     * Get the root vertex
     *
     * @return the root vertex if one is set, null if no vertex has been set as
     * the root.
     */
    @Override
    public IVertex<T> getRootVertex() {
        return rootVertex;
    }

    /**
     * Set a root vertex. If root does no exist in the graph it is added.
     *
     * @param root - the vertex to set as the root and optionally add if it does
     * not exist in the graph.
     */
    private void setRootVertex(IVertex<T> root) {
        this.rootVertex = root;
        if (verticies.contains(root) == false) {
            this.addVertex(root);
        }
    }

    /**
     * Get the given Vertex.
     *
     * @param n the index [0, size()-1] of the Vertex to access
     * @return the nth Vertex
     */
    private IVertex<T> getVertex(int n) {
        return verticies.get(n);
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Search the graph for cycles. In order to detect cycles, we use a modified
     * depth first search called a colored DFS. All nodes are initially marked
     * white. When a node is encountered, it is marked grey, and when its
     * descendants are completely visited, it is marked black. If a grey node is
     * ever encountered, then there is a cycle.
     *
     * @return the edges that form cycles in the graph. The array will be empty
     * if there are no cycles.
     */
    @Override
    public ArrayList findCycles() {
        ArrayList<IEdge<T>> cycleEdges = new ArrayList<>();
        // Mark all verticies as white
        for (int n = 0; n < this.getVerticies().size(); n++) {
            IVertex<T> v = this.getVertex(n);
            v.setMarkState(VISIT_COLOR_WHITE);
        }

        for (int n = 0; n < this.getVerticies().size(); n++) {
            IVertex<T> v = this.getVertex(n);
            this.visit(v, cycleEdges);
        }

        Edge<T>[] cycles = new Edge[cycleEdges.size()];
        cycleEdges.toArray(cycles);
        return cycleEdges;
    }

    /**
     * Get the graph verticies
     *
     * @return the graph verticies
     */
    private List<IVertex<T>> getVerticies() {
        return this.verticies;
    }

    /**
     * Insert a directed, weighted Edge<T> into the graph.
     *
     * @param from - the Edge<T> starting vertex
     * @param to - the Edge<T> ending vertex
     * @param cost - the Edge<T> weight/cost
     * @return true if the Edge<T> was added, false if from already has this
     * Edge<T>
     * @throws IllegalArgumentException if from/to are not verticies in the
     * graph
     */
    private boolean addEdge(IVertex<T> from, IVertex<T> to, int cost) throws IllegalArgumentException {
        if (verticies.contains(from) == false) {
            throw new IllegalArgumentException("from is not in graph");
        }
        if (verticies.contains(to) == false) {
            throw new IllegalArgumentException("to is not in graph");
        }

        IEdge<T> e = new Edge<T>(from, to, cost);
        if (from.findEdgeTo(to) != null) {
            return false;
        } else {
            from.addEdge(e);
            to.addEdge(e);
            edges.add(e);
            return true;
        }
    }

    /**
     * Get the graph edges
     *
     * @return the graph edges
     */
    @Override
    public List<IEdge<T>> getEdges() {
        return this.edges;
    }

    @Override
    public boolean getEdge(IVertex<T> from, IVertex<T> to) {    //qqq judge whether there is an edge between two vertexs
        IEdge<T> e = from.findEdgeTo(to);
        if (e == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Clear the mark state of all verticies in the graph by calling clearMark()
     * on all verticies.
     *
     * @see Vertex#clearMark()
     */
    @Override
    public void clearMark() {
        for (IVertex<T> w : verticies) {
            w.clearMark();
        }
    }

    /**
     * Clear the mark state of all edges in the graph by calling clearMark() on
     * all edges.
     */
    @Override
    public void clearEdges() {
        for (IEdge<T> e : edges) {
            e.clearMark();
        }
    }

    /**
     * Perform a depth first search using recursion.
     *
     * @param v - the Vertex to start the search from
     * @param visitor - the visitor to inform prior to
     * @param minMaxFitnessValues
     * @param rSelector
     * @return
     */
    @Override
    public List<ISolution> depthFirstSearch(IVertex v, IVisitor visitor, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector) {    //qqq v: root, rSelector: SOCIAL
        //qqq   root vertex, DepthFristVisitor,     minMax fitness of 7 concerns,            SOCIAL

        List<ISolution> pop = null;

        //qqq only for root
        if (visitor != null) {
            //TODO //qqq log
//            try {
//                if (fw_analyze != null) {
//                    visitor.setLogFile(fw_analyze);
//                }
//                str = String.format("\n\n=============== %s Vertex ===============\n\n", v.getName());
//                fw_analyze.write(str);
//            } catch (IOException ex) {
//                Exceptions.printStackTrace(ex);
//            }
            /////////////////////////

            switch (v.getName()) {      //qqq add, different nodes can utilize individule SW
                case "root":
                    rSelector = SOCIAL;
                    break;
                case "N1":
                    rSelector = SOCIAL;
                    break;
                case "N2":
                    rSelector = SOCIAL;
                    break;
                default:
                    rSelector = SOCIAL;
                    break;
            }

            pop = visitor.visit(v, minMaxFitnessValues, rSelector);     //qqq !!! entry to "visit" in class "DepthFirstVisitor", return the sub Pareto on the current vertex
        }

        if (!(pop.size() > 1)) {
            return pop;
        }

        //qqq for other vertex after root, recursion
        for (int i = 0; i < v.getOutgoingEdgeCount(); i++) {
            IEdge<T> e = v.getOutgoingEdge(i);
            {
                DepthFirstVisitor df = new DepthFirstVisitor(pop);
                return depthFirstSearch(e.getTo(), df, minMaxFitnessValues, rSelector);
            }
        }

        return pop;
    }

    @Override
    public Document depthFirstSearch(IVertex v, IVisitor visitor) throws VisitorException {
        Document doc = null;

        try {
            doc = visitor.visit(v, createNewDOM());
            for (int i = 0; i < v.getOutgoingEdgeCount(); i++) {
                IEdge<T> e = v.getOutgoingEdge(i);
                doc = visitor.visit(e.getTo(), doc);

            }
        } catch (ParserConfigurationException ex) {
            VisitorException e = new VisitorException("Unable to create document.");
            e.addSuppressed(ex);
            throw e;
        }

        return doc;
    }

    /**
     * Perform a breadth first search of this graph, starting at v. The vist may
     * be cut short if visitor throws an exception during a vist callback.
     *
     * @param v -the search starting point
     * @param visitor - the vistor whose vist method is called prior to visting
     * a vertex.
     */
    @Override
    public void breadthFirstSearch(IVertex<T> v, IVisitor<T> visitor) throws VisitorException {
        LinkedList<IVertex<T>> q = new LinkedList<>();
        Document doc = null;
        q.add(v);
        if (visitor != null) {
            try {
                doc = visitor.visit(v, createNewDOM());
            } catch (ParserConfigurationException ex) {
                VisitorException e = new VisitorException("Unable to create document.");
                e.addSuppressed(ex);
                throw e;
            }
        }
        v.visit();
        while (q.isEmpty() == false) {
            v = q.removeFirst();
            for (int i = 0; i < v.getOutgoingEdgeCount(); i++) {
                IEdge<T> e = v.getOutgoingEdge(i);
                IVertex<T> to = e.getTo();
                if (!to.visited()) {
                    q.add(to);
                    if (visitor != null) {
                        doc = visitor.visit(to, doc);
                    }
                    to.visit();
                }
            }
        }
    }

    /**
     * Find the spanning tree using a DFS starting from v.
     *
     * @param v - the vertex to start the search from
     * @param visitor - visitor invoked after each vertex is visited and an edge
     * is added to the tree.
     */
    @Override
    public void dfsSpanningTree(IVertex<T> v, IDFSVisitor<T> visitor) {
        v.visit();
        if (visitor != null) {
            visitor.visit(this, v);
        }

        for (int i = 0; i < v.getOutgoingEdgeCount(); i++) {
            IEdge<T> e = v.getOutgoingEdge(i);
            if (!e.getTo().visited()) {
                if (visitor != null) {
                    visitor.visit(this, v, e);
                }
                e.mark();
                dfsSpanningTree(e.getTo(), visitor);
            }
        }
    }

    public int findCycles2() {
        ArrayList<IEdge<T>> cycleEdges = new ArrayList<>();
        // Mark all verticies as white
        for (int n = 0; n < verticies.size(); n++) {
            IVertex<T> v = getVertex(n);
            v.setMarkState(VISIT_COLOR_WHITE);
        }
        for (int n = 0; n < verticies.size(); n++) {
            IVertex<T> v = getVertex(n);
            visit(v, cycleEdges);
        }

        Edge<T>[] cycles = new Edge[cycleEdges.size()];
        //System.out.println(cycleEdges.get(0).getFrom()+" to "+ cycleEdges.get(0).getTo()+"="+cycleEdges.get(0).getCost());

        cycleEdges.toArray(cycles);
        //System.out.println("Size="+cycleEdges.size());
        if (cycles.length > 0) {
            System.out.println(cycleEdges.get(0).getFrom().getData() + " to " + cycleEdges.get(0).getTo().getData() + "=" + cycleEdges.get(0).getCost());
        }
        return cycles.length;
    }

    public void visit(IVertex<T> v, ArrayList<IEdge<T>> cycleEdges) {
        v.setMarkState(VISIT_COLOR_GREY);
        int count = v.getOutgoingEdgeCount();
        for (int n = 0; n < count; n++) {
            IEdge<T> e = v.getOutgoingEdge(n);
            IVertex<T> u = e.getTo();
            if (u.getMarkState() == VISIT_COLOR_GREY) {
                // A cycle Edge<T>
                cycleEdges.add(e);
            } else if (u.getMarkState() == VISIT_COLOR_WHITE) {
                visit(u, cycleEdges);
            }
        }
        v.setMarkState(VISIT_COLOR_BLACK);
    }

    @Override
    public String toString() {
        StringBuffer tmp = new StringBuffer("Graph[");
        for (IVertex<T> v : verticies) {
            tmp.append(v.getName() + "=" + v.getData().toString() + ", " + v.getIncomingEdges() + ", " + v.getOutgoingEdges());
            tmp.append(']');
            tmp.append("\n");
        }

        return tmp.toString();
    }

    @Override
    public List<RelativeImportanceConcern> getConcerns() {
        return concerns;
    }

    private Comparator getComparator() {
        return comp;
    }

    public void setComparator(Comparator comparator) {     //qqq add
        comp = comparator;
    }

    @Override
    public GraphType getGraphType() {
        return graphType;
    }

    private Document createNewDOM() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("GraphXML");

        doc.appendChild(rootElement);
        return doc;
    }

    public void setTime(Date time) {
        this.currentTime = time;
    }

    public void setLogFile(FileWriter file) {   //qqq add
//        this.fw_analyze = file;
    }
}
