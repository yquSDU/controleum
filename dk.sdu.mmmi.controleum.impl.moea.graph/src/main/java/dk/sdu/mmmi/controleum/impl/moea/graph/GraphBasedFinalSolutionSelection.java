package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphSolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphType;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.decouplink.Utilities.context;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Date;
import org.openide.util.Exceptions;

/**
 *
 * @author AIU
 */
public class GraphBasedFinalSolutionSelection implements GraphSolutionSelector {

    List<ISolution> pop = new ArrayList<>();
    Graph g;
    ControleumContext cd = null;
    GraphUtil gUtil = new GraphUtil();
    Map<Concern, MinMax> minMaxFitnessValues;
    private FileWriter fw = null;    //qqq log

    @Override
    public ISolution getSolution(List<ISolution> solPopulation, Selector rSelector, Date time) {       //TODO //qqq add "Date time"

        this.pop = solPopulation;
        cd = (ControleumContext) solPopulation.get(0).getContext();
        for (Graph gIndex : context(cd).all(Graph.class)) //qqq get the graph defined in Plugin.java of package "rig"
        {
            if (gIndex.getGraphType().equals(GraphType.CONCERN)) {
                g = gIndex;
                g.setLogFile(this.fw);
            }
        }

        Collection<? extends RelativeImportanceConcern> concernCollection = context(cd).all(RelativeImportanceConcern.class);   //qqq get all 7 concerns
        List<RelativeImportanceConcern> concerns = new ArrayList<>(concernCollection);

        minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(solPopulation);

        //g=new Graph(g.getName(),concerns,new PreferenceImportanceComparator());     //TODO //qqq comment //qqq renew the graph defined in Plugin of "rig", with the new Comparator. Original comparator is RelativeImportanceComparator 
        //g.buildGraph();     //TODO //qqq comment //qqq (1) build graph as a map: <node rank number> <concern> (2) draw graph as vertex(node) with its rank number
        g.setComparator(new PreferenceImportanceComparator());        //TODO //qqq add
        g.setTime(time);    //qqq add
        System.out.println(cd.getName() + ": Post Negotiation " + g.toString());

        GraphAnalyzer analyser = new GraphAnalyzer(g);

        //Uncomment if analysis required
        /*  try {
                result = analyser.generateAnalysis();
            } catch (ParserConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
                if(!result)
                {
                    System.out.println("Analysis failed");
                }*/
        //System.out.println(cd.getName()+": Post Negotiation "+g.toString());
        DepthFirstVisitor df = new DepthFirstVisitor(pop);
        pop = g.depthFirstSearch(g.getRootVertex(), df, minMaxFitnessValues, rSelector);     //qqq !!! finish get final sub Pareto Front after filtering through the graph, rSelector == SOCIAL
        return pop.get(0);
    }

    @Override
    public Graph getGraph() {
        return g;
    }

    @Override
    public void setLogFile(FileWriter file) {   //qqq add
//        this.fw = file;
    }
}
