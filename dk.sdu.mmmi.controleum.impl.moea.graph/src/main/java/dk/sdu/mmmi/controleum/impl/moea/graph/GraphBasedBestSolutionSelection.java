package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphSolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphType;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.decouplink.Utilities.context;
import java.io.FileWriter;
import java.util.Date;

/**
 *
 * @author AIU
 */
public class GraphBasedBestSolutionSelection implements GraphSolutionSelector {

    
    List<ISolution> pop = new ArrayList<>();
    Graph g;
    ControleumContext cd = null;
    GraphUtil gUtil = new GraphUtil();
    Map<Concern, MinMax> minMaxFitnessValues;
    FileWriter fw = null;

    @Override
    public ISolution getSolution(List<ISolution> solPopulation, Selector rSelector, Date time) {       //TODO //qqq add "Date time"

        this.pop = solPopulation;
        cd = (ControleumContext) solPopulation.get(0).getContext();
        minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(solPopulation);
        for(Graph gIndex:context(cd).all(Graph.class))
        {
            if(gIndex.getGraphType().equals(GraphType.CONCERN))
            g = gIndex;
        }
        
        DepthFirstVisitor df=new DepthFirstVisitor(pop);
        pop=g.depthFirstSearch(g.getRootVertex(),df,minMaxFitnessValues, rSelector);
        return pop.get(0);
    }
    
    @Override
    public Graph getGraph()
    {
        return g;
    }
    
    @Override
    public void setLogFile(FileWriter file) {   //qqq add
//        if (this.fw == null) {
//            this.fw = file;
//        }
    }
}
