package dk.sdu.mmmi.controleum.impl.moea.graph;

import com.decouplink.Utilities;
import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.*;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.SocialSelector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import org.w3c.dom.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.impl.moea.comperator.PrioritizedNormalizedFitnessComparator;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.ApproxFairnessSelector;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.ElitistSelector;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.NashProductSelector;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import org.openide.util.Exceptions;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author AIU
 */
public class DepthFirstVisitor<T> implements IVisitor<T> {

    List<ISolution> solPopulation = new ArrayList<>();
    Map<ISolution, List<Double>> mSol_normFitnessList_accepted_thisV = new HashMap<>();
    Selector rSelector;
    GraphUtil gu = new GraphUtil();
    ListSolutionSelection prioritySelector = null;
    //qqq log
    private ControleumContext cd = null;        //qqq add
//    private static FileWriter fw_analyze = null;
    private String str = "";
    private Map<Concern, MinMax> minMaxInThisVertex = new HashMap<>();
    private Map<Concern, MinMax> minMaxAll = null;
    private List<Concern> concernInVertex = new ArrayList<>();
    private List<Concern> enabledConcerns = new ArrayList<>();
    Map<ISolution, Map<Concern, Double>> mSol_normFitnessMap_accepted_thisV = new HashMap<>();
    Map<ISolution, Map<Concern, Double>> mSol_normFitnessMap_all_thisV = new HashMap<>();
    Map<ISolution, List<Double>> mSol_normFitnessList_all_thisV = new HashMap<>();
    Map<ISolution, Double> mSol_normScoreSum_all_thisV = new HashMap<>();
    Map<ISolution, Boolean> mSold_Accepted_all = new HashMap<>();
    List<ISolution> Population_accepted_list = new ArrayList<>();
    List<ISolution> unacceptedSolutions = new ArrayList<>();

    public DepthFirstVisitor() {
        this.solPopulation = new ArrayList<>();
    }

    public DepthFirstVisitor(List<ISolution> solPopulation) {
        this.solPopulation = solPopulation;

    }

    @Override
    public List<ISolution> visit(IVertex<T> v, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector) {
        //qqq                   root vertex,    min max fitness of 7 concerns,              SOCIAL
        // if different selector is used for each vertex
        //this.rSelector=v.getSelector();

        //if same selector is used for all vertices
        this.rSelector = rSelector;
        List<Double> values = new ArrayList<>();
        mSol_normFitnessList_accepted_thisV = new HashMap<>();
        List<T> data = v.getData();                                      //qqq data: root vertex concerns

        //qqq add
        boolean bAccepted = true;
        Map<Concern, Double> normalizedSolution = new HashMap<>();

        cd = (ControleumContext) solPopulation.get(0).getContext();      //qqq add, for getting all concerns
        Population_accepted_list = new ArrayList<>(solPopulation);
        mSol_normFitnessMap_accepted_thisV = new HashMap<>();
        mSol_normFitnessMap_all_thisV = new HashMap<>();
        mSol_normFitnessList_all_thisV = new HashMap<>();
        mSol_normScoreSum_all_thisV = new HashMap<>();

        //qqqq qLogConcerns(v, minMaxFitnessValues);        //qqq add, log concerns in the vertex//qqq add, for avoiding normalizedSolution.get(c) == null, otherwise, directly using concerns in data would cause !!! normalizedSolution.get(c) == null !!!
        /////////////////////////

        for (ISolution s : solPopulation) {
            bAccepted = true;
            normalizedSolution = NormalizationUtil.NormalizeSolution(s, minMaxFitnessValues);
            values = new ArrayList<>();     //qqq move the renew state here (from the end of this for loop)

            mSol_normFitnessMap_all_thisV.put(s, normalizedSolution);
            mSol_normFitnessList_all_thisV.put(s, values);

            for (Concern CC : concernInVertex) {               //qqq add, for avoiding normalizedSolution.get(c) == null
                values.add(normalizedSolution.get(CC));        //qqq using the concern from "context" rather than from "data"
                mSol_normFitnessList_all_thisV.put(s, values);

                //TODO //qqq add, only accepted solutions can survive in the candidate list ***********
                if ("Light interval" == CC.getName()) {
                    if (0.0 != normalizedSolution.get(CC)) {
                        bAccepted = false;
                        mSold_Accepted_all.put(s, FALSE);
                    }
                } else if ("Fixed light hours" == CC.getName()) {
                    if (0.0 != normalizedSolution.get(CC)) {
                        bAccepted = false;
                        mSold_Accepted_all.put(s, FALSE);
                    }
                }
            }
            if (true == bAccepted) {
                mSol_normFitnessList_accepted_thisV.put(s, values);
                mSol_normFitnessMap_accepted_thisV.put(s, normalizedSolution);
                mSold_Accepted_all.put(s, TRUE);
                ////////////////////////////////
            } else {
                unacceptedSolutions.add(s);
            }
        }
        if ((unacceptedSolutions != null) && (unacceptedSolutions.size() < solPopulation.size())) { //qqq remove the unaccepted solutions from the candidate list 
            for (ISolution ss : unacceptedSolutions) {
                Population_accepted_list.remove(ss);
            }
        }
        // *************************************************************************************************

        try {
            // parsing XML file and load Comparator //qqq "C:\Users\yqu\selector.xml"
            //prioritySelector = gu.parseXML_loadComparator(rSelector, mSol_normFitnessList_accepted_thisV, minMaxFitnessValues);    //TODO //qqq comment

            //TODO //qqq !!! add
            switch (rSelector) {
                case SOCIAL:
                    prioritySelector = new SocialSelector(mSol_normFitnessList_accepted_thisV, minMaxFitnessValues);
                    break;
                case APPROX_FAIRNESS:
                    prioritySelector = new ApproxFairnessSelector(mSol_normFitnessList_accepted_thisV, minMaxFitnessValues);
                    break;
                case ELITIST:
                    prioritySelector = new ElitistSelector(mSol_normFitnessList_accepted_thisV, minMaxFitnessValues);
                    break;
                case NASH_PRODUCT:
                    prioritySelector = new NashProductSelector(mSol_normFitnessList_accepted_thisV, minMaxFitnessValues);
                    break;
                default:
                    break;
            }
        } catch (/*InstantiationException | IllegalAccessException |*/IllegalArgumentException /*| InvocationTargetException*/ ex) {
            ex.printStackTrace();
        }

        //qLogSortInAnalyze(v, minMaxFitnessValues, rSelector);       //qqq add, log before filtering   //qqqq comment
        // selecting list of candidate solutions for each priority group
        List<ISolution> population = prioritySelector.select(Population_accepted_list);        //qqq filter the population with the concerns in the node v

        return population;
    }

    /**
     * //qqq log concerns in current vertex
     *
     * @author yqu
     */
    private void qLogConcerns(IVertex<T> v, Map<Concern, MinMax> minMaxFitnessValues) {
        List<T> data = v.getData();
        minMaxAll = new HashMap<>(minMaxFitnessValues);

        for (Concern CC : Utilities.context(cd).all(Concern.class)) {
            for (T c : data) {
                if (((Concern) c).getName().equals(CC.getName())) {
                    concernInVertex.add(CC);
                    minMaxInThisVertex.put(CC, minMaxFitnessValues.get(CC));
                }
            }
            if (CC.isEnabled()) {
                enabledConcerns.add(CC);
            }
        }
    }

    /**
     * //qqq log sub Pareto Front before filter in current vertex
     *
     * @author yqu
     */
    private void qLogSortInAnalyze(IVertex<T> v, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector) {
        List<ISolution> popForSort = new ArrayList<>(solPopulation);        //qqq log all solutions (before removing unaccepted solutions) in this node

        //qqq ------------- only for "Social selector" ------------------
        if (Selector.SOCIAL == rSelector) {
            PrioritizedNormalizedFitnessComparator BY_SOCIAL_BENEFIT = new PrioritizedNormalizedFitnessComparator(minMaxInThisVertex); //qqq the sort criteria: sort the sums of normalized fitness in ascending order
            Collections.sort(popForSort, BY_SOCIAL_BENEFIT);       //qqq in ascending order

            for (ISolution p : popForSort) {
                double f1 = p.getSummarizedEvaluationResult(mSol_normFitnessList_all_thisV.get(p), minMaxFitnessValues);
                mSol_normScoreSum_all_thisV.put(p, f1);
            }
        }

        //qqq ------------- only for "ApproxFair selector" --------------
        if (Selector.APPROX_FAIRNESS == rSelector) {

            for (ISolution p : popForSort) {
                double f1 = p.getApproxFairnessEvaluationResult(mSol_normFitnessList_all_thisV.get(p), minMaxFitnessValues);    //qqq for ApproxFairnessSelector
                mSol_normScoreSum_all_thisV.put(p, f1);
            }
        }

        //qqq ------------- only for "Elitist" --------------
        if (Selector.ELITIST == rSelector) {

            for (ISolution p : popForSort) {
                double f1 = p.getElitistEvaluationResult(mSol_normFitnessList_all_thisV.get(p), minMaxFitnessValues);    //qqq for ApproxFairnessSelector
                mSol_normScoreSum_all_thisV.put(p, f1);
            }
        }

        //qqq ------------- only for "Nash selector" --------------
        if (Selector.NASH_PRODUCT == rSelector) {

            for (ISolution p : popForSort) {
                double f1 = p.getNashEvaluationResult(mSol_normFitnessList_all_thisV.get(p), minMaxAll);    //qqq for NashSelector
                mSol_normScoreSum_all_thisV.put(p, f1);
            }
        }

        // sorting
        List<Map.Entry<ISolution, Double>> list = new ArrayList<Map.Entry<ISolution, Double>>(mSol_normScoreSum_all_thisV.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<ISolution, Double>>() {
            // acseding sorting
            @Override
            public int compare(Entry<ISolution, Double> o1,
                    Entry<ISolution, Double> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

        qLogWrite(v, list);
    }

    /**
     * //qqq write log into files
     *
     * @author yqu
     */
    private void qLogWrite(IVertex<T> v, List<Map.Entry<ISolution, Double>> sortList) {
        int count = 1;
//        try {
//            str = String.format("[selector]: %s\n\n"
//                    + "[concerns]:\n", rSelector.name());
//            fw_analyze.write(str);
//            for (Concern CC : enabledConcerns) {
//                str = String.format("%s\t minmax: [%s, %s]\n", (CC).getName(), minMaxAll.get(CC).getMin(), minMaxAll.get(CC).getMax());
//                fw_analyze.write(str);
//            }
//
//            str = String.format("\n---------------------------\n\n"
//                    + "size: %s\n\n", sortList.size());
//            fw_analyze.write(str);
//
//            for (int i = 0; i < sortList.size(); i++) {
//                ISolution p = sortList.get(i).getKey();
//                // light plan
//                str = String.format("\n[%s] %s",
//                        count, p.qToString_onlyLP());
//                fw_analyze.write(str);
//                // normalized score
//                if (TRUE == mSold_Accepted_all.get(p)) {
//                    str = String.format("* Accepted\n");
//                } else {
//                    str = String.format("* Unaccepted\n");
//                }
//                fw_analyze.write(str);
//                str = String.format("* normalized fitness score in node %s:   %s\n", v.getName(), mSol_normScoreSum_all_thisV.get(p));
//                fw_analyze.write(str);
//                // fitness
//                String parEqu = null;
//                for (Concern CC : enabledConcerns) {
//                    str = String.format("%s:  %s (%s)\n", CC.getName(), CC.evaluate(p), mSol_normFitnessMap_all_thisV.get(p).get(CC));
//                    fw_analyze.write(str);
//                    if (CC.getName() == "Achieve PAR sum balance") {
//                        parEqu = CC.qToString(p);
//                    }
//                }
//                fw_analyze.write(parEqu);
//
//                count++;
//            }
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
    }

    @Override
    public Document visit(IVertex v, Document doc) throws VisitorException {
        List<RelativeImportanceConcern> data = v.getData();

        Element rootElement = doc.getDocumentElement();

        // node elements
        Element agent = doc.createElement("node");
        rootElement.appendChild(agent);

        // set attribute to node element
        Attr attr = doc.createAttribute("name");
        attr.setValue(v.getName());
        agent.setAttributeNode(attr);
        Element concern = null;
        Element issue = null;
        for (RelativeImportanceConcern ca : data) {
            List<UUID> issues = ca.getIssueIds();
            for (UUID o : issues) {
                issue = checkIssueExists(doc, o, agent);
                if (issue == null) {
                    issue = doc.createElement("issue");
                    attr = doc.createAttribute("id");
                    attr.setValue(o.toString());
                    issue.setAttributeNode(attr);
                }

                concern = doc.createElement("concern");
                attr = doc.createAttribute("name");
                attr.setValue(ca.getName());
                concern.setAttributeNode(attr);

                attr = doc.createAttribute("preference_importance");
                //if (ca.getInput() != null) 
                {
                    attr.setValue(ca.getPreferenceImportance().toString());
                }
                //else {
                // attr.setValue("unconstrained");
                //}
                concern.setAttributeNode(attr);
                issue.appendChild(concern);
                agent.appendChild(issue);
            }
        }
        return doc;
    }

    public Element checkIssueExists(Document doc, UUID u, Element parent) {

        Element e = null;
        NodeList nList = doc.getElementsByTagName("issue");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                if ((eElement.getAttribute("id")).equals(u.toString()) && eElement.getParentNode().isEqualNode(parent)) {
                    e = eElement;
                    break;

                }

            }
        }
        return e;
    }

    @Override
    public void setLogFile(FileWriter file) {   //qqq add
//        this.fw_analyze = file;
    }
}
