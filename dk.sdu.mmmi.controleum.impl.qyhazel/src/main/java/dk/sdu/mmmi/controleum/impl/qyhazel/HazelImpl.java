/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dk.sdu.mmmi.controleum.impl.qyhazel;
import static com.decouplink.Utilities.context;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
//import com.hazelcast.org.json.JSONObject;
import com.hazelcast.topic.ITopic;
import com.hazelcast.topic.Message;
import com.hazelcast.topic.MessageListener;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import org.openide.util.Exceptions;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
//import org.json.simple.parser.ParseException;

/**
 *
 * @author yqu
 */
public class HazelImpl implements MessageListener<Object> {
    private ControleumContext g;
    private static final Logger logger = getLogger(HazelImpl.class.getName());
    private HazelcastInstance hzInstance;
    private ITopic<Object> topic;
    private String compartmentID;
    private JSONObject receivedMessage;
    private ArrayList<String> co2List = new ArrayList<String>();
    private ArrayList<String> temperatureList = new ArrayList<String>();
    private ArrayList<String> lightLevelList = new ArrayList<String>();
    private ArrayList<String> ArtificialLightStatusList = new ArrayList<String>();
    private ArrayList<Double> recAveTemperature = new ArrayList<Double>();//AverageTemperature
    private ArrayList<Double> recPARSum = new ArrayList<Double>();
    private ArrayList<Double> recCO2 = new ArrayList<Double>();
    
    ClientConfig cfg = new ClientConfig();
    String host = "10.93.0.204";
    String port = "5701";
    
    //qqq log
    String path = "C:\\Users\\yqu\\OneDrive - Syddansk Universitet\\Documents\\NetBeansProjects\\log\\hz\\";
    String file_name;
    private FileWriter fw_hz;
    private String str = "";
    private static Date currentTime;
    private static Date date_file = new Date();
    private static SimpleDateFormat dateFormat_file = new SimpleDateFormat("yyyy.MM.dd.HH.mm");
    private static String date_format = dateFormat_file.format(date_file);
    private int count_recmsg = 1;
    private int count_sendmsg = 1;
    
    public HazelImpl(ControleumContext g) {

        this.g = g;
        cfg.getNetworkConfig().addAddress(host + ":" + port);
        compartmentID = String.format("compartment_%s", g.getName());
        
        //qqq log
        try {
            file_name = path + String.format("receiveMsg_%s--%s.txt", g.getName(), date_format);
            this.fw_hz = new FileWriter(file_name, true);
            //fw_hz.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        ///////////////////////////////// 
       
    }

 
    public void startHazelcastInstance(){
   
        hzInstance = HazelcastClient.newHazelcastClient(cfg);
        System.out.println("start a hazel instance" + cfg.getInstanceName());
    }

    public void getTopic(String topic){

        this.topic = this.hzInstance.getTopic(compartmentID);
        this.topic.addMessageListener(this);
    }

    public void publishOnTopic(String topicName){

        ITopic<Object> _topic = this.hzInstance.getTopic(compartmentID);
        //_topic.publish(topicMsg);
        Object tm = (Object)packMsg().toString();
        
        ///////////////////qqq log
        try {
            str = String.format("\n\ntopic [%s] send msg: \n[%s] %s\n", _topic.getName(), count_sendmsg++, tm);
            fw_hz.write(str);
            fw_hz.close();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        _topic.publish(tm);
    }
    

    public Object packMsg() {
        
        JSONObject jsMsg = new JSONObject();
        JSONObject meta = new JSONObject();
        JSONObject climateState = new JSONObject();
        
        meta.put("sender_id", "climate_compartment_no5");//getClimateSenderID());
        meta.put("timestamp", getTimestamp());
        
        climateState.put("CO2", getCO2());
        climateState.put("temperature", getTemperature());
        climateState.put("PARsum", getLightLevel());
        climateState.put("artificialLightStatus", getArtificialLightStatus());
        
        jsMsg.put("meta", meta);
        jsMsg.put("climateState", climateState);
        
        return (Object)jsMsg;
    }
   
    
    String getClimateSenderID(){
        return compartmentID;
    }
    
    long getTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }
    
    ArrayList<String> getCO2() {
        ArrayList<Double> fakeCO2 = new ArrayList<Double>();
        
        for(int i=1; i<=72; i++){
            fakeCO2.add(i*0.5);
        }
        
        for(Double co2 : fakeCO2) {
            co2List.add(co2.toString());
        }
        
        return co2List;
    }
    
    ArrayList<String> getTemperature() {
        ArrayList<Double> fakeTemperature = new ArrayList<Double>();
        
        for(int i=1; i<=72; i++){
            fakeTemperature.add(i*0.5);
        }
        
        for(Double temperature : fakeTemperature) {
            temperatureList.add(temperature.toString());
        }
        
        return temperatureList;
    }
    
    ArrayList<String> getLightLevel() {
        ArrayList<Double> fakeLightLevel = new ArrayList<Double>();
        
        for(int i=1; i<=72; i++){
            fakeLightLevel.add(i*0.5);
        }
        
        for(Double lightLevel : fakeLightLevel) {
            lightLevelList.add(lightLevel.toString());
        }
        
        return lightLevelList;
    }
    
    ArrayList<String> getArtificialLightStatus() {
        ArrayList<Double> fakeArtificialLightStatus = new ArrayList<Double>();
        Double tstr = new Double(1);
        
        for(int i=1; i<=72; i++){
            if(i>36){
                tstr = new Double(0);
            }
            fakeArtificialLightStatus.add(tstr);
        }
        
        for(Double artificialLightStatus : fakeArtificialLightStatus) {
            ArtificialLightStatusList.add(artificialLightStatus.toString());
        }
        
        return ArtificialLightStatusList;
    }


    /* production -> climate message format
    {
	"payload": {
		"AverageTemperature": [22.41, 20.61, 20.77, 22.2, 20.81, 21.17, 21.03, 19.11, 20.21, 20.32, 20.3, 18.62, 18.47, 20.97, 19.56, 19.08, 20.58, 20.13, 20.17, 21.7, 21.88, 21.22, 20.28, 21.88, 21.64, 20.1, 21.78, 21.32, 20.76, 21.84, 20.04, 18.52, 19.9, 20.18, 19.26, 17.08, 19.26, 20.34, 19.17, 20.47, 20.47, 19.55, 19.98, 21.3, 22.08, 22.35, 20.32, 21.35, 22.21, 20.21, 21.29, 22.41, 20.51, 21.29, 20.81, 18.81, 20.29, 20.39, 19.32, 18.68, 20.69, 19.42, 18.7, 20.84, 20.75, 20.13, 19.68, 21.09, 22.19, 22.02, 19.9, 21.79],
		"PARSum": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 90.0, 90.0, 90.0, 104.55545000000001, 148.38175, 197.00655, 230.4361, 339.77792, 197.99824, 90.51184, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 90.0, 90.0, 90.67179, 245.56737, 608.1299, 836.76243, 496.10092000000003, 271.97898, 152.98831, 97.10178, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 90.0, 90.0, 90.0, 135.23386, 173.174, 249.95, 174.7735, 221.09502, 277.33344, 90.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"CO2": [661.59, 620.23, 604.06, 599.95, 567.1, 555.28, 561.37, 577.28, 555.29, 535.95, 551.2, 541.9, 575.1, 574.69, 570.57, 539.7, 519.0, 511.35, 520.83, 533.94, 535.88, 531.71, 524.73, 537.18, 525.73, 518.77, 537.98, 529.85, 517.3, 533.41, 521.41, 528.83, 507.63, 517.68, 484.84, 506.41, 482.33, 499.55, 498.91, 495.2, 510.25, 528.69, 531.0, 598.21, 615.31, 618.32, 603.87, 630.5, 660.42, 666.1, 676.55, 678.49, 635.23, 654.0, 654.38, 684.87, 678.69, 640.28, 653.44, 649.61, 640.16, 630.25, 601.73, 551.71, 564.02, 549.82, 548.42, 555.79, 577.81, 542.53, 541.72, 537.07]
	},
	"meta": {
		"sender_type": "logistics",
		"data_type": "simulation",
		"request_id": "climate_control_5_0",
		"sender_id": "climate_control_5",
		"timestamp": 1514934000
	}
    }
    */
    
    public void unpackMsg(JSONObject recMsg) {
        ArrayList<String> strAveTemperature = new ArrayList<String>();
        ArrayList<String> strPARSum = new ArrayList<String>();
        ArrayList<String> strCO2 = new ArrayList<String>();
        
        // unpack "meta"
        JSONObject recMeta = (JSONObject) receivedMessage.get("meta");
        String sender_type = (String) recMeta.get("sender_type");
        String data_type = (String) recMeta.get("data_type");
        String request_id = (String) recMeta.get("request_id");
        String sender_id = (String) recMeta.get("sender_id");
        String timestamp = (String) recMeta.get("timestamp");

        try {
            this.fw_hz = new FileWriter(file_name, true);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        // unpack logistics package
        if ("logistics" == sender_type) {
            // unpack "payload"
            JSONObject recPayload = (JSONObject) receivedMessage.get("payload");               
//          str = String.format("\n\nreceived msg: \n[%s] %s\n", count_recmsg++, receivedMessage);
//          fw_hz.write(str);

            strAveTemperature = (ArrayList<String>) recPayload.get("AverageTemperature");
            strPARSum = (ArrayList<String>) recPayload.get("PARsum");
            strCO2 = (ArrayList<String>) recPayload.get("CO2");
            
            // convert to double
            for (String st : strAveTemperature) {
                recAveTemperature.add(Double.parseDouble(st));
            }
            for (String st : strPARSum) {
                recPARSum.add(Double.parseDouble(st));
            }
            for (String st : strCO2) {
                recCO2.add(Double.parseDouble(st));
            }
            
            //qqq log
            try {
                str = String.format("recAveTemperature:%s\n", recAveTemperature);
                fw_hz.write(str);
                str = String.format("recPARSum:%s\n", recPARSum);
                fw_hz.write(str);
                str = String.format("recCO2:%s\n", recCO2);
                fw_hz.write(str);
                fw_hz.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            
            // setup par sum request
            
//                Date fromDate, 
//                        MolSqrMeter parSum
//            ClimateDataAccess db = context(g).one(ClimateDataAccess.class);
//            db.insertPARSumDayGoal(new Sample<>(fromDate, result));
        }
        else if ("climate" == sender_type){
            
        }
    }
    
    
    @Override
    public void onMessage(Message<Object> message) {

        JSONParser parser = new JSONParser();

        try {
            receivedMessage = (JSONObject) parser.parse(message.getMessageObject().toString());
            unpackMsg(receivedMessage);
            
        } catch (org.json.simple.parser.ParseException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
