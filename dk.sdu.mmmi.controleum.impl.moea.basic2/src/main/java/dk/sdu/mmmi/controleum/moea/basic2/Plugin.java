package dk.sdu.mmmi.controleum.moea.basic2;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssueConfig;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import java.util.logging.Logger;

import dk.sdu.mmmi.controleum.api.moea.Solver;
import org.openide.util.lookup.ServiceProvider;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import java.util.UUID;

/**
 *
 * @author ancla
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private static final Logger LOGGER = Logger.getLogger(Plugin.class.getName());

    @Override
    public Disposable create(ControleumContext g, UUID issueId) {   //qqq add UUID issueId
        DisposableList d = new DisposableList();

        System.out.println("Creating new solver.");
        SolverImpl2 s = new SolverImpl2(g);
        d.add(context(g).add(Solver.class, s));
        System.out.println("Solver " +  s + " added.");

        return d;
    }
}
