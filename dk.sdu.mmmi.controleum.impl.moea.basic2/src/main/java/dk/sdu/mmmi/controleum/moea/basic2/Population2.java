package dk.sdu.mmmi.controleum.moea.basic2;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Issue;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.ancla.IParetoFront;
import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulation;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphSolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.IGraph;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;
import dk.sdu.mmmi.controleum.impl.moea.graph.GraphBasedBestSolutionSelection;
import dk.sdu.mmmi.controleum.impl.moea.graph.GraphBasedFinalSolutionSelection;
import dk.sdu.mmmi.controleum.impl.moea.paretofront.ParetoFront;
import dk.sdu.mmmi.controleum.impl.moea.solution.Option;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.ApproximatedFairnessSelectorI;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.MaximizeSocialWelfareSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.IGraphPopulation;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import dk.sdu.mmmi.controleum.impl.moea.comperator.PrioritizedFitnessComparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javolution.util.FastTable;

import java.util.*;
import org.openide.util.Exceptions;

/**
 *
 * @author mrj & jcs
 */
class Population2 implements IGraphPopulation {

    //private final Object problem;
    /**
     * Pareto front
     */
    public static final double EPS = 1e-10;

    private IParetoFront pop; // best first

    List<ISolution> candidatePopulation = new ArrayList<>();

    private boolean evolved = false;

    private final List<Concern> concerns;

    private final int localPopSize = 500;
    /*
     * Final selection
     */
    private final ISolutionSelector selector = new MaximizeSocialWelfareSelector();

    private final SearchConfiguration cfg;

    private final static Random R = new Random();
    private static final ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();

    //qqq ancl for graph
    private Selector s;
    private IGraph graph;
    private final GraphSolutionSelector selectorBest = new GraphBasedBestSolutionSelection();
    private final GraphSolutionSelector selectorFinal = new GraphBasedFinalSolutionSelection();

    //qqq log
    String path = "C:\\Users\\yqu\\OneDrive - Syddansk Universitet\\Documents\\NetBeansProjects\\log\\rig_test\\";
    private static FileWriter fw_analyze = null;
    private String str = "";
    private static Date currentTime;
    //qqq compare v1 PF
    String file_v1_PF = String.format("C:\\Users\\yqu\\OneDrive - Syddansk Universitet\\Documents\\NetBeansProjects\\log\\compare_v1_PF\\v1_PF_Nov1_PARdg10.txt");
    String file_rig_PF = String.format("C:\\Users\\yqu\\OneDrive - Syddansk Universitet\\Documents\\NetBeansProjects\\log\\compare_v1_PF\\rig_PF_Nov1_PARdg10.txt");
    FileReader fr_v1 = null;
    FileReader fr_rig = null;
    String file_compare_result = String.format("C:\\Users\\yqu\\OneDrive - Syddansk Universitet\\Documents\\NetBeansProjects\\log\\compare_v1_PF\\compare_result_Nov1_PARdg10.txt");
    private static FileWriter fw_compareV1 = null;
    private List<String> list_read_string = new ArrayList<>();
    private List<String> list_read_string_rig = new ArrayList<>();
    List<ISolution> v1_PF = new ArrayList<>();
    List<ISolution> rig_PF = new ArrayList<>();
    static boolean bOnlyOnce = true;
    boolean v1HasBeenRemoved = false;
    /////////////////////////////////////////

    /**
     * Create initial population.
     */
    public Population2(Object problem, Date time, IPopulation reuse, SearchConfiguration cfg, List<Concern> concerns) {
        this.cfg = cfg;
        this.currentTime = time;     //qqq add
        this.pop = new ParetoFront(new FastTable<>(), cfg);
        this.concerns = concerns;

        if (reuse != null) {
            for (ISolution oldSolution : reuse.getPopulation()) {
                pop.add((ISolution) oldSolution.copy(problem, time));
            }
        } else {
            pop.addAll(generateRandomSolutions(problem, time));
        }
        evolved = true;
    }

    private List<ISolution> generateRandomSolutions(Object problem, Date time) {
        // Use randomness
        List<ISolution> ranPop = new ArrayList<>();
        for (int i = 0; i < localPopSize; i++) {
            ranPop.add(Option.random(problem, time, getConcerns()));
        }
        return ranPop;
    }

    /**
     * Evolve new population.
     */
    private Population2(IPopulation old) {
        this.cfg = old.getSearchConfiguration();
        this.concerns = old.getConcerns();
        this.pop = old.getFront();
        //this.sortedPop = old.getSortedPop();
        // Determine number of tries for mutation and crossover
        candidatePopulation.clear();
        geneticOperation2();
        //System.out.println("C1 " +  c1 + " C2 " + c2);
        if (candidatePopulation.size() > 0) {
            evolved = pop.addAll(candidatePopulation);
        }

        //       System.out.println("Population2 size: " + pop.getPop().size());
    }

    private void geneticOperation2() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (int i = 0; i <= localPopSize; i++) {

            if (R.nextDouble() < cfg.getMutationRate()) {
                // Mutate.
                ISolution parent = randomElement(paretoFrontCopy);
                ISolution child = (ISolution) parent.mutate();
                if (checkDominate(child, parent)) {
                    candidatePopulation.add(child);
                }

            } else {
                // Crossover.
                ISolution parentA = randomElement(paretoFrontCopy);
                ISolution parentB = randomElement(paretoFrontCopy);
                ISolution child = (ISolution) parentA.crossover(parentB);
                if (checkDominate(child, parentA) && checkDominate(child, parentB)) {
                    candidatePopulation.add(child);
                }
            }
        }
    }

    /*
    * Experimental implementation of genetic operation which maps to the conventional way of doing crossover and mutation in GAs.
    * Seems to underperform with domain specific operators compared to the geneticOperation2 implementation (Jan's implementation).
     */
    private void geneticOperation() {
        int c1 = 0;
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (int i = 0; i < paretoFrontCopy.size() * cfg.getCrossoverRate(); i++) {
            c1++;
            ISolution parentA = randomElement(paretoFrontCopy);
            ISolution parentB = randomElement(paretoFrontCopy);
            ISolution child = (ISolution) parentA.crossover(parentB);
            if (checkDominate(child, parentA) && checkDominate(child, parentB)) {
                candidatePopulation.add(child);
            }
        }
        int c2 = 0;
        for (int i = 0; i < Math.ceil(paretoFrontCopy.size() * cfg.getMutationRate2()); i++) {
            // Mutate.
            c2++;
            ISolution parent = randomElement(paretoFrontCopy);
            ISolution child = (ISolution) parent.mutate();
            if (checkDominate(child, parent)) {
                candidatePopulation.add(child);
            }
        }
    }

    private boolean checkDominate(ISolution child, ISolution parent) {
        int flag = PARETO_FITNESS.compare(child, parent);
        if (flag > 0) {
            return false;
        } else {
            return !(NormalizationUtil.Distance(child, parent) < EPS);
        }
    }

    @Override
    public IPopulation evolve() {
        return new Population2(this);
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getBestSolution() {
        return selector.getSolution(pop.getCopy());
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getWorstSolution() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        paretoFrontCopy.sort(BY_PRIORITIZED_FITNESS);
        return paretoFrontCopy.get(paretoFrontCopy.size() - 1);
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getMeanSolution() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        paretoFrontCopy.sort(BY_PRIORITIZED_FITNESS);
        return paretoFrontCopy.get(paretoFrontCopy.size() / 2);
    }

    @Override
    public List<Double> getBestFitness() {
        return getBestSolution().getEvaluationValues();
    }

    @Override
    public List<Double> getWorstFitness() {
        return getWorstSolution().getEvaluationValues();
    }

    @Override
    public List<Double> getMeanFitness() {
        return getMeanSolution().getEvaluationValues();
    }

    @Override
    public double getFitnessVariance() {
        double sum = 0;
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (ISolution s : paretoFrontCopy) {
            double diff = NormalizationUtil.Distance(getMeanSolution(), s);
            sum += diff;
        }
        return sum / (double) paretoFrontCopy.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("population = { ");
        sb.append("size : ");
        sb.append(getSize());
        sb.append(", ");
        sb.append("solutions : {\n");
        for (ISolution solution : pop.getCopy()) {
            sb.append(solution);
            sb.append(",\n");
        }
        sb.append("}");

        return sb.toString();
    }

    @Override
    public int getSize() {
        return pop.getCopy().size();
    }

    //
    // Misc.
    //
    private <T> T randomElement(List<T> list) {
        return list.get(R.nextInt(list.size()));
    }

    //
    // Comparators.
    //
    private final Comparator<ISolution> BY_PRIORITIZED_FITNESS = new PrioritizedFitnessComparator();

    @Override
    public List<ISolution> getPopulation() {
        return pop.getCopy();
    }

    @Override
    public SearchConfiguration getSearchConfiguration() {
        return cfg;
    }

    /**
     * @return the concerns
     */
    @Override
    public List<Concern> getConcerns() {
        return concerns;
    }

    /**
     * @return the evolved
     */
    @Override
    public boolean isEvolved() {
        return evolved;
    }

    @Override
    public IParetoFront getFront() {
        return pop;
    }

    @Override
    public ISolution getFairSolution() {
        ISolutionSelector fairSelector = new ApproximatedFairnessSelectorI();
        return fairSelector.getSolution(pop.getCopy());
    }

    @Override
    public ISolution getMaxSocialBenefitSolution() {
        ISolutionSelector maxBenefitSelector = new MaximizeSocialWelfareSelector();
        return maxBenefitSelector.getSolution(pop.getCopy());
    }

    @Override
    public ISolution getBestGraphSolution(Selector s) {     //qqq no use
        this.s = s;
        ISolution ss = selectorBest.getSolution(pop.getCopy(), s, currentTime);        //TODO //qqq add "Date time"
        this.graph = selectorBest.getGraph();
        return ss;
    }

    @Override
    public ISolution getFinalGraphSolution(Selector s) {        //qqq replace getBestSolution() for final selection
        this.s = s;
        //qqq log
        str = String.format("\n\n\n*******************************************************************************\n"
                + "************************ %s ******************************\n"
                + "*******************************************************************************\n\n",
                currentTime);
//        try {
//            this.fw_analyze.write(str);
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//        selectorFinal.setLogFile(this.fw_analyze);
        ///////////////////////////
        //TODO //qqq add for compare v1 PF
        if (bOnlyOnce) {
            compareV1PF(pop.getCopy());
            bOnlyOnce = false;
        }
        ///////////////////////////

        ISolution ss = selectorFinal.getSolution(pop.getCopy(), s, currentTime);   //qqq s == SOCIAL        //TODO //qqq add "Date time"
        this.graph = selectorFinal.getGraph();
        return ss;
    }

    @Override
    public Selector getSelector() {
        return this.s;
    }

    @Override
    public Object getGraph() {
        return this.graph;
    }

    @Override
    public IGraphPopulation evolveAsGraphPop() {
        return new Population2(this);
    }

    @Override
    public void setLogFile(FileWriter file) {   //qqq add
//        this.fw_analyze = file;
    }

    @Override
    public void compareV1PF(List<ISolution> pf_v2) {

        int count = 0;
        IParetoFront v2_PF_afterCompare = pop;

        /*try {
            // read v1 PF
            fw_compareV1 = new FileWriter(file_compare_result, false);
            fr_v1 = new FileReader(file_v1_PF);
            BufferedReader br_v1 = new BufferedReader(fr_v1);
            String lineContent = null;

            while ((lineContent = br_v1.readLine()) != null) {     // read v1 PF
                list_read_string.add(lineContent);
                count++;
            }

            ArrayList<Issue> keys = new ArrayList<>(pop.getCopy().get(0).getIssueValues().keySet());
            Issue issue = keys.get(0);
            int size = list_read_string.size();

            // log original v1 PF
            /*fw_compareV1.write(String.format("origianl v1 PF size = %s\n\n", count));
            count = 1;
            for (String linestr : list_read_string) {
                fw_compareV1.write(String.format("[%s] [%s]\n\n", count, linestr));
                count++;
            }* /
            // read rig PF
            fr_rig = new FileReader(file_rig_PF);
            BufferedReader br_rig = new BufferedReader(fr_rig);
            String lineContent_rig = null;

            while ((lineContent_rig = br_rig.readLine()) != null) {     // read v1 PF
                list_read_string_rig.add(lineContent_rig);
            }

            int size_rig = list_read_string_rig.size();

            // convert strings to solutions (v1 PF)
            Object problem = pf_v2.get(0).qGetProblem();
            Date time = pf_v2.get(0).getTime();
            ISolution s = Option.random(problem, time, concerns);
            for (int i = 0; i < list_read_string.size(); i++) {
                ISolution ss = (ISolution) s.convertStringToLP(problem, time, concerns, list_read_string.get(i));
                v1_PF.add(ss);
            }

            // convert strings to solutions (rig PF)
            ISolution s_rig = Option.random(problem, time, concerns);
            for (int i = 0; i < list_read_string_rig.size(); i++) {
                ISolution ss = (ISolution) s_rig.convertStringToLP(problem, time, concerns, list_read_string_rig.get(i));
                rig_PF.add(ss);
            }

            // compare v1 PF with rig PF
            fw_compareV1.write(String.format("---------- compare results (%s) -----------\n\n", pop.getCopy().get(0).getTime()));
            if (v1_PF.size() > 0) {
                //v2_PF_afterCompare.compV1PF(fw_compareV1, v1_PF);
                implementCompare();
            }

            // close all files
            br_v1.close();
            fr_v1.close();
            fw_compareV1.close();
            br_rig.close();
            fr_rig.close();

        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }*/
    }

    private void implementCompare() {

        boolean dominate1 = false;
        boolean dominate2 = false;
        boolean compForOnceUnfinished = true;

        List<ISolution> rig_PF_forComp = new ArrayList<>(rig_PF);
        int count = 1;

        try {
            fw_compareV1.write(String.format("rig PF original size =\t\t%s\n", rig_PF_forComp.size()));

            for (ISolution s_v1 : v1_PF) {
                v1HasBeenRemoved = false;
                int count_rig = 0;

                fw_compareV1.write(String.format("[%s]%s", count++, s_v1.qToString_onlyLP()));

                if (rig_PF.contains(s_v1)) {
                    fw_compareV1.write(String.format("rig PF contains\n"));
                } else {
                    List<Double> f1 = s_v1.getEvaluationValues();

                    for (ISolution s_v2 : rig_PF) {
                        count_rig++;
                        dominate1 = false;
                        dominate2 = false;

                        if (!v1HasBeenRemoved) {
                            List<Double> f2 = s_v2.getEvaluationValues();

                            if (f1.size() != f2.size()) {
                                throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
                            }

                            compForOnceUnfinished = true;
                            for (int i = 0; i < f1.size(); i++) {
                                if (f1.get(i) < f2.get(i)) {
                                    dominate1 = true;

                                    if (dominate2) {
                                        compForOnceUnfinished = false;
                                    }
                                } else if (f1.get(i) > f2.get(i)) {
                                    dominate2 = true;

                                    if (dominate1) {
                                        compForOnceUnfinished = false;
                                    }
                                }
                            }

                            if (compForOnceUnfinished) {
                                if (dominate1 == dominate2) {
                                    if (checkEquals(s_v1, s_v2) == 0) {
                                        if (!rig_PF.contains(s_v1)) {
                                            fw_compareV1.write(String.format("rig PF contains\n"));
                                        }
                                    }
                                } else if (dominate1) {
                                    if (rig_PF.contains(s_v2)) {
                                        rig_PF_forComp.remove(s_v2);
                                    }
                                } else {
                                    v1HasBeenRemoved = true;
                                }
                            }
                        }
                    }

                    //log
                    if (v1HasBeenRemoved) {
                        fw_compareV1.write(String.format("is dominated by rig's PF (cannot be added to rig's PF)\n"));
                    } else {
                        fw_compareV1.write(String.format("non-dominated (can be added to rig's PF)\n"));
                    }
                }
            }

            fw_compareV1.write("------------- after compare ------------\n\n");
            fw_compareV1.write(String.format("rig PF size after compare =\t%s\n", rig_PF_forComp.size()));

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private int checkEquals(Solution solution1, Solution solution2) {
        if (solution1.equals(solution2)) {
            return 1;
        } else {
            return 0;
        }
    }
}
