package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.util.Properties;
import org.moeaframework.core.Algorithm;
import org.moeaframework.core.Problem;
import org.moeaframework.core.spi.AlgorithmProvider;
import org.moeaframework.util.TypedProperties;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = AlgorithmProvider.class)
public class ControleumAlgorithmProvider extends AlgorithmProvider {

    @Override
    public Algorithm getAlgorithm(String name, Properties properties, Problem problem) {
        TypedProperties typedProperties = new TypedProperties(properties);
        Algorithm algorithmAdapter = null;

        Solver solver = context(name).one(Solver.class);

        if (solver != null
                && (name.equalsIgnoreCase("ControleumGA")
                || name.equalsIgnoreCase("ControleumGA"))) {

            algorithmAdapter = new ControleumAlgorithmAdapter(solver, problem);
        }

        return algorithmAdapter;
    }

}
