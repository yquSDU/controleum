package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.io.NotSerializableException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.moeaframework.core.Algorithm;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;

/**
 * Adapter for Controleum algorithm. This allows for the Controleum algorithm to
 * be used within the MOEAFramework.
 *
 * @author jcs
 */
public class ControleumAlgorithmAdapter implements Algorithm, SearchObserver {

    private final Solver algorithmAdaptee;
    private final ControleumProblemAdapter problemAdapter;
    private int numOfEvals = 0;
    private boolean isTerminated = false;

    public ControleumAlgorithmAdapter(Solver solver, Problem problem) {
        super();
        this.problemAdapter = new ControleumProblemAdapter(problem);
        this.algorithmAdaptee = solver;


        context(problem).add(SearchObserver.class, this);

    }

    @Override
    public Problem getProblem() {
        return problemAdapter.getProblem();
    }

    @Override
    public NondominatedPopulation getResult() {
        NondominatedPopulation result = new NondominatedPopulation();
        List<dk.sdu.mmmi.controleum.api.moea.Solution> solutionSet
                = algorithmAdaptee.getSolutionList();

        Date d = new Date();
        if (solutionSet != null) {
            for (int i = 0; i < solutionSet.size(); i++) {
                dk.sdu.mmmi.controleum.api.moea.Solution s = solutionSet.get(i);
                s.commit(d);
                result.add(problemAdapter.translate(s));
            }
        }

        return result;
    }

    @Override
    public void step() {
        if (!isTerminated()) {
            algorithmAdaptee.solve(new Date());
        }
    }

    @Override
    public void evaluate(Solution solution) {
        problemAdapter.getProblem().evaluate(solution);
        numOfEvals++;
    }

    @Override
    public int getNumberOfEvaluations() {

        return numOfEvals;
    }

    @Override
    public boolean isTerminated() {

        return isTerminated;
    }

    @Override
    public void terminate() {
        this.isTerminated = true;
    }

    @Override
    public Serializable getState() throws NotSerializableException {
        throw new NotSerializableException("Not supported yet.");
    }

    @Override
    public void setState(Object o) throws NotSerializableException {
        throw new NotSerializableException("Not supported yet.");
    }

    @Override
    public void onSearchCompleted(dk.sdu.mmmi.controleum.api.moea.Solution s) {
        this.isTerminated = true;
    }

    public void setNumberOfEvaluations(int evalNumber) throws NotSerializableException {
        this.numOfEvals = evalNumber;
    }
}
