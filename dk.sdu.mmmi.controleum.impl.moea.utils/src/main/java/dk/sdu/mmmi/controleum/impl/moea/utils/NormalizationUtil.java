/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.utils;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import java.math.RoundingMode;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author ancla
 */
public class NormalizationUtil {

    public static Map<Concern, MinMax> GetMinMaxFitnessValues(List<ISolution> pop) {
        //Not pretty dimensionen. Size in second dimension should be number of active concerns.
        //Currently found by checking the number of evaluationvalues in first solution in the population.
        Map<Concern, MinMax> res = new HashMap<>();
        //For each solution in the current population
        for (ISolution s : pop) {
            //Get map with concerns and corresponding fitness values;
            Map<Concern, Double> fitnessValuesMap = s.getFitnessValuesMap();
            //For each entry in fitness value map
            for (Entry<Concern, Double> fitnessEntry : fitnessValuesMap.entrySet()) {
                if (res.get(fitnessEntry.getKey()) == null) {
//                    res.put(fitnessEntry.getKey(), new MinMax()); //qqq comment. because it causes min is always 0. replaced by row 38, 39.
                    //TODO qqq add. for solving min is always 0 (due to the init is 0, so on fitness can be smaller than 0)
                    Double initMinMax = fitnessEntry.getValue();    
                    res.put(fitnessEntry.getKey(), new MinMax(initMinMax, initMinMax));
                }
                Concern currentConcern = fitnessEntry.getKey();
                MinMax minMaxForCurrentConcern = res.get(currentConcern);
                Double fitnessForConcernInSolution = fitnessEntry.getValue();
                //Compare with current minimum value. If less than, replace minimum value.
                if (fitnessForConcernInSolution < minMaxForCurrentConcern.getMin()) {
                    minMaxForCurrentConcern.setMin(fitnessForConcernInSolution);
                    res.put(currentConcern, minMaxForCurrentConcern);
                }
                //Compare with current maximum value. If larger than, replace maximum value.
                //Not else, since the first value is - and should be - both min and max.
                if (fitnessForConcernInSolution > minMaxForCurrentConcern.getMax()) {
                    minMaxForCurrentConcern.setMax(fitnessForConcernInSolution);
                    res.put(currentConcern, minMaxForCurrentConcern);
                }
            }
        }
        return res;
    }

    public static double TotalDistanceFromAverage(ISolution s1, Map<Concern, MinMax> minMaxConcernFitness) {
        //Calculate Normalized fitness value for each concern for particular solution
        Map<Concern, Double> normalizedFitnessValues = NormalizeSolution(s1, minMaxConcernFitness);
        //Calculate average normalized fitness value across current solution
        double average = CalculateAverageCost(normalizedFitnessValues);
        //Calculate absolute deviation from average
        return calculateAbsoluteDeviation(normalizedFitnessValues, average);
    }

    public static double CalculateAverageCost(Map<Concern, Double> vals) {

        double sum = 0.0;
        for (Double d : vals.values()) {
            sum += d;
        }
        return sum / (double) vals.size();
    }

    public static double calculateAbsoluteDeviation(Map<Concern, Double> vals, double average) {

        double res = 0.0;
        for (Double d : vals.values()) {
            res += Math.abs(average - d);
        }
        return res;
    }

    public static double VectorLength(ISolution s1, Map<Concern, MinMax> minMaxConcernFitness) {
        RealVector v = new ArrayRealVector(NormalizeSolution(s1, minMaxConcernFitness).values().toArray(new Double[]{}));
        return v.getNorm();
    }

    /*Returns an array with values between 0 and 1 representing the relative position of the fitness
     of each concern compared to the best and worst solution for that concern (represented through the min and max value arrays.
     */
    public static Map<Concern, Double> NormalizeSolution(ISolution s1, Map<Concern, MinMax> minMaxConcernFitness) {
        Map<Concern, Double> fitnessValues = ((ISolution) s1).getFitnessValuesMap();
        Map<Concern, Double> normalizedFitnessValues = new HashMap<>();
        for (Entry<Concern, Double> ecd : fitnessValues.entrySet()) {
            //Ensure no division by zero
            MinMax mm = minMaxConcernFitness.get(ecd.getKey());
            if ((mm.getMax() - mm.getMin()) > 0.0) {        //TODO //qqq mm.getMax() -> (mm.getMax() - mm.getMin())
                double normalizedValue = (ecd.getValue() - mm.getMin()) / (mm.getMax() - mm.getMin());

                normalizedFitnessValues.put(ecd.getKey(), normalizedValue);

            } else {
                normalizedFitnessValues.put(ecd.getKey(), 0.0);
            }
        }
        return normalizedFitnessValues;
    }

    /**
     * Min-max normalization method.
     * If minValue and maxValue is equal, the method will return 0.
     * @param x The input that has to be normalized
     * @param minValue The minimum boundary
     * @param maxValue The maximum boundary
     * @return Returns a normalized value within the range 0.0 to 1.0 both included.
     */
    public static double normalizeMinMax(double x, double minValue, double maxValue) {
        if(minValue == maxValue){
            // This is needed behavior for Controleum to function properly
            return 0;
        }

        if (minValue < maxValue && minValue <= x && x <= maxValue) {
            return (x - minValue) / (maxValue - minValue);
        }

        throw new IllegalArgumentException("Input values not valid for inputs:\n" +
                                            "minValue: " + minValue + "\n" +
                                            "x: " + x + "\n" +
                                            "maxValue: " + maxValue);
    }

    /**
     * Returns the Euclidean distance between two solutions in objective space.
     *
     * @param s1 the first solution
     * @param s2 the second solution
     * @return the distance between the two solutions in objective space
     */
    public static double Distance(ISolution s1, ISolution s2) {
        double distance = 0.0;

        List<Double> f1 = s1.getEvaluationValues();
        List<Double> f2 = s2.getEvaluationValues();

        for (int i = 0; i < f1.size(); i++) {
            distance += Math.pow(f1.get(i) - f2.get(i), 2.0);
        }

        return Math.sqrt(distance);
    }

    public static double CalculateAverageCost(List<Double> vals) {

        double sum = 0.0;
        for (Double d : vals) {
            sum += d;
        }
        return sum / (double) vals.size();
    }
}
