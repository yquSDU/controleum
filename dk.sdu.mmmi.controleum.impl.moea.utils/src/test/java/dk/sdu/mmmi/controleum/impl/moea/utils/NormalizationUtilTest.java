package dk.sdu.mmmi.controleum.impl.moea.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class NormalizationUtilTest {

    @org.junit.Test
    public void normalizeMinMaxErrorTest() {
        double[] minValues = {1, 0, 0};
        double[] xValues   = {1, 2, -1};
        double[] maxValues = {0, 1, 1};

        for (int i = 0; i < minValues.length; i++) {
            try {
                NormalizationUtil.normalizeMinMax(xValues[i], minValues[i], maxValues[i]);
                fail("Expected IllegalArgumentException was not thrown for inputs: \n" +
                        "min: " + minValues[i] + "\n" +
                        "x: "   + xValues[i] + "\n" +
                        "max: " + maxValues[i] + "\n");
            } catch (IllegalArgumentException ex) {
                // PASS
            }
        }
    }

    @org.junit.Test
    public void normalizeMinMaxPositiveValues() {
        double[] minValues = {0, 0, 1};
        double[] xValues   = {1, 1, 1};
        double[] maxValues = {1, 2, 2};

        double[] expectedValues = {1, 0.5, 0};

        for (int i = 0; i < minValues.length; i++) {
            double actualValue = NormalizationUtil.normalizeMinMax(xValues[i], minValues[i], maxValues[i]);
            assertEquals(expectedValues[i], actualValue, 0.00001d);
        }
    }

    @org.junit.Test
    public void normalizeMinMaxNegativeValues() {
        double[] minValues = {-2, -2, -1};
        double[] xValues   = {-1, -1, -1};
        double[] maxValues = {-1, 0, 0};

        double[] expectedValues = {1, 0.5, 0};

        for (int i = 0; i < minValues.length; i++) {
            double actualValue = NormalizationUtil.normalizeMinMax(xValues[i], minValues[i], maxValues[i]);
            assertEquals(expectedValues[i], actualValue, 0.00001d);
        }
    }

    @Test
    public void NormalizeMinMaxIsEqualAllowed() {
        // Normally the min and max value must not be equal due to devide by zero error.
        // This behavior is different than a normal min man normalization because it is needed by the controleum implementation.
        double actualValue = NormalizationUtil.normalizeMinMax(1, 1, 1);
        assertEquals(0, actualValue, 0.00001d);
    }
}