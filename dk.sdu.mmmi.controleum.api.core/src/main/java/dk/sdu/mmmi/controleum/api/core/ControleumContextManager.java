package dk.sdu.mmmi.controleum.api.core;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import java.util.List;

/**
 *
 * @author jcs
 */
public interface ControleumContextManager extends Disposable {

    ControleumContext createContext(String name);

    void deleteContext(ControleumContext domain);

    ControleumContext getContext(int id);

    ControleumContext getContext(String name);

    List<ControleumContext> getContexts();

    void loadContexts();

    void renameContext(ControleumContext domain, String name);

    void loadConcernConfig(ControleumContext domain, Concern concern);

    void saveConcernConfig(ControleumContext domain, Concern concern);
}
