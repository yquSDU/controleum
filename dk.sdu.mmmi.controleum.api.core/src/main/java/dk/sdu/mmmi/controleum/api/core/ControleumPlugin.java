package dk.sdu.mmmi.controleum.api.core;

import com.decouplink.Disposable;
import com.decouplink.Shareable;
import java.util.UUID;

/**
 * Global service-provider interface.
 *
 * @author mrj
 */
@Shareable
public interface ControleumPlugin {

    /**
     * When a ControlDomain is loaded, this method is called to allow plugins to
     * "add stuff". The returned disposable is called when the ControlDomain is
     * unloaded.
     */
    Disposable create(ControleumContext domain, UUID issueId);     //qqq add UUID issueId
}
