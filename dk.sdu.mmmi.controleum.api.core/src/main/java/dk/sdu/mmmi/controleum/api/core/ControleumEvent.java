package dk.sdu.mmmi.controleum.api.core;

/**
 * @author mrj
 */
public class ControleumEvent {

    private final ControleumContext domain;

    public ControleumEvent(ControleumContext domain) {
        this.domain = domain;
    }

    public ControleumContext getControlDomain() {
        return domain;
    }
}
