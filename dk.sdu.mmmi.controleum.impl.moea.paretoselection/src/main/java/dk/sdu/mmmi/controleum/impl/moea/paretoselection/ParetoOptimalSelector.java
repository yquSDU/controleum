package dk.sdu.mmmi.controleum.impl.moea.paretoselection;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulationSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.openide.util.Exceptions;

/**
 *
 * @author ancla
 */
public class ParetoOptimalSelector implements IPopulationSelector {

    public static final double EPS = 1e-10;
    private static final ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();

    //TODO //qqq add for compare v1 PF
    static boolean bLogCompareV1_PF = false;
    FileWriter fw_compareV1PF = null;
    List<ISolution> v2_PF = new ArrayList<>();
    static int count = 1;

    @Override
    public boolean selectParetoFront(List<ISolution> paretoFront, List<ISolution> newPopulation) {
        boolean changed = false;
        for (ISolution s : newPopulation) {
            Map<Integer, List<ISolution>> sortByDomination = paretoFront.parallelStream().collect(Collectors.groupingBy(sR -> PARETO_FITNESS.compare(s, sR)));
            List<ISolution> newSolutionDominatedBy = sortByDomination.get(1);   // 1 means the return value from PARETO_FITNESS.compare: the new solution is dominated by the current PF
            if (newSolutionDominatedBy == null) {   //qqq that is, if the new solution s dominats or equals to all the current PF ...
                paretoFront.add(s); //qqq add the new solution to the PF
                changed = true;
                List<ISolution> solutionsDominatedByNewSolution = sortByDomination.get(-1); //qqq -1 means the solution s dominates some solutions in the current PF
                if (solutionsDominatedByNewSolution != null) {
                    paretoFront.removeAll(solutionsDominatedByNewSolution); //qqq remove the solutions (in the current PF) that are dominted by the new solution s
                }
            }
        }

        return changed;
    }

    @Override
    public void compare_v1PF(FileWriter fw_v1PF, List<ISolution> v1PF, List<ISolution> v2PF) {

        boolean dominate1 = false;
        boolean dominate2 = false;
        boolean compForOnceUnfinished = true;

        fw_compareV1PF = fw_v1PF;
        this.v2_PF = new ArrayList<>(v2PF);
        count = 1;

        try {
            fw_compareV1PF.write(String.format("v2 PF original size =\t\t%s\n", this.v2_PF.size()));

            for (ISolution s_v1 : v1PF) {
                boolean v1_isDominated = false;
                boolean v1_dominatev2 = false;
                fw_compareV1PF.write(String.format("[%s]%s", count++, s_v1.qToString_onlyLP()));

                if (v2_PF.contains(s_v1)) {
                    fw_compareV1PF.write(String.format("v2 PF contains\n"));
                } else {
                    List<Double> f1 = s_v1.getEvaluationValues();

                    for (ISolution s_v2 : v2PF) {
                        List<Double> f2 = s_v2.getEvaluationValues();

                        if (f1.size() != f2.size()) {
                            throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
                        }

                        compForOnceUnfinished = true;
                        for (int i = 0; i < f1.size(); i++) {
                            if (f1.get(i) < f2.get(i)) {
                                dominate1 = true;

                                if (dominate2) {
                                    compForOnceUnfinished = false;
                                }
                            } else if (f1.get(i) > f2.get(i)) {
                                dominate2 = true;

                                if (dominate1) {
                                    compForOnceUnfinished = false;
                                }
                            }
                        }

                        if (compForOnceUnfinished) {
                            if (dominate1 == dominate2) {
                                if (checkEquals(s_v1, s_v2) == 0) {
                                    if (!v2_PF.contains(s_v1)) {
                                        fw_compareV1PF.write(String.format("v2 PF contains\n"));
                                    }
                                } else {
                                }
                            } else if (dominate1) {
                                v1_dominatev2 = true;
                                if (this.v2_PF.contains(s_v2)) {
                                    this.v2_PF.remove(s_v2);
                                }
                            } else {
                                v1_isDominated = true;
                            }
                        }
                    }

                    //log
                    if (v1_isDominated) {
                        fw_compareV1PF.write(String.format("removed\n"));
                    } else {
                        if (v1_dominatev2) {
                            fw_compareV1PF.write(String.format("dominates v2\n"));
                        } else {
                            fw_compareV1PF.write(String.format("non-dominated\n"));
                        }
                    }
                }
            }

            fw_compareV1PF.write("------------- after compare ------------\n\n");
            fw_compareV1PF.write(String.format("v2 PF size after compare =\t%s\n", this.v2_PF.size()));

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private int checkEquals(Solution solution1, Solution solution2) {
        if (solution1.equals(solution2)) {
            return 1;
        } else {
            return 0;
        }
    }
}
