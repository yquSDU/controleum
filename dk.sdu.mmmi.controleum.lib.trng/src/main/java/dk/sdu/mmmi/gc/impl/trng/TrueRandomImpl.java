package dk.sdu.mmmi.gc.impl.trng;

import dk.sdu.mmmi.gc.api.trng.TrueRandomNumberGenerator;
import hr.irb.random.QrbgSecureRandom;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import org.random.rjgodoy.trng.RjgodoyProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = TrueRandomNumberGenerator.class)
public class TrueRandomImpl implements TrueRandomNumberGenerator {

    public TrueRandomImpl() {
        

            //Sets the username and password
            System.setProperty(QrbgSecureRandom.USERNAME, "corfixen");
            System.setProperty(QrbgSecureRandom.PASSWORD, "JCS301080");

            //register the provider
            Security.addProvider(new RjgodoyProvider());
    }

    @Override
    public Random createNewRandom() {
        Random R;

        try {
            //creates a SecureRandom object using the QRBG algorithm (which access random.irb.hr)
            R = new Random(SecureRandom.getInstance("QRBG").nextLong());

        } catch (NoSuchAlgorithmException ex) {
            R = new Random();
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Randomizer could not be initialized.", ex);
        }

        return R;
    }

}
