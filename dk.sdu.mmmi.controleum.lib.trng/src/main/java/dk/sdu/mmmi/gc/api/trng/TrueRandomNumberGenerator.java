package dk.sdu.mmmi.gc.api.trng;

import java.util.Random;

/**
 *
 * @author jcs
 */
public interface TrueRandomNumberGenerator {
    Random createNewRandom();

}
