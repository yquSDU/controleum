package dk.sdu.mmmi.controleum.impl.db.schema;

import java.sql.Connection;

/**
 * @author mrj
 */
public class Schema {

    public static SchemaBuilder createSchemaBuilder(Connection con) {
        SchemaBuilder sb = new SchemaBuilder(con);

        /*
         * To maintain backwards compatibilty. - Only add new versions. - Make
         * sure to use increasing version numbers.
         */
        sb.addVersion(1,
                "CREATE TABLE context ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "name VARCHAR(128))");

        sb.addVersion(2,
                "ALTER TABLE context "
                + "ADD COLUMN deleted SMALLINT DEFAULT 0");

        sb.addVersion(3,
                "CREATE TABLE concern_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "concern_id VARCHAR(128), "
                + "is_enabled SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(4, "ALTER TABLE concern_configuration ADD COLUMN priority INT");

        sb.addVersion(5,
                "CREATE TABLE output_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "output_id VARCHAR(128), "
                + "is_enabled SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(6,
                "CREATE TABLE control_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "config CLOB, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(7,
                "CREATE TABLE concern_negotiation_result ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT, "
                + "concern_id VARCHAR(128), "
                + "time TIMESTAMP, "
                + "fitness DOUBLE, "
                + "accepted SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(8, "ALTER TABLE concern_negotiation_result ADD COLUMN value_help CLOB");

        sb.addVersion(9,
                "CREATE TABLE negotiation_statistics ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "generations INT, "
                + "duration_ms BIGINT, "
                + "solution_found SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        sb.addVersion(10, "ALTER TABLE concern_negotiation_result DROP COLUMN accepted");

        sb.addVersion(11, "ALTER TABLE negotiation_statistics DROP COLUMN solution_found");
        
        sb.addVersion(12,
                "CREATE TABLE issue_configuration ("
                + "id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
                + "context_id INT,"
                + "time TIMESTAMP, "
                + "issue_id VARCHAR(128), "
                + "is_enabled SMALLINT, "
                + "FOREIGN KEY (context_id) REFERENCES context (id))");

        //sb.addVersion(x, "ALTER TABLE context ADD COLUMN name VARCHAR(128)");
        return sb;
    }
}
