package dk.sdu.mmmi.controleum.impl.db.jdbc;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.impl.db.util.JDBC;
import dk.sdu.mmmi.controleum.common.config.ConcernConfig;
import dk.sdu.mmmi.controleum.common.config.ControlConfig;
import dk.sdu.mmmi.controleum.common.config.IssueConfig;
import dk.sdu.mmmi.controleum.common.config.OutputConfig;
import dk.sdu.mmmi.controleum.common.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.common.results.NegotiationResult;
import dk.sdu.mmmi.controleum.common.units.Sample;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mrj & jcs
 *
 * TODO: Refactor into simpler database code. Avoid DRY principle.
 */
public class GenericContextDataAccess implements ContextDataAccess {

    private final Connection con;
    private final int contextID;
    private final String name;
    private final Gson gson;

    public GenericContextDataAccess(Connection con, int contextID, String name) {
        this.con = con;
        this.contextID = contextID;
        this.name = name;
        this.gson = new Gson();
    }

    @Override
    public int getID() {
        return contextID;
    }

    @Override
    public String getName() {
        return name;
    }
    //
    // Setter
    //

    @Override
    public void setControlConfiguration(ControlConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO control_configuration "
                + "(context_id, time, config) "
                + "VALUES (?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, gson.toJson(cfg));

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save ControlConfiguration to DB.", ex);
        }
    }

    @Override
    public void setConcernConfiguration(String id, ConcernConfig cfg) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO concern_configuration "
                + "(context_id, time, concern_id, is_enabled, priority) "
                + "VALUES (?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, id);
            ps.setInt(4, cfg.isEnabled() ? 1 : 0);
            ps.setInt(5, cfg.getPriority());
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save ConcernConfiguration to DB.", ex);
        }
    }

    @Override
    public void setOutputConfiguration(String id, OutputConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO output_configuration "
                + "(context_id, time, output_id, is_enabled) "
                + "VALUES (?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, id);
            ps.setInt(4, cfg.isEnabled() ? 1 : 0);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save OutputConfiguration to DB.", ex);
        }
    }

    @Override
    public void setIssueConfiguration(String id, IssueConfig cfg) {

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO issue_configuration "
                + "(context_id, time, issue_id, is_enabled) "
                + "VALUES (?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(new Date()));
            ps.setString(3, id);
            ps.setInt(4, cfg.isEnabled() ? 1 : 0);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not save IssueConfiguration to DB.", ex);
        }
    }

    @Override
    public ControlConfig getControlConfiguration() {

        ControlConfig cfg = new ControlConfig.Builder().build();

        try (PreparedStatement ps = con.prepareStatement("SELECT time, config "
                + "FROM control_configuration "
                + "WHERE context_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cfg = gson.fromJson(rs.getString(2), ControlConfig.class);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve ControlConfig from DB.", ex);
        }
        return cfg;
    }

    @Override
    public ConcernConfig getConcernConfiguration(String id) {

        ConcernConfig cfg = new ConcernConfig.Builder().build();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, is_enabled, priority "
                + "FROM concern_configuration "
                + "WHERE context_id = ? AND concern_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cfg = new ConcernConfig.Builder().setEnabled(rs.getInt(2) > 0)
                            .setPriority(rs.getInt(3)).build();
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    "Could not retrieve ConcernConfig from DB.", ex);
        }

        return cfg;
    }

    @Override
    public OutputConfig getOutputConfiguration(String id) {
        OutputConfig cfg = new OutputConfig();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, is_enabled "
                + "FROM output_configuration "
                + "WHERE context_id = ? AND output_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cfg.setEnabled(rs.getInt(2) > 0);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve OutputConfig from DB.", ex);
        }
        return cfg;
    }
    
     @Override
    public IssueConfig getIssueConfiguration(String id) {
        IssueConfig cfg = new IssueConfig();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, is_enabled "
                + "FROM issue_configuration "
                + "WHERE context_id = ? AND output_id = ? "
                + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cfg.setEnabled(rs.getInt(2) > 0);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not retrieve IssueConfig from DB.", ex);
        }
        return cfg;
    }

    @Override
    public void insertConcernNegotiationResult(Sample<ConcernNegotiationResult> s) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO concern_negotiation_result "
                + "(context_id, concern_id, time, fitness, value_help) "
                + "VALUES (?, ?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setString(2, s.getSample().getConcernID());
            ps.setTimestamp(3, JDBC.toTime(s.getTimestamp()));
            ps.setDouble(4, s.getSample().getCost());

            Type type = new TypeToken<Map<String, String>>() {
            }.getType();

            ps.setString(5, gson.toJson(s.getSample().getValueHelp(), type));
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert X into DB.", ex);
        }
    }

    @Override
    public Map<Date, List<ConcernNegotiationResult>> selectConcernsNegotiationResult(Date fr, Date to) {

        Map<Date, List<ConcernNegotiationResult>> r = new TreeMap<>();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, concern_id, fitness, value_help "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time")) {

            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(fr));
            ps.setTimestamp(3, JDBC.toTime(to));

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Date d = JDBC.fromTime(rs.getTimestamp(1));

                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                    ConcernNegotiationResult cnr
                            = new ConcernNegotiationResult(rs.getString(2),
                            rs.getDouble(3),
                            valueHelp);

                    if (r.containsKey(d)) {
                        r.get(d).add(cnr);
                    } else {
                        List<ConcernNegotiationResult> l = new ArrayList<>();
                        l.add(cnr);
                        r.put(d, l);
                    }

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernsNegotiationResult from DB.", ex);
        }

        return r;
    }

    @Override
    public List<Sample<ConcernNegotiationResult>> selectConcernNegotiationResult(String concernID, Date fr, Date to) {
        List<Sample<ConcernNegotiationResult>> r = new ArrayList();

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT time, concern_id, fitness, value_help "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? "
                + "ORDER BY time");) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Date d = JDBC.fromTime(rs.getTimestamp(1));
                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                    ConcernNegotiationResult cnr = new ConcernNegotiationResult(rs.getString(2), rs.getDouble(3), valueHelp);

                    r.add(new Sample(d, cnr));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public Sample<ConcernNegotiationResult> selectConcernNegotiationResult(String concernID, Date t) {

        Sample<ConcernNegotiationResult> r = null;

        try (
                PreparedStatement ps = con.prepareStatement(
                        "SELECT time, concern_id, fitness, value_help "
                        + "FROM concern_negotiation_result "
                        + "WHERE context_id = ? "
                        + "AND concern_id = ? "
                        + "AND time <= ? "
                        + "ORDER BY time DESC");) {

            ps.setMaxRows(1);
            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(t));

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Date d = JDBC.fromTime(rs.getTimestamp(1));

                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Map<String, String> valueHelp = gson.fromJson(rs.getString(4), type);

                    ConcernNegotiationResult cnr
                            = new ConcernNegotiationResult(rs.getString(2),
                            rs.getDouble(3),
                            valueHelp);

                    r = new Sample(d, cnr);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select X from DB.", ex);
        }
        return r;
    }

    @Override
    public Double selectConcernMinFitness(String concernID, Date fr, Date to) {

        Double r = 0.0;
        try (PreparedStatement ps = con.prepareStatement(
                "SELECT MIN(fitness) "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? ")) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    r = rs.getDouble(1);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernMinFitness from DB.", ex);
        }
        return r;
    }

    @Override
    public Double selectConcernMaxFitness(String concernID, Date fr, Date to) {

        Double r = 1.0;

        try (PreparedStatement ps = con.prepareStatement(
                "SELECT MAX(fitness) "
                + "FROM concern_negotiation_result "
                + "WHERE context_id = ? "
                + "AND concern_id = ? "
                + "AND time >= ? AND time < ? ")) {

            ps.setInt(1, contextID);
            ps.setString(2, concernID);
            ps.setTimestamp(3, JDBC.toTime(fr));
            ps.setTimestamp(4, JDBC.toTime(to));

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    r = rs.getDouble(1);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not select ConcernMaxFitness from DB.", ex);
        }
        return r;
    }

    @Override
    public void insertNegotiationResult(Sample<NegotiationResult> s) {
        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO negotiation_statistics "
                + "(context_id, time, generations, duration_ms) "
                + "VALUES (?, ?, ?, ?)")) {
            ps.setInt(1, contextID);
            ps.setTimestamp(2, JDBC.toTime(s.getTimestamp()));
            ps.setInt(3, s.getSample().getGenerationsExecuted());
            ps.setLong(4, s.getSample().getTimeSpendMS());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Could not insert X into DB.", ex);
        }
    }

}
