package dk.sdu.mmmi.controleum.control.commons;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ConcernListener;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Value;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 * A common base class for concerns
 *
 * @author mrj, jcs
 */
public abstract class AbstractConcern implements Concern {

    private int priority = 2;
    private boolean enabled = true;
    private double cost = 1;
    private Map<Class, String> valueHelp = new HashMap<>();
    private String name;
    protected final ControleumContext g;

    public AbstractConcern(String name, ControleumContext controlDomain, int priority) {
        this.name = name;
        this.g = controlDomain;

        if (priority > LOW_PRIORITY || priority < HARD_PRIORITY) {
            throw new IllegalArgumentException("Priority out of bounds.");
        }
        this.priority = priority;
    }

    @Override
    public synchronized void setPriority(int priority, boolean fireEvent) {
        this.priority = priority;
        if (fireEvent) {
            fireConcernChanged();
        }
    }

    @Override
    public synchronized int getPriority() {
        return priority;
    }

    @Override
    public double getEvaluationResult() {
        return this.cost;
    }

    @Override
    public synchronized void setEnabled(boolean enabled) {
        this.setEnabled(enabled, true);
    }

    @Override
    public synchronized void setEnabled(boolean enabled, boolean fireEvent) {
        this.enabled = enabled;
        if (fireEvent) {
            fireConcernChanged();
        }
    }

    @Override
    public synchronized boolean isEnabled() {
        return enabled;
    }

    @Override
    public void fireConcernChanged() {
        for (ConcernListener l : context(g).all(ConcernListener.class)) {
            l.onConcernChanged(this);
        }
    }

    @Override
    public final boolean isOptimal() {
        return cost <= 0.01;
    }

    @Override
    public Map<Class, String> getValueHelp(Solution s) {
        return Collections.EMPTY_MAP;
    }

    @Override
    public final Map<Class, String> getValueHelp() {
        return Collections.unmodifiableMap(valueHelp);
    }

    @Override
    public final String getValueHelpOrNull(Class<? extends Value> v) {
        return getValueHelp().get(v);
    }

    @Override
    public String getID() {
        return getClass().getName();
    }

    /**
     * Get a string representation of this concern.
     */
    @Override
    public final String toString() {
        return getName();
    }

    @Override
    public void commit(Solution s) {
        if (enabled) {
            // Clear old dependencies.
            //dependencies.clear();

            // Configure recorder.
            //Solution r = new SolutionDependencyRecorder(s);
            // Evaluate solution using recorder.
            cost = evaluate(s);

            // Find output help strings.
            valueHelp = getValueHelp(s);
        }
    }

    //
    // ELEMENT INTERFACE
    //
    @Override
    public void setName(String name) {
        this.name = name;
        fireConcernChanged();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public URL getHtmlHelp() {
        URL url = getClass().getResource(getID() + ".html");
        return url;
    }
    
    @Override
    public String qToString(Solution s) {
        return "";
    }
}
