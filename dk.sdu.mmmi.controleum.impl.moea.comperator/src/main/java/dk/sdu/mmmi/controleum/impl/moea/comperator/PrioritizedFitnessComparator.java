/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.util.Comparator;

/**
 *
 * @author ancla
 */
public class PrioritizedFitnessComparator implements Comparator<ISolution> {

    @Override
    public int compare(ISolution s1, ISolution s2) {
        int r = 0;

        for (int p = Concern.HARD_PRIORITY; p <= Concern.LOW_PRIORITY; p++) {

            double f1 = s1.getEvaluationResult(p);
            double f2 = s2.getEvaluationResult(p);
            if (f1 != f2) {
                r = (f1 > f2) ? 1 : -1;
                break;
            } else {
                r = 0;
            }
        }
        return r;
    }


}
