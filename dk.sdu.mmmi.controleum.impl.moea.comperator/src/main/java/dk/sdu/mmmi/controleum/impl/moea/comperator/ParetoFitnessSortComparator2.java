/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ancla
 */
public class ParetoFitnessSortComparator2 implements Comparator<Solution> {

    /**
     * Compares the two solutions using a dominance relation, returning
     * {@code -1} if {@code solution1} dominates {@code solution2}, {@code 1} if
     * {@code solution2} dominates {@code solution1}, and {@code 0} if the
     * solutions are non-dominated.
     *
     * @param solution1 the first solution
     * @param solution2 the second solution
     * @return {@code -1} if {@code solution1} dominates {@code solution2},
     * {@code 1} if {@code solution2} dominates {@code solution1}, and {@code 0}
     * if the solutions are non-dominated
     */
    @Override
    public int compare(Solution solution1, Solution solution2) {

        List<Double> f1 = solution1.getEvaluationValues();
        List<Double> f2 = solution2.getEvaluationValues();

        if (f1.size() != f2.size()) {
            throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
        }
        int v;
        for (int i = 0; i < f1.size(); i++) {
            v = f1.get(i).compareTo(f2.get(i));
            if (v != 0) {
                return v;
            }
        }
        
        return 0;
        /*        for (int i = 0; i < f1.size(); i++) {
         if (f1.get(i) < f2.get(i)) {
         return -1;
         } else if (f1.get(i) > f2.get(i)) {
         return 1;
         }
         }
         return 0;*/
    }

}
