/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.util.Comparator;

/**
 *
 * @author Anders
 */
public class SortedFitnessComperator implements Comparator<ISolution> {

    @Override
    public int compare(ISolution t, ISolution t1) {
        int c = 0;

        for (int i = 0; i < t.getEvaluationValues().size(); i++) {
            c = t.getEvaluationValues().get(i).compareTo(t1.getEvaluationValues().get(i));
            if (c != 0) {
                return c;
            }
        }
        return c;
    }

}
