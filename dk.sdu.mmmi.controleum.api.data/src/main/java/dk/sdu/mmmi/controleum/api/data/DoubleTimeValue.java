package dk.sdu.mmmi.controleum.api.data;

import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author mrj
 */
public class DoubleTimeValue implements TimeValue<Double> {

    private final Date time;
    private final Double value;

    public DoubleTimeValue(Date time, Double value) {
        this.time = time;
        this.value = value;
    }

    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getTime() + " = " + getValue();
    }
    public final static Comparator<DoubleTimeValue> ASCENDING_ORDER = new Comparator<DoubleTimeValue>() {
        @Override
        public int compare(DoubleTimeValue a, DoubleTimeValue b) {
            return a.getTime().compareTo(b.getTime());
        }
    };
}
