package dk.sdu.mmmi.controleum.api.data;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author mrj
 */
public abstract class DoubleTimeSeries implements TimeSeries {

    private String name = getClass().getName();
    private String unit = "";

    // TODO: Should be able to show linear interpolation if the control interval is more than 15 min. 
    private long ms = 1000L * 60 * 15;

    @Override
    public String getID() {
        return getClass().getName();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public long getPeriod()
    {
        return ms;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public void setMaxPeriodLenghtMS(long ms)    {
        this.ms = ms;
    }

    @Override
    public long getMaxPeriodLenghtMS() {
        return ms;
    }

    /**
     * Implement this method to provide data. It is important the returned list
     * is sorted in ascending order. See DoubleTimeValue.ASCENDING_ORDER.
     */
    @Override
    public abstract List<DoubleTimeValue> getValues(Date from, Date to) throws Exception;
    public static final Comparator<DoubleTimeSeries> ASC_ORDER = new Comparator<DoubleTimeSeries>() {

        @Override
        public int compare(DoubleTimeSeries t, DoubleTimeSeries t1) {
            return t.getName().compareTo(t1.getName());
        }
    };
}
