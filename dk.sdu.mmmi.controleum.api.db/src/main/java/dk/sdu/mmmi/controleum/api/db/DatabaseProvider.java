package dk.sdu.mmmi.controleum.api.db;

import com.decouplink.Link;

/**
 * @author mrj
 */
public interface DatabaseProvider {

    /**
     * Get access to a control domain database. Make sure to dispose the link when
     * you do not need to use it anymore.
     *
     * @return
     */
    Link<DatabaseDataAccess> getDataAccess();
}
