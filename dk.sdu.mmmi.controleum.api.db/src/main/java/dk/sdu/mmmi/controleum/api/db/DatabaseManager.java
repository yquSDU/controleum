package dk.sdu.mmmi.controleum.api.db;

import com.decouplink.AbstractLinkShare;
import com.decouplink.Link;
import org.openide.util.Lookup;

/**
 * Provides access to shared DatabaseDataAccess.
 *
 * @author mrj
 */
public class DatabaseManager {

    //
    // Singleton.
    //
    private static final DatabaseManager instance = new DatabaseManager();

    public static DatabaseManager getInstance() {
        return instance;
    }
    //
    // Implementation.
    //
    private final DBShare share = new DBShare();

    private DatabaseManager() {
    }

    public Link<DatabaseDataAccess> getDataAccess() {
        return share.getLink();
    }

    /**
     * DBShare.
     */
    private static class DBShare extends AbstractLinkShare<DatabaseDataAccess> {

        @Override
        protected Link<DatabaseDataAccess> createObject() {
            Lookup global = Lookup.getDefault();
            DatabaseProvider dp = global.lookup(DatabaseProvider.class);
            return dp.getDataAccess();
        }
    }
}
