/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.db;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 * This implementation of this interface indicates an application specfic database.
 * All implementations of ApplicationDatabase will be loaded when Controleum is started.
 * @author ancla
 */
public interface ApplicationDatabase extends Disposable {
    public void createDatabaseContext(ControleumContext cd);
}
