/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.generic;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This is a genetic schedule.
 *
 * @author ancla
 * may be different from the type of the values stored in the values list.
 */
public class ControleumValueListImpl implements ControleumValueList {

    private List<Number> list;

    public ControleumValueListImpl() {
        list = new ArrayList<>();
    }

    public ControleumValueListImpl(List<Number> list) {
        this.list = list;
    }

    public ControleumValueListImpl(Number value) {
        list = new ArrayList<>();
        list.add(value);
    }
    
    
    public ControleumValueListImpl(ControleumValueList schedule) {
        /*this.values = new ArrayList<>();
         List<ControleumValue> l = schedule.getValues();
         for (ControleumValue v : l) {
         this.values.add(v);
         }*/
        this.list = new ArrayList<>();
        for (Number v : schedule.getValues()) {
            this.list.add(v);
        }

        //this.value = value;

    }

    public ControleumValueListImpl(ControleumValueListImpl schedule) {
        /*this.values = new ArrayList<>();
         List<ControleumValue> l = schedule.getValues();
         for (ControleumValue v : l) {
         this.values.add(v);
         }*/
        this.list = new ArrayList<>();
        for (Number v : schedule.list) {
            this.list.add(v);
        }

        //this.value = value;

    }
/*
    public synchronized Number getValue() {
        return value;
    }

    public synchronized void setValue(Number value) {
        this.value = value;
    }
*/
    @Override
    public synchronized List<Number> getValues() {
        return Collections.unmodifiableList(list);
    }

    @Override
    public synchronized String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int i = 0;
        for (Number v : this.list) {
            sb.append(v.toString());
            if (this.list.indexOf(v) < this.list.size() - 1) {
                sb.append(" ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public ControleumValueListImpl summarizedValue() {
        return list
                .stream()
                .collect(Collectors.collectingAndThen(Collectors.summingDouble(Number::doubleValue), value -> new ControleumValueListImpl(value)
                        )
                );
    }

    @Override
    public ControleumValueListImpl getValue(Number a, Number b) {
        return new ControleumValueListImpl();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ControleumValueListImpl)) {
            return false;
        }
        ControleumValueListImpl oC = (ControleumValueListImpl) o;
   
        if (oC.size() != this.size()) {
            return false;
        }
        for (int i = 0; i < this.size(); i++) {
            Number v1 = this.get(i);
            Number v2 = oC.get(i);
            if (!v1.equals(v2)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + list.hashCode();
        return hash;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Number get(int index) {
        return list.get(index);
    }

    @Override
    public void set(int i, Number value) {
        list.set(i, value);
    }

    @Override
    public void addAll(Number[] c) {
        list.addAll(Arrays.asList(c));
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<Number> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(Number e) {
        return list.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Number> c) {
        return list.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public ControleumValueList multipliedWith(ControleumValueList l) {
        return IntStream.range(0, Math.max(this.size(), l.size()))
                .mapToObj(i -> {
                    Number n1 = this.get(i);
                    Number n2 = l.get(i);
                    return multiplyNumbers(n1 == null ? 0.0 : n1, n2 == null ? 0.0 : n2);
                }
                )
                .collect(Collectors.toCollection(ControleumValueListImpl::new));

    }

    @Override
    public ControleumValueList addedWith(ControleumValueList l) {
        return IntStream.range(0, Math.max(this.size(), l.size()))
                .mapToObj(i -> {
                    Number n1 = 0.0;
                    Number n2 = 0.0;
                    if (this.size() > i) {
                        n1 = this.get(i);
                    }
                    if (l.size() > i) {
                        n2 = l.get(i);
                    }
                    return addNumbers(n1, n2);
                }
                )
                .collect(Collectors.toCollection(ControleumValueListImpl::new));

    }

    public static Number multiplyNumbers(Number a, Number b) {
        Number res = 0;
        if (a instanceof Byte) {
            res = a.byteValue() * b.byteValue();
        } else if (a instanceof Short) {
            res = a.shortValue() * b.shortValue();
        } else if (a instanceof Integer) {
            res = a.intValue() * b.intValue();
        } else if (a instanceof Long) {
            res = a.longValue() * b.longValue();
        } else if (a instanceof Float) {
            res = a.floatValue() * b.floatValue();
        } else if (a instanceof Double) {
            res = a.doubleValue() * b.doubleValue();
        }
        return res;
    }

    public static int multiplyIntegers(Number a, Number b) {
        return a.intValue() * b.intValue();
    }

    public static Number addNumbers(Number a, Number b) {
        Number res = 0;
        if (a instanceof Byte) {
            res = a.byteValue() + b.byteValue();
        } else if (a instanceof Short) {
            res = a.shortValue() + b.shortValue();
        } else if (a instanceof Integer) {
            res = a.intValue() + b.intValue();
        } else if (a instanceof Long) {
            res = a.longValue() + b.longValue();
        } else if (a instanceof Float) {
            res = a.floatValue() + b.floatValue();
        } else if (a instanceof Double) {
            res = a.doubleValue() + b.doubleValue();
        }
        return res;
    }

    static Number subtractNumbers(Number a, Number b) {
        Number res = 0;
        if (a instanceof Byte) {
            res = a.byteValue() - b.byteValue();
        } else if (a instanceof Short) {
            res = a.shortValue() - b.shortValue();
        } else if (a instanceof Integer) {
            res = a.intValue() - b.intValue();
        } else if (a instanceof Long) {
            res = a.longValue() - b.longValue();
        } else if (a instanceof Float) {
            res = a.floatValue() - b.floatValue();
        } else if (a instanceof Double) {
            res = a.doubleValue() - b.doubleValue();
        }
        return res;
    }

    static Number divideNumbers(Number a, Number b) {
        Number res = 0;
        if (a instanceof Byte) {
            res = a.byteValue() / b.byteValue();
        } else if (a instanceof Short) {
            res = a.shortValue() / b.shortValue();
        } else if (a instanceof Integer) {
            res = a.intValue() / b.intValue();
        } else if (a instanceof Long) {
            res = a.longValue() / b.longValue();
        } else if (a instanceof Float) {
            res = a.floatValue() / b.floatValue();
        } else if (a instanceof Double) {
            res = a.doubleValue() / b.doubleValue();
        }
        return res;
    }

    @Override
    public List<Double> getValuesAsDouble() {
        List<Double> res = new ArrayList<>();
        for(Number n : list)
        {
            res.add(n.doubleValue());
        }
        return Collections.unmodifiableList(res);
    }

}
