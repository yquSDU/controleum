package dk.sdu.mmmi.controleum.impl.core;

import org.openide.modules.ModuleInstall;

public class ControleumLoader extends ModuleInstall {

    /**
     * <p>It is important that this field is static, since the platform may
     * use two different ModuleInstall instances for restoration and closing.
     * </p>
     */
    private static Session session = null;

    @Override
    public void restored() {
        if(session != null) {
            throw new IllegalStateException();
        }
        
        session = new Session();
        session.init();
    }

    @Override
    public boolean closing() {
        if(session == null) {
            throw new IllegalStateException();
        }

        session.dispose();
        session = null;

        return true;
    }
}
