package dk.sdu.mmmi.controleum.impl.core;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author jcs
 */
public final class ControleumContextImpl implements ControleumContext {

    private final int ID;
    private String name;

    public ControleumContextImpl(String name, int id) {
        this.name = name;
        this.ID = id;
    }

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.ID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ControleumContextImpl other = (ControleumContextImpl) obj;
        if (this.ID != other.ID) {
            return false;
        }
        return true;
    }
}
