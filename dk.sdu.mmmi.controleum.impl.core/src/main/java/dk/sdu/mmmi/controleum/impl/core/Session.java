package dk.sdu.mmmi.controleum.impl.core;

import com.decouplink.Disposable;
import org.openide.util.Lookup;
import dk.sdu.mmmi.controleum.api.core.ControleumContextManager;

/**
 * @author mrj
 */
public class Session implements Disposable {

    /**
     * Load all information from Greenhouse Database API into Greenhouse API.
     */
    public void init() {
        ControleumContextManager gm = Lookup.getDefault().lookup(ControleumContextManager.class);
        gm.loadContexts();
    }

    /**
     * Unload all information.
     */
    @Override
    public void dispose() {

        // Dispose control domains.
        for (Disposable disposable : Lookup.getDefault().lookupAll(Disposable.class)) {
            disposable.dispose();
        }
    }
}
