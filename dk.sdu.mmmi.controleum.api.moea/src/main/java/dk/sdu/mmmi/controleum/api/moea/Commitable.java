/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea;

import dk.sdu.mmmi.controleum.api.moea.Solution;

/**
 *
 * @author ancla
 */
public interface Commitable {
    public void commit(Solution s);
}
