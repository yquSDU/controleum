/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea;

import java.util.BitSet;
import java.util.Date;

/**
 *
 * @author Anders
 */
public interface Issue<V> extends Element, Value<V> {
        /**
     * Provide a random value for V.
     */
    V getRandomValue(Date t);

    /**
     * Provide a mutated value for V.
     */
    V getMutatedValue(V v);

    /**
     * Provide a crossover value for two V instances. It only makes sense to
     * override this method if V is a relatively complex type. If V is something
     * simple - like an Integer or a Double - then there is no need to override
     * this method. For simple types, the default implementation simply picks v1
     * or v2 randomly.
     */
    V crossover(V v1, V v2);

    /**
     * Some complex types, it is required to implement a copy() method for V.
     * For simple types - like Integer or Double - this is not needed.
     *
     * @param time Control time
     * @param obj Object to be copied
     * @return Copy of object obj
     */
    V copy(Date time, V obj);

        int bitSize();

    BitSet asBitSet();

    void setBitSet(BitSet bitset);
    
    public void doCommitValue(Date t, V result);
    
    public String getName();
    
    void addObserver(IssueObserver<V> observer);
    
    void removeObserver(IssueObserver<V> observer);

    void markImmutable();

    void markMutable();

    boolean isMutable();
    
    V qSetBitSet(Date now, BitSet bitset);   //TODO //qqq add for compare v1 PF
}
