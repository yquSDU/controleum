package dk.sdu.mmmi.controleum.api.moea;

import java.net.URL;

/**
 * An element that is input, output or concern in a negotiation.
 *
 * @author mrj, jcs
 */
public interface Element {

    /**
     * Set a name of this element.
     *
     * @param name Name of this concern
     */
    void setName(String name);

    /**
     * @return Name of this element
     */
    String getName();

    /*
     * ID Added to all inputs and outputs, since ID is needed on inputs for dynamic
     * inputs.
     * TODO Not sure if default implementation could return getClass().getName()
     */
    String getID();

    /**
     * URL to html that explains this element.
     *
     * @return An URL or null.
     */
    URL getHtmlHelp();
}
