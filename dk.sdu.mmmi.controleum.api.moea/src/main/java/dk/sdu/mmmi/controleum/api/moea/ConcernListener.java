package dk.sdu.mmmi.controleum.api.moea;

import java.util.EventListener;

/**
 *
 * @author jcs
 */
public interface ConcernListener extends EventListener {

    void onConcernChanged(Concern c);
}
