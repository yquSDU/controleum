package dk.sdu.mmmi.controleum.api.moea;

import java.util.EventListener;

/**
 * @author mrj and jcs
 */
public interface SearchObserver extends EventListener {

    /**
     * When a negotiation round has finished.
     *
     * @param s
     */
    void onSearchCompleted(Solution s);
}
