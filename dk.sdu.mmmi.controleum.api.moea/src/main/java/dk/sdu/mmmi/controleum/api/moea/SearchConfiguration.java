package dk.sdu.mmmi.controleum.api.moea;

/**
 * @author mrj, ancla
 */
public class SearchConfiguration {
    
    private int reduceTo;
    private int popSize;
    private int maxGenerations;
    private long maxNegotiationTimeMS;
    private int reproductionFactor;
    private double mutationRate;
    private double mutationRate2;
    private double crossoverRate;
    private boolean debuggingEnabled;
    private boolean clusteringEnabled;
    private boolean isAggregator;

    public static class Builder {
        //Optional parameters

        private int popSize = 2;  //800
        private int maxGenerations = 1;       //TODO qqq max generation
        private long maxNegotiationTimeMS = 1000L * 60 * 4;
        private int reproductionFactor = 50;
        private double mutationRate = 0.5;
        private double mutationRate2 = 0.5;
        private double crossoverRate = 0.5;
        private boolean debuggingEnabled = false;
        private boolean clusteringEnabled = false;
        private int reduceTo = 3000;
        private boolean isAggregator = false;

        public Builder popSize(int val) {
            popSize = val;
            return this;
        }

        public Builder maxGenerations(int val) {
            maxGenerations = val;
            return this;
        }

        public Builder maxNegotiationTimeMS(long val) {
            maxNegotiationTimeMS = val;
            return this;
        }

        public Builder reproductionFactor(int val) {
            reproductionFactor = val;
            return this;
        }

        public Builder mutationRate(double val) {
            mutationRate = val;
            return this;
        }

        public Builder debuggingEnabled(boolean val) {
            debuggingEnabled = val;
            return this;
        }

        public Builder clusteringEnabled(boolean val) {
            clusteringEnabled = val;
            return this;
        }

        public Builder reduceTo(int val) {
            reduceTo = val;
            return this;
        }

        public Builder crossoverRate(double val) {
            crossoverRate = val;
            return this;
        }

        public Builder mutationRate2(double val) {
            mutationRate2 = val;
            return this;
        }
        
        public Builder isAggregator(boolean val)
        {
            isAggregator = val;
            return this;
        }

        public Builder copy(SearchConfiguration scg) {
            this.debuggingEnabled = scg.debuggingEnabled;
            this.maxGenerations = scg.maxGenerations;
            this.maxNegotiationTimeMS = scg.maxNegotiationTimeMS;
            this.mutationRate = scg.mutationRate;
            this.crossoverRate = scg.crossoverRate;
            this.popSize = scg.popSize;
            this.reproductionFactor = scg.reproductionFactor;
            this.clusteringEnabled = scg.clusteringEnabled;
            this.reduceTo = scg.reduceTo;
            this.mutationRate2 = scg.mutationRate2;
            this.isAggregator = scg.isAggregator;
            return this;
        }

        public SearchConfiguration build() {
            return new SearchConfiguration(this);
        }
    }

    private SearchConfiguration(Builder builder) {
        this.debuggingEnabled = builder.debuggingEnabled;
        this.maxGenerations = builder.maxGenerations;
        this.maxNegotiationTimeMS = builder.maxNegotiationTimeMS;
        this.mutationRate = builder.mutationRate;
        this.popSize = builder.popSize;
        this.reproductionFactor = builder.reproductionFactor;
        this.clusteringEnabled = builder.clusteringEnabled;
        this.reduceTo = builder.reduceTo;
        this.crossoverRate = builder.crossoverRate;
        this.mutationRate2 = builder.mutationRate2;
        this.isAggregator = builder.isAggregator;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    public long getMaxNegotiationTimeMS() {
        return maxNegotiationTimeMS;
    }

    public int getPopSize() {
        return popSize;
    }

    public int getReproductionFactor() {
        return reproductionFactor;
    }

    public double getMutationRate() {
        return mutationRate;
    }

    public boolean isClusteringEnabled() {
        return clusteringEnabled;
    }

    public boolean isDebuggingEnabled() {
        return debuggingEnabled;
    }

    public int reduceTo() {
        return reduceTo;
    }

    public double getCrossoverRate() {
        return crossoverRate;
    }

    public double getMutationRate2() {
        return mutationRate2;
    }

    public boolean isAggregator() {

        return isAggregator;
    }
}
