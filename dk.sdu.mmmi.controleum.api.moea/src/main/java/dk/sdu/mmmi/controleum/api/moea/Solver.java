package dk.sdu.mmmi.controleum.api.moea;

import java.util.Date;
import java.util.List;
import org.netbeans.api.progress.ProgressHandle;

/**
 *
 * @author jcs
 */
public interface Solver {

    Solution getSolution();

    List<Solution> getSolutionList();

    SearchStatistics solve(Date startTime, Date endTime);

    SearchStatistics solve(Date startTime, Date endTime, ProgressHandle h, boolean useSwingThreadNotification);

    void setSearchConfiguration(SearchConfiguration configuration);

    SearchConfiguration getSearchCfg();

    int getGeneration();

    boolean isTerminated();

    void terminate();
}
