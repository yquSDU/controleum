package dk.sdu.mmmi.controleum.api.moea;

import java.util.BitSet;
import java.util.Date;

/**
 *
 * @author mrj, jcs
 */
public interface Output<V> extends Element, IssueObserver<V> {

    boolean isEnabled();

    void setEnabled(boolean enabled, boolean fireEvent);

    int bitSize();

    BitSet asBitSet();

    void setBitSet(BitSet bitset);

    public V getValue();

}
