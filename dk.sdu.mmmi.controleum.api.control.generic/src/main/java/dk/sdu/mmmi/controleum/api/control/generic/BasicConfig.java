package dk.sdu.mmmi.controleum.api.control.generic;

import java.util.UUID;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ancla
 */
public class BasicConfig {

    private final UUID id;
    private String name;
    private String mapName;

    public BasicConfig() {
        this.id = UUID.randomUUID();
        this.name = "no-name";
        this.mapName = "controleum-values";
    }

    public BasicConfig(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.mapName = "controleum-values";
    }

    public BasicConfig(UUID id, String name) {
        this.id = id;
        this.name = name;
        this.mapName = "controleum-values";
    }
    
    public BasicConfig(UUID id, String name, String mapName)
    {
        this.id = id;
        this.name = name;
        this.mapName = mapName;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the mapName
     */
    public String getMapName() {
        return mapName;
    }

    /**
     * @param mapName the mapName to set
     */
    public void setMapName(String mapName) {
        this.mapName = mapName;
    }
}
