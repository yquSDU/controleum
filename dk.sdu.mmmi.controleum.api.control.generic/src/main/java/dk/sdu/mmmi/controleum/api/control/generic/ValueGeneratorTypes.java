/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

/**
 *
 * @author ancla
 */
public enum ValueGeneratorTypes {

    DOUBLE {
                @Override
                public Double generateRandomValue(Number resolution, Number upperBoundary, Number lowerBoundary) {
                    if (resolution.doubleValue() > 0.0) {
                        //return Math.round((Math.random() * (upperBoundary.doubleValue() - lowerBoundary.doubleValue())) + lowerBoundary.doubleValue() / resolution.doubleValue()) * resolution.doubleValue();
                        return BigDecimal.valueOf(Math.round((Math.random() * (upperBoundary.doubleValue() - lowerBoundary.doubleValue()) + lowerBoundary.doubleValue()) / resolution.doubleValue()) * resolution.doubleValue()).setScale(2, RoundingMode.FLOOR).doubleValue();
                    } else {
                        return (Math.random() * (upperBoundary.doubleValue() - lowerBoundary.doubleValue())) + lowerBoundary.doubleValue();
                    }
                }

                @Override
                public Double parseString(String number) {
                    return Double.parseDouble(number);
                }
            },
    FLOAT {

                @Override
                public Float generateRandomValue(Number resolution, Number upperBoundary, Number lowerBoundary) {
                    if (resolution.floatValue() > 0.0) {
                        return Math.round((Math.random() * (upperBoundary.floatValue() - lowerBoundary.floatValue())) + lowerBoundary.floatValue() / resolution.floatValue()) * resolution.floatValue();
                    } else {
                        return ((float) Math.random() * (upperBoundary.floatValue() - lowerBoundary.floatValue())) + lowerBoundary.floatValue();
                    }
                }

                @Override
                public Float parseString(String number) {
                    return Float.parseFloat(number);
                }
            },
    INTEGER {

                @Override
                public Number generateRandomValue(Number resolution, Number upperBoundary, Number lowerBoundary) {
                    if (resolution.intValue() > 0) {
                        return (int) (Math.round(((Math.random() * (upperBoundary.intValue() - lowerBoundary.intValue())) + lowerBoundary.intValue()) / resolution.intValue()) * resolution.intValue());
                    } else {
                        return lowerBoundary.intValue() + (int) (Math.random() * ((upperBoundary.intValue() - lowerBoundary.intValue()) + 1));
                    }
                }

                @Override
                public Integer parseString(String number) {
                    return Integer.parseInt(number);

                }
            },
    BOOLEAN {
                Random r = new Random();

                @Override
                public Number generateRandomValue(Number resolution, Number upperBoundary, Number lowerBoundary) {
                    return (short) (r.nextBoolean() ? 1 : 0);
                }

                @Override
                public Short parseString(String number) {
                    return Short.parseShort(number);
                }
            };

    /**
     * @return the resolution
     */
    public abstract Number generateRandomValue(Number resolution, Number upperBoundary, Number lowerBoundary);

    public abstract Number parseString(String number);
}
