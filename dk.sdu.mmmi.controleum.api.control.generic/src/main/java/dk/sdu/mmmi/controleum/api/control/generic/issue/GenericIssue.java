/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic.issue;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public interface GenericIssue {

    UUID getIssueId();

    Number getLowerBound();

    Number getUpperBound();
    
    void updateToConfig(GenericIssueConfig ic);
    
}
