/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic.issue;

import dk.sdu.mmmi.controleum.api.control.generic.BasicConfig;
import dk.sdu.mmmi.controleum.api.control.generic.ValueGeneratorTypes;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class GenericIssueConfig extends BasicConfig {

    private int timeSlots;
    
    //LEGACY _ REMOVE
    private long timeSpan;
    private Boundary boundary;
    private ValueGeneratorTypes generator;

    public GenericIssueConfig(UUID guid, String name, int timeSlots, long timeSpan, Boundary boundary, ValueGeneratorTypes generator) {
        super(guid, name);
        this.timeSlots = timeSlots;
        this.timeSpan = timeSpan;
        this.boundary = boundary;
        this.generator = generator;
    }

    /**
     *
     * @param name Name of the issue
     * @param timeSlots Number of slots to optimize. Eg. if timeSlot is set to 2, the returned list of results is the same length of 2.
     * @param timeSpan
     * @param boundary boundary object containing the upper and lower boundary for the randomly generated issue values, as well as the resolution.
     * @param generator Type of Number generated gy the ValueGenerator. Eg. Integer produces integers.
     */
    public GenericIssueConfig(String name, int timeSlots, long timeSpan, Boundary boundary, ValueGeneratorTypes generator)
    {
        super(name);
        this.timeSlots = timeSlots;
        this.timeSpan = timeSpan;
        this.boundary = boundary;
        this.generator = generator;
    }

    public GenericIssueConfig() {
        super("Standard Issue Name");
    }
    
    public GenericIssueConfig(String name) {
        super(name);
    }

    /**
     * @return the upperBoundary
     */
    public Number getUpperBoundary() {
        return this.boundary.getUpperBoundary();
    }

    /**
     * @return the lowerBoundary
     */
    public Number getLowerBoundary() {
        return this.boundary.getLowerBoundary();
    }

    /**
     * @return the resolution
     */
    public Number getResolution() {
        return this.boundary.getResolution();
    }

    /**
     * @return the timeSlots
     */
    public int getTimeSlots() {
        return timeSlots;
    }

    /**
     * @param timeSlots the timeSlots to set
     */
    public void setTimeSlots(int timeSlots) {
        this.timeSlots = timeSlots;
    }

    /**
     * @return the timeSpan
     */
    public long getTimeSpan() {
        return timeSpan;
    }

    /**
     * @param timeSpan the timeSpan to set
     */
    public void setTimeSpan(long timeSpan) {
        this.timeSpan = timeSpan;
    }

    /**
     * @return the generator
     */
    public ValueGeneratorTypes getGenerator() {
        return generator;
    }

    /**
     * @param generator the generator to set
     */
    public void setGenerator(ValueGeneratorTypes generator) {
        this.generator = generator;
    }

}
