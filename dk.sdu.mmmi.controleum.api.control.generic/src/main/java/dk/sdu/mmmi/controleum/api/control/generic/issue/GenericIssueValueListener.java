/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic.issue;

import dk.sdu.mmmi.controleum.api.control.generic.ValueEvent;

/**
 *
 * @author ancla
 */
public interface GenericIssueValueListener {
    public void notify(ValueEvent e);
}
