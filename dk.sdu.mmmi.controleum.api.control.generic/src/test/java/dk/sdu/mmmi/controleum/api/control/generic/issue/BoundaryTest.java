package dk.sdu.mmmi.controleum.api.control.generic.issue;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BoundaryTest {

    /**
     * Checks that a exception is thrown if the resolution does not match Upper and Lower boundary.
     */
    @Test
    public void integerInvalidInputTest() {
        int upperBoundary = 9, lowerBoundary = 0, resolution = 5;
        assertThrows(IllegalArgumentException.class,() -> new Boundary(upperBoundary, lowerBoundary, resolution));
    }

    /**
     * Checks that a exception is thrown if upper boundary is null.
     */
    @Test
    public void upperBoundaryNullInputTest() {
        Integer upperBoundary = null;
        int lowerBoundary = 0, resolution = 5;
        assertThrows(IllegalArgumentException.class,() -> new Boundary(upperBoundary, lowerBoundary, resolution));
    }

    /**
     * Checks that a exception is thrown if lower boundary is null.
     */
    @Test
    public void lowerBoundaryNullInputTest() {
        Integer lowerBoundary = null;
        int upperBoundary = 9, resolution = 5;
        assertThrows(IllegalArgumentException.class,() -> new Boundary(upperBoundary, lowerBoundary, resolution));
    }

    /**
     * Checks that a exception is thrown if resolution boundary is null.
     */
    @Test
    public void resolutionNullInputTest() {
        Integer resolution = null;
        int upperBoundary = 9, lowerBoundary = 0;
        assertThrows(IllegalArgumentException.class,() -> new Boundary(upperBoundary, lowerBoundary, resolution));
    }

    /**
     * Checks that a exception is thrown if inputs are of different type.
     */
    @Test
    public void differentInputTypesTest() {
        int upperBoundary = 9, resolution = 5;
        double lowerBoundary = 0.0;
        assertThrows(IllegalArgumentException.class,() -> new Boundary(upperBoundary, lowerBoundary, resolution));
    }

    /**
     * Check that boundary can be created with integers or doubles.
     */
    @Test
    public void boundaryCreationTest() {
        try {
            new Boundary(9,0,1);
            new Boundary(9.5,0.5,1.0);
        } catch (IllegalArgumentException ex) {
            fail(ex.getMessage());
        }
    }
}