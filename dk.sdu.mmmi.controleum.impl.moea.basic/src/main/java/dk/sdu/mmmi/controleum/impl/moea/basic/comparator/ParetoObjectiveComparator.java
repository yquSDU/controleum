package dk.sdu.mmmi.controleum.impl.moea.basic.comparator;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.io.Serializable;
import java.util.List;

/**
 * Compares two solutions using the Pareto dominance relation on the objectives.
 * <p>
 * In general, the {@link ParetoDominanceComparator} should be used instead as
 * it also incorporates constraint violation checks.
 */
public class ParetoObjectiveComparator implements DominanceComparator, Serializable {

    private static final long serialVersionUID = 5086102885918799148L;

    /**
     * Constructs a Pareto dominance comparator.
     */
    public ParetoObjectiveComparator() {
        super();
    }

    @Override
    public int compare(Solution solution1, Solution solution2) {
        boolean dominate1 = false;
        boolean dominate2 = false;

        List<Double> f1 = solution1.getEvaluationValues();
        List<Double> f2 = solution2.getEvaluationValues();

        if (f1.size() != f2.size()) {
            throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
        }

        for (int i = 0; i < f1.size(); i++) {

            if (f1.get(i) < f2.get(i)) {
                dominate1 = true;

                if (dominate2) {
                    return 0;
                }
            } else if (f1.get(i) > f2.get(i)) {
                dominate2 = true;

                if (dominate1) {
                    return 0;
                }
            }
        }

        if (dominate1 == dominate2) {
            return 0;
        } else if (dominate1) {
            return -1;
        } else {
            return 1;
        }
    }
}
