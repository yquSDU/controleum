package dk.sdu.mmmi.controleum.impl.moea.basic;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Issue;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Value;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author mrj & jcs
 */
public class SolutionImpl implements Solution {

    private final static Random R = new Random();
    private final static Logger logger = Logger.getLogger(SolutionImpl.class.getName());
    private final Object problem;
    private final Map<Input, Object> inputValues;
    private final Map<Output, Object> outputValues;
    private final Map<Issue, Object> issueValues;
    private final Date time;
    private List<Double> objectiveValues;

    /**
     * Copy an existing solution.
     */
    private SolutionImpl(Object problem, Date time, Solution copyThis) {
        this.problem = problem;
        this.inputValues = new HashMap<>();
        this.outputValues = new HashMap<>();
        this.issueValues = new HashMap<>();
        this.time = time;

        // Import input values into this candidate solution.
        for (Input i : context(problem).all(Input.class)) {
            inputValues.put(i, i.getValue());
        }

        // Impot output values into this candidate solution.
        for (Output output : context(problem).all(Output.class)) {
            outputValues.put(output, output.getValue());
        }

        // Import random issue values into this candidate solution.
        for (Issue issue : context(problem).all(Issue.class)) {
            Object item = issue.copy(time, copyThis.getIssueValues().get(issue));
            if (item == null) {
                // If current issue doesn't have a value assigned, assign a random value.
                item = issue.getRandomValue(time);
            }
            issueValues.put(issue, item);
        }

    }

    /**
     * Create a random solution.
     */
    private SolutionImpl(Object problem, Date time) {
        this.problem = problem;
        this.inputValues = new HashMap<>();
        this.outputValues = new HashMap<>();
        this.issueValues = new HashMap<>();
        this.time = time;

        // Import input values into this candidate solution.
        for (Input i : context(problem).all(Input.class)) {
            inputValues.put(i, i.getValue());
        }

        // Set random values for all issues.
        for (Issue issue : context(problem).all(Issue.class)) {
            issueValues.put(issue, issue.getRandomValue(time));
        }

        // Set outputs to null, as no value has been assigned to these until termination.
        for (Output o : context(problem).all(Output.class)) {
            outputValues.put(o, null);
        }
    }

    /**
     * Mutate an existing solution.
     */
    private SolutionImpl(SolutionImpl mutateThis) {
        this.problem = mutateThis.problem;
        this.inputValues = new HashMap<>(mutateThis.inputValues);
        this.outputValues = new HashMap<>(mutateThis.outputValues);
        this.issueValues = new HashMap<>(mutateThis.issueValues);
        this.time = mutateThis.time;

        // Mutate one random output.
        ArrayList<Issue> a = new ArrayList(issueValues.keySet());
        if (a.size() > 0) {
            Issue randomIssue = a.get(R.nextInt(a.size()));
            Object randomValue = issueValues.get(randomIssue);
            Object newValue = randomIssue.getMutatedValue(randomValue);
            issueValues.put(randomIssue, newValue);
        }
    }

    /**
     * Crossover solutions must come from the same context. Thus, they can be
     * guaranteed to have the same inputs, input values, and outputs. Only
     * output values are crossed over.
     */
    private SolutionImpl(Solution s1, Solution s2) {
        this.problem = s1.getContext();
        this.inputValues = new HashMap<>(s1.getInputValues());
        this.outputValues = new HashMap<>(s1.getOutputValues()); //new HashMap<Output, Object>(s1.outputs);
        this.issueValues = new HashMap<>(s1.getIssueValues());
        this.time = s1.getTime();

        if (s1.getIssueValues().isEmpty() || s2.getIssueValues().isEmpty()) {
            throw new IllegalArgumentException("0 Issues present in solutions. Terminating.");
        }

        if (s1.getIssueValues().size() != s2.getIssueValues().size()) {
            throw new IllegalArgumentException("Different amount of issues in solution " + s1 + " and solution " + s2 + ". Terminating.");
        }

        if (s1.getInputValues().size() != s2.getInputValues().size()) {
            throw new IllegalArgumentException("Different amount of inputs present in solution " + s1 + " and solution " + s2 + ". Terminating.");
        }

        if (s1.getOutputValues().size() != s2.getOutputValues().size()) {
            throw new IllegalArgumentException("Different amount of outputs present in solution " + s1 + " and solution " + s2 + ". Terminating.");
        }

        if (s1.getContext() != s2.getContext()) {
            throw new IllegalArgumentException("Solution " + s1 + " and " + s2 + " operates on different contexts. Terminating.");
        }

        if (!s1.getTime().equals(s2.getTime())) {
            throw new IllegalArgumentException("Solutions " + s1 + " and " + s2 + " have different date origins (different optimization cycles). Terminating.");
        }

        ArrayList<Issue> keys = new ArrayList<>(s1.getIssueValues().keySet());

        // Select a random point for crossover
        int idx = R.nextInt(issueValues.size());

        // Do issue crossover at the random point
        Issue issue = keys.get(idx);
        issueValues.put(issue, issue.crossover(s1.getIssueValues().get(issue), s2.getIssueValues().get(issue)));

        // Copy the rest the outputs from 'Solution 2, i.e., crossover at solution level.
        for (int i = idx + 1; i < issueValues.size() - 1; i++) {
            Issue issue2 = keys.get(i);
            issueValues.put(issue2, s2.getIssueValues().get(issue2));
        }
    }

    private synchronized <T> T getValue(Value<T> value) {
        // TODO This is quite ugly, but it works.
        // Improvements are most welcome :-)
        Object v = inputValues.get(value);
        if (v == null) {
            v = issueValues.get(value);
        }

        assert v != null;

        return (T) v;
    }

    //
    // Public
    //
    @Override
    public Date getTime() {
        return time;
    }

    @Override
    public double getEvaluationResult(int priority) {

        // Build total fitness.
        double sum = 0;

        // Calculate average.
        for (Concern c : context(problem).all(Concern.class)) {

            if (c.isEnabled() && priority == c.getPriority()) {
                sum += c.evaluate(this);
            }
        }

        return sum;
    }

    @Override
    public List<Double> getEvaluationValues() {
        if (objectiveValues == null) {
            objectiveValues = new ArrayList<>();
            for (Concern c : context(problem).all(Concern.class)) {
                double r = 0;
                if (c.isEnabled()) {
                    r = c.evaluate(this);
                    objectiveValues.add(r);

                } else {
                    // ensure the num. of fitness values == total num. of objectives
                    objectiveValues.add(r);
                }
            }
        }
        return objectiveValues;
    }

    @Override
    public double getEvaluationValue(Class<? extends Concern> clazz) {
        return context(problem).one(clazz).evaluate(this);
    }

    @Override
    public <T> T getValue(Class<? extends Value<T>> valueClazz) {
        return getValue(context(problem).one(valueClazz));
    }

    @Override
    public Object getContext() {
        return problem;

    }

    @Override
    public void commit(Date now) {

        for (Issue issue : context(problem).all(Issue.class)) {
            Object v = issueValues.get(issue);

            issue.setValue(v);

            issue.doCommitValue(now, v);
        }

        for (Concern c : context(problem).all(Concern.class)) {
            if (c.isEnabled()) {
                c.commit(this);
            }
        }
    }

    @Override
    public Solution mutate() {
        return new SolutionImpl(this);
    }

    @Override
    public Solution crossover(Solution s) {
        return new SolutionImpl(this, s);
    }

    @Override
    public Solution copy(Object context, Date time) {
        return new SolutionImpl(context, time, this);
    }

    public static SolutionImpl random(Object context, Date time) {
        return new SolutionImpl(context, time);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("solution = { ");
        sb.append("\n");
//        sb.append("INPUTS : {");
//        for (Map.Entry<Input, Object> e : inputValues.entrySet()) {
//            sb.append(e.getKey());
//            sb.append(", ");
//        }
//        sb.append("\n");

        sb.append("\tConcerns : {");
        for (Concern concern : context(problem).all(Concern.class)) {
            if (concern.isEnabled()) {
                sb.append(concern.getName());
                sb.append(" = ");
                sb.append(String.format("%.2f", concern.evaluate(this)));
                sb.append(", ");
            }
        }
        sb.append("}, \n");
        sb.append("\tOutputs : {");
        for (Map.Entry<Output, Object> e : outputValues.entrySet()) {
            sb.append(e.getKey().getName());
            sb.append(" = ");
            sb.append(e.getValue());
            sb.append(", ");
        }
        sb.append("}\n");

        return sb.toString();
    }

    @Override
    public Map<Output, Object> getOutputValues() {
        return outputValues;
    }

    @Override
    public Map<Input, Object> getInputValues() {
        return inputValues;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.inputValues);
        hash = 97 * hash + Objects.hashCode(this.outputValues);
        hash = 97 * hash + Objects.hashCode(this.objectiveValues);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SolutionImpl other = (SolutionImpl) obj;
        if (!Objects.equals(this.inputValues, other.inputValues)) {
            return false;
        }
        if (!Objects.equals(this.outputValues, other.outputValues)) {
            return false;
        }
        if (!Objects.equals(this.objectiveValues, other.objectiveValues)) {
            return false;
        }
        return true;
    }

}
