package dk.sdu.mmmi.controleum.impl.moea.basic;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.SearchObserver;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;

/**
 * TODO: Make the algorithm parallel according to
 * http://www.heatonresearch.com/node/2955
 *
 * @author mrj & jcs
 */
public class Negotiator implements Solver {

    private final Object problem;
    private final List<Population> popHistory;
    private Population solutionPop;
    private Solution solution;
    private Link<SearchConfiguration> searchCfg;
    private int generation;
    private AtomicBoolean terminated = new AtomicBoolean(false);

    public Negotiator(Object context) {
        this.problem = context;
        this.searchCfg = context(problem).add(SearchConfiguration.class, new SearchConfiguration.Builder().build());
        this.popHistory = new ArrayList<>();
    }

    @Override
    public SearchStatistics solve(Date t) {
        return solve(t, null, false);
    }

    @Override
    public SearchStatistics solve(Date t, ProgressHandle h, boolean useSwingThreadNotification) {

        SearchStatistics ns;
        long durMS = 0;
        generation = 0;
        long startMS = 0;
        terminated = new AtomicBoolean(false);

        try {
            start(h, getSearchCfg().getMaxGenerations());
            Population p = null;

            // 1: Update inputs.
            for (Input i : context(problem).all(Input.class)) {
                progress(h, String.format("Reading input %s ", i.getName()), generation);
                i.doUpdateValue(t);
            }
            // Start epoch
            progress(h, "Initializing.");
            p = new Population(problem, t, solutionPop, getSearchCfg());

            // 2: Genetic search.
            progress(h, "Negotiating.", generation);

            startMS = new Date().getTime();
            while (true && !terminated.get()) {

                generation++;

                // Evolve
                p = p.evolve();

                // Termination.
                durMS = new Date().getTime() - startMS;

                if (!isImprovementLikely(p)) {
                    String message = "Terminated because improvement is unlikely.";
                    progress(h, message);
                    printDebug(message);
                    break;
                }

                if (generation >= getSearchCfg().getMaxGenerations()) {
                    String message = "Terminated due to generation limit.";
                    progress(h, message);
                    printDebug(message);
                    break;
                }

                if (startMS != 0 && durMS >= getSearchCfg().getMaxNegotiationTimeMS()) {
                    String message = "Terminated due to time limit.";
                    progress(h, message);
                    printDebug(message);
                    break;
                }
                progress(h, String.format("Negotiating (Gen. %s, Pop. %s).", generation, p.getSize()), generation);

                // Record recent history
                popHistory.add(p);
            }

            // 3: Commit solution.
            progress(h, "Writing outputs.", generation);
            solution = p.getBestSolution();
            solution.commit(t);

            printDebug(solution);

            // Save population for next run
            solutionPop = p;
            printDebug(p);

            // 5. Notify observers.
            Runnable notify = new Runnable() {

                @Override
                public void run() {

                    List<SearchObserver> l = new CopyOnWriteArrayList(context(problem).all(SearchObserver.class));

                    for (SearchObserver o : l) {
                        long start = System.currentTimeMillis();
                        o.onSearchCompleted(solution);
                        long dur = System.currentTimeMillis() - start;
                        if (dur > 100) {
                            System.out.format("SLOW EVENT NOTIFICATION: %d ms spend notifying %s.\n", dur, o);
                        }
                    }
                    terminated.set(true);
                }
            };

            if (useSwingThreadNotification) {
                SwingUtilities.invokeAndWait(notify);
            } else {
                notify.run();
            }

            finish(h);

        } catch (InterruptedException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
        }
        return new SearchStatistics(generation, durMS);
    }

    @Override
    public Solution getSolution() {

        return solution;
    }

    @Override
    public List<Solution> getSolutionList() {
        return solutionPop != null ? solutionPop.get() : new ArrayList<>();
    }

    @Override
    public void setSearchConfiguration(SearchConfiguration configuration) {
        searchCfg.dispose();
        searchCfg = context(problem).add(SearchConfiguration.class, configuration);
    }

    @Override
    public SearchConfiguration getSearchCfg() {
        return context(problem).one(SearchConfiguration.class);
    }

    @Override
    public int getGeneration() {
        return generation;
    }

    @Override
    public boolean isTerminated() {
        return terminated.get();
    }

    @Override
    public void terminate() {
        this.terminated.set(true);
    }

    //
    // Private methods
    //
    private void printDebug(Object line) {
        if (getSearchCfg().isDebuggingEnabled()) {
            System.out.println(line);
            System.out.println();
        }

        // TODO: Export population statistics to CVS file
    }

    private boolean isImprovementLikely(Population p) {

        boolean result = true;

        if (popHistory.size() > 10) {

            for (Population pastPop : popHistory) {
                if (!p.equals(pastPop)) {
                    break;
                } else {
                    result = false;
                }
            }
            popHistory.clear();
        }

        return result;
    }

    private void start(ProgressHandle h, int size) {
        if (h != null) {
            h.start(size);
        }
    }

    private void finish(ProgressHandle h) {
        if (h != null) {
            h.finish();
        }
    }

    private void progress(ProgressHandle h, String message) {
        if (h != null) {
            h.progress(message);
        }
    }

    private void progress(ProgressHandle h, String msg, int progress) {
        if (h != null) {
            h.progress(msg, progress);
        }
    }

}
