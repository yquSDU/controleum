package dk.sdu.mmmi.controleum.impl.moea.basic;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.impl.moea.basic.comparator.AggregateConstraintComparator;
import dk.sdu.mmmi.controleum.impl.moea.basic.comparator.ChainedComparator;
import dk.sdu.mmmi.controleum.impl.moea.basic.comparator.ParetoObjectiveComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 *
 * @author mrj & jcs
 */
class Population {

    private final Object problem;
    /**
     * Pareto front
     */
    private final List<Solution> pop; // best first
    /**
     * Level of significance or machine precision.
     */
    public static final double EPS = 1e-10;
    private final SearchConfiguration cfg;
    /**
     * Creation time of population
     */
    private final Date time;
    private final static Random R = new Random();

    /**
     * Create initial population.
     */
    public Population(Object problem, Date time, Population reuse, SearchConfiguration cfg) {
        this.problem = problem;
        this.cfg = cfg;
        this.pop = new ArrayList<>();
        this.time = time;

        if (reuse != null) {
            for (Solution oldSolution : reuse.pop) {
                addToNonDominatedSolutions(oldSolution.copy(problem, time));
            }
        }

        // Use randomness
        for (int i = 0; i < cfg.getPopSize(); i++) {
            addToNonDominatedSolutions(SolutionImpl.random(problem, time));
        }
    }

    /**
     * Evolve new population.
     */
    private Population(Population old) {
        this.problem = old.problem;
        this.cfg = old.cfg;
        this.time = old.time;
        this.pop = new ArrayList(old.pop);

        for (int i = 0; i <= cfg.getPopSize(); i++) {
            // Mutate
            if (R.nextDouble() < cfg.getMutationRate()) {
                // Mutate.
                addToNonDominatedSolutions(randomElement(pop).mutate());
            } else {
                // Crossover.
                Solution a = randomElement(pop);
                Solution b = randomElement(pop);
                addToNonDominatedSolutions(a.crossover(b));
            }
        }
    }

    public Population evolve() {
        return new Population(this);
    }

    /**
     * If {@code newSolution} dominates any solution or is non-dominated with
     * all solutions in this population, the dominated solutions are removed and
     * {@code newSolution} is added to this population. Otherwise,
     * {@code newSolution} is dominated and is not added to this population.
     */
    private boolean addToNonDominatedSolutions(Solution newSolution) {
        for (Iterator<Solution> it = pop.iterator(); it.hasNext();) {
            Solution oldSolution = it.next();

            int flag = PARETO_FITNESS.compare(newSolution, oldSolution);

            if (flag < 0) {
                it.remove();
            } else if (flag > 0) {
                return false;
            } else if (distance(newSolution, oldSolution) < EPS) {
                return false;
            }
        }

        pop.add(newSolution);
        return true;
    }

    /**
     * Returns the Euclidean distance between two solutions in objective space.
     *
     * @param s1 the first solution
     * @param s2 the second solution
     * @return the distance between the two solutions in objective space
     */
    static double distance(Solution s1, Solution s2) {
        double distance = 0.0;

        List<Double> f1 = s1.getEvaluationValues();
        List<Double> f2 = s2.getEvaluationValues();

        for (int i = 0; i < f1.size(); i++) {
            distance += Math.pow(f1.get(i) - f2.get(i), 2.0);
        }

        return Math.sqrt(distance);
    }

    public List<Solution> get() {
        return pop;
    }

    public Solution getBestSolution() {
        Collections.sort(pop, BY_PRIORITIZED_FITNESS);
        return pop.get(0);
    }

    public Solution getWorstSolution() {
        Collections.sort(pop, BY_PRIORITIZED_FITNESS);
        return pop.get(pop.size() - 1);
    }

    public Solution getMeanSolution() {
        Collections.sort(pop, BY_PRIORITIZED_FITNESS);
        return pop.get(pop.size() / 2);
    }

    public List<Double> getBestFitness() {
        return getBestSolution().getEvaluationValues();
    }

    public List<Double> getWorstFitness() {
        return getWorstSolution().getEvaluationValues();
    }

    public List<Double> getMeanFitness() {
        return getMeanSolution().getEvaluationValues();
    }

    public int getSize() {
        return pop.size();
    }

    //
    // Misc.
    //
    private <T> T randomElement(List<T> list) {
        return list.get(R.nextInt(list.size()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.pop);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Population other = (Population) obj;
        if (!Objects.equals(this.pop, other.pop)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("Population = { ");
        sb.append("size : ");
        sb.append(getSize());
        sb.append(", ");
        sb.append("SolutionList : {\n");
        for (Solution solution : pop) {
            sb.append("\nSolution={\nConcerns = {");
            for (int i = Concern.HARD_PRIORITY; i <= Concern.LOW_PRIORITY; i++) {
                sb.append(solution.getEvaluationResult(i));
                sb.append(", ");
            }
            sb.append("}");
            sb.append(",\n");
            sb.append("Outputs={");
            sb.append(solution.getOutputValues().values());
            sb.append("}");

            sb.append("\n},\n");
        }

        sb.append("}");
        return sb.toString();
    }

    //
    // Comparators.
    //
    private final Comparator<Solution> BY_PRIORITIZED_FITNESS = new Comparator<Solution>() {
        @Override
        public int compare(Solution s1, Solution s2) {
            int r = 0;

            for (int p = Concern.HARD_PRIORITY; p <= Concern.LOW_PRIORITY; p++) {

                double f1 = s1.getEvaluationResult(p);
                double f2 = s2.getEvaluationResult(p);
                if (f1 != f2) {
                    r = (f1 > f2) ? 1 : -1;
                    break;
                } else {
                    r = 0;
                }
            }
            return r;
        }
    };
    /**
     * Interface for comparing two solutions using a dominance relation. A
     * dominance relation may impose a partial or total ordering on a set of
     * solutions.
     * <p>
     * Implementations which also implement {@link Comparator} impose a total
     * ordering on the set of solutions. However, it is typically the case that
     * {@code (compare(x, y)==0) == (x.equals(y))} does not hold, and the
     * comparator may impose orderings that are inconsistent with equals.
     */
    private final Comparator<Solution> PARETO_FITNESS = new ChainedComparator(new AggregateConstraintComparator(), new ParetoObjectiveComparator());

}
