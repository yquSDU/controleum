package dk.sdu.mmmi.controleum.impl.moea.basic.comparator;

import java.io.Serializable;

/**
 * Compares two solutions using aggregate constraint violation and the Pareto
 * dominance relation as originally proposed by Kalyanmoy Deb.
 * <p>
 * References:
 * <ol>
 * <li>Kalyanmoy, D., "An Efficient Constraint Handling Method for Genetic
 * Algorithms." Computer Methods in Applied Mechanics and Engineering, pp.
 * 311--338, 1998.
 * </ol>
 *
 * @see AggregateConstraintComparator
 * @see ParetoObjectiveComparator
 */
public class ParetoDominanceComparator extends ChainedComparator implements Serializable {

    private static final long serialVersionUID = -3198596505754896119L;

    /**
     * Constructs a Pareto dominance comparator.
     */
    public ParetoDominanceComparator() {
        super(new AggregateConstraintComparator(),
                new ParetoObjectiveComparator());
    }

}
