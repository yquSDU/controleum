package dk.sdu.mmmi.controleum.impl.moea.basic;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import java.util.UUID;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private static final Logger LOGGER = Logger.getLogger(Plugin.class.getName());

    @Override
    public Disposable create(ControleumContext g, UUID issueId) {   //qqq add UUID issueId
        DisposableList d = new DisposableList();
        
        d.add(context(g).add(Solver.class, new Negotiator(g)));

        return d;
    }
}
