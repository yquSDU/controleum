package dk.sdu.mmmi.controleum.impl.moea.basic.comparator;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.io.Serializable;
import java.util.Comparator;

/**
 * Compares solutions based on their magnitude of constraint violations.
 * Absolute values of constraints are used, so only the magnitude of the
 * constraint violation is important.
 */
public class AggregateConstraintComparator implements DominanceComparator, Comparator<Solution>, Serializable {

    private static final long serialVersionUID = 2876962422278502088L;

    /**
     * Constructs an aggregate constraint comparator.
     */
    public AggregateConstraintComparator() {
        super();
    }

    /**
     * Returns the sum of the absolute value of the constraints for the
     * specified solution. This allows supporting constraints expressed as both
     * positive and negative magnitudes. Larger magnitudes correspond to larger
     * constraint violations.
     *
     * @param solution the solution
     * @return the sum of the absolute value of the constraints for the
     * specified solution
     */
    protected double getConstraints(Solution solution) {
        return Math.abs(solution.getEvaluationResult(Concern.HARD_PRIORITY));
    }

    @Override
    public int compare(Solution solution1, Solution solution2) {
        double constraints1 = getConstraints(solution1);
        double constraints2 = getConstraints(solution2);

        if ((constraints1 != 0.0) || (constraints2 != 0.0)) {
            if (constraints1 == 0.0) {
                return -1;
            } else if (constraints2 == 0.0) {
                return 1;
            } else {
                return Double.compare(constraints1, constraints2);
            }
        } else {
            return 0;
        }
    }
}
