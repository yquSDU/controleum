package dk.sdu.mmmi.controleum.impl.moea.basic.comparator;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.util.Comparator;

/**
 * Interface for comparing two solutions using a dominance relation. A dominance
 * relation may impose a partial or total ordering on a set of solutions.
 * <p>
 * Implementations which also implement {@link Comparator} impose a total
 * ordering on the set of solutions. However, it is typically the case that
 * {@code (compare(x, y)==0) == (x.equals(y))} does not hold, and the comparator
 * may impose orderings that are inconsistent with equals.
 */
public interface DominanceComparator extends Comparator<Solution> {

    /**
     * Compares the two solutions using a dominance relation, returning
     * {@code -1} if {@code solution1} dominates {@code solution2}, {@code 1} if
     * {@code solution2} dominates {@code solution1}, and {@code 0} if the
     * solutions are non-dominated.
     *
     * @param solution1 the first solution
     * @param solution2 the second solution
     * @return {@code -1} if {@code solution1} dominates {@code solution2},
     * {@code 1} if {@code solution2} dominates {@code solution1}, and {@code 0}
     * if the solutions are non-dominated
     */
    public int compare(Solution solution1, Solution solution2);

}
